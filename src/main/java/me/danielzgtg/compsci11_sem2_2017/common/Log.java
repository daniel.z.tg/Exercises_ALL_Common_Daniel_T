package me.danielzgtg.compsci11_sem2_2017.common;

import java.io.PrintStream;

/**
 * Logging functions.
 *
 * @author Daniel Tang
 * @since 20 April 2017
 */
@SuppressWarnings("unused")
public final class Log {

	/**
	 * Whether logging will happen.
	 */
	private static boolean enabled = true;

	/**
	 * The logging target.
	 */
	private static PrintStream target = System.out;

	/**
	 * Prints a formatted string to the log.
	 *
	 * @param format The format for {@link String#format}.
	 * @param args The formatting arguments for {@link String#format}.
	 * @see PrintStream#format(String, Object...)
	 */
	public static final void log(final String format, final Object ... args) {
		log(String.format(format, args));
	}

	/**
	 * Prints some text to the log.
	 *
	 * @param s The {@code String} to log.
	 */
	public static final void log(final String s) {
		if (enabled) {
			target.println("[LOG] " + s);
		}
	}

	/**
	 * Prints the stack trace of the {@code Throwable} to the log.
	 *
	 * @param t The {@code Throwable} to log.
	 */
	public static final void log(final Throwable t) {
		if (t != null) {
			t.printStackTrace(target);
		}
	}

	/**
	 * Prints {@code Object} to the log, if enabled.
	 *
	 * @param o The {@code Object} to log.
	 */
	public static final void log(final Object o) {
		log(String.valueOf(o));
	}

	/**
	 * Prints the {@code boolean} to the log, if enabled.
	 *
	 * @param b The {@code boolean} to log.
	 */
	public static final void log(final boolean b) {
		log(String.valueOf(b));
	}

	/**
	 * Prints the {@code char} to the log, if enabled.
	 *
	 * @param c The {@code char} to log.
	 */
	public static final void log(final char c) {
		log(String.valueOf(c));
	}

	/**
	 * Prints the {@code short} to the log, if enabled.
	 *
	 * @param s The {@code short} to log.
	 */
	public static final void log(final short s) {
		log(String.valueOf(s));
	}

	/**
	 * Prints the {@code int} to the log, if enabled.
	 *
	 * @param s The {@code int} to log.
	 */
	public static final void log(final int s) {
		log(String.valueOf(s));
	}

	/**
	 * Prints the {@code long} to the log, if enabled.
	 *
	 * @param s The {@code long} to log.
	 */
	public static final void log(final long s) {
		log(String.valueOf(s));
	}

	/**
	 * Prints the {@code float} to the log, if enabled.
	 *
	 * @param s The {@code float} to log.
	 */
	public static final void log(final float s) {
		log(String.valueOf(s));
	}

	/**
	 * Prints the {@code double} to the log, if enabled.
	 *
	 * @param s The {@code double} to log.
	 */
	public static final void log(final double s) {
		log(String.valueOf(s));
	}

	/**
	 * Turns log output on.
	 */
	public static final void enableLogging() {
		enabled = true;
	}

	/**
	 * Mutes log output.
	 */
	public static final void disableLogging() {
		enabled = false;
	}

	/**
	 * Changes the target of log stream.
	 *
	 * @param target The {@link PrintStream} target.
	 */
	public static final void setLogTarget(final PrintStream target) {
		Validate.notNull(target, "Use disableLogging() to disable, not null!");

		Log.target = target;
	}

	@Deprecated
	private Log() { throw new UnsupportedOperationException(); }
}
