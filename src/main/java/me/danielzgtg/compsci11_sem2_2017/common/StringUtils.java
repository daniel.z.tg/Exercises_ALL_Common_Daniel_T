package me.danielzgtg.compsci11_sem2_2017.common;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Utilities for dealing with {@link String}s.
 *
 * @author Daniel Tang
 * @since 3 April 2017
 */
@SuppressWarnings({"SameParameterValue", "unused", "WeakerAccess"})
public final class StringUtils {

	/**
	 * A {@link Pattern} that matches anything but whitespace.
	 */
	public static final Pattern NON_WHITESPACE_PATTERN = Pattern.compile("[^\\s]");

	/**
	 * A {@link Predicate} that tests if a {@link String} contains non whitespace characters.
	 */
	public static final Predicate<String> NON_WHITESPACE_DETECTOR = NON_WHITESPACE_PATTERN.asPredicate();

	/**
	 * Produces a {@link String} representing the original,
	 * but with a minimum specified length, padded using spaces.
	 *
	 * @param input The original {@link String}. May NOT be {@code null}.
	 * @param length The minimum length to extend to.
	 * @param rightAlign If {@code true} spaces will be added from the left side.
	 * @return A {@link String} representing the original, but with minimum length of the specified length.
	 * @throws IllegalArgumentException If {@code input} is {@code null}.
	 */
	public static final String padString(final String input, final int length, final boolean rightAlign)
			throws IllegalArgumentException{
		Validate.notNull(input);

		if (length <= input.length()) { // Already long enough
			return input;
		}

		final int origLength = input.length();

		// First, make a char array of spaces
		char[] result = new char[length];
		Arrays.fill(result, ' ');

		// Then copy in the original String characters
		if (rightAlign) {
			input.getChars(0, origLength, result, length - origLength);
		} else {
			input.getChars(0, origLength, result, 0);
		}

		return new String(result);
	}

	/**
	 * Produces a {@link String} of the specific length, filled with the specific character
	 *
	 * @param length The length to make the String. Must NOT be negative.
	 * @param stamp The {@code char} to fill the String with.
	 * @return The String filled with the specified character, to the specified length.
	 * @throws IllegalArgumentException If {@code length} is negative.
	 */
	public static final String stringWithChar(final int length, final char stamp)
			throws IllegalArgumentException{
		if (length <= 0) {
			if (length < 0) {
				throw new IllegalArgumentException("String length cannot be negative!");
			}

			// Zero-length String are simply empty ones
			return "";
		}

		char[] result = new char[length];
		Arrays.fill(result, stamp);
		return new String(result);
	}

	/**
	 * Joins a {@link String}{@code []}, with a space separator.
	 *
	 * @param strings The array of String to join together.
	 * @return The String joined into a big String.
	 * @throws IllegalArgumentException If {@code strings} or an element of it is {@code null}.
	 */
	public static final String joinStrings(final String[] strings) throws IllegalArgumentException {
		Validate.withoutNull(strings);

		final StringBuilder result = new StringBuilder();
		for (final String s : strings) {
			result.append(s);
		}

		return result.toString();
	}

	/**
	 * Joins a {@link String}{@code []}, with a specified separator.
	 *
	 * @param strings The array of Strings to join together. It and any elements may NOT be {@code null}.
	 * @param separator The separator between elements. May NOT be {@code null}.
	 * @return A {@link String} representing the inputs joined together.
	 * @throws IllegalArgumentException If any parameter, or element of, is {@code null}.
	 */
	public static final String joinStrings(final String[] strings, final String separator)
			throws IllegalArgumentException{
		Validate.notNull(strings);
		Validate.notNull(separator);

		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			final String s = strings[i];
			Validate.notNull(s);

			if (i != 0 ) {
				result.append(separator);
			}

			result.append(s);
		}

		return result.toString();
	}

	/**
	 * Joins a {@link String}{@code []}, with a specified separator, prefix, and suffix.
	 *
	 * @param strings The array of {@link String}s to join together. It and any elements may NOT be {@code null}.
	 * @param separator The separator between elements. May NOT be {@code null}.
	 * @param prefix The prefix to prepend to the result. May NOT be {@code null}.
	 * @param suffix The suffix to append to the result. May NOT be {@code null}.
	 * @return A {@link String} representing the inputs joined together.
	 * @throws IllegalArgumentException If any parameter, or element of, is {@code null}.
	 */
	public static final String joinStrings(final String[] strings, final String separator,
			final String prefix, final String suffix) throws IllegalArgumentException {
		Validate.notNull(strings);
		Validate.notNull(separator);
		Validate.notNull(prefix);
		Validate.notNull(suffix);

		final StringBuilder result = new StringBuilder(prefix);
		for (int i = 0; i < strings.length; i++) {
			final String s = strings[i];
			Validate.notNull(s);

			if (i != 0 ) {
				result.append(separator);
			}

			result.append(s);
		}

		return result.append(suffix).toString();
	}

	/**
	 * Converts a {@link String} into a simple blob of lowercase letters.
	 *
	 * @param input The original {@link String}. May NOT be {@code null}.
	 * @return A {@link String} with only the alphabetic characters of the original, in lowercase.
	 * @throws IllegalArgumentException If {@code input} is {@code null}.
	 */
	public static final String asSimpleBlob(final String input) throws IllegalArgumentException {
		Validate.notNull(input);

		return input.toLowerCase().replaceAll("[^a-z]", "");
	}

	/**
	 * Converts the {@link String} to upperCamelCase format.
	 *
	 * @param input The {@link String} to format.
	 * @return A {@link String} representing the original, in upperCamelCase.
	 */
	public static final String toUpperCamelCase(final String input) {
		Validate.notNull(input);

		if (input.length() == 0) {
			return "";
		}

		return Character.toUpperCase(input.charAt(0)) + input.substring(1);
	}

	/**
	 * Determines the standardized name for the getter method for the specified field name.
	 *
	 * @param fieldName The field name to find the getter for.
	 * @return The standardized name for the getter method for the specified field name.
	 */
	public static final String fieldToGetterName(final String fieldName) {
		if (fieldName == null) {
			return "get";
		}

		return "get" + toUpperCamelCase(fieldName);
	}

	/**
	 * Determines the standardized name for the setter method for the specified field name.
	 *
	 * @param fieldName The field name to find the setter for.
	 * @return The standardized name for the setter method for the specified field name.
	 */
	public static final String fieldToSetterName(final String fieldName) {
		if (fieldName == null) {
			return "set";
		}

		return "set" + toUpperCamelCase(fieldName);
	}

	/**
	 * Determines if the {@link String} is a decimal. Not to be confused with if it is a floating-point number.
	 *
	 * @param input The {@link String} to check.
	 * @return {@code true} if it is indeed a decimal.
	 */
	public static final boolean isDecimal(final String input) {
		Validate.notNull(input);

		final int length = input.length();
		if (length == 0) {
			return true;
		}

		boolean seenDecimal = false;
		for (int i = input.charAt(0) == '-' ? 1 : 0; i < length; i++) {
			switch (input.charAt(i)) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					continue;
				case '.':
					if (seenDecimal) {
						return false;
					}
					seenDecimal = true;

					break;
				default:
					return false;
			}
		}

		return true;
	}

	/**
	 * Determines if the {@link String} contains any non-whitespace characters or is {@code null}.
	 *
	 * @param input The {@link String} to check.
	 * @return {@code true} if the {@link String} is not {@code null} and contains any non-whitespace character.
	 */
	public static final boolean containsNonWhitespace(final String input) {
		return input != null && NON_WHITESPACE_DETECTOR.test(input);
	}

	/**
	 * Format an {@code int[]} into a {@link String}.
	 *
	 * @param array The {@code int[]} to format.
	 * @param delimiter The separator between elements.
	 * @return A {@link String} representing the {@code int[]}
	 */
	public static final String formatArray(final int[] array, final String delimiter) {
		final StringBuilder result = new StringBuilder();

		for (int i = 0; i < array.length; i++) {
			result.append(array[i]);

			if (i != array.length - 1) {
				result.append(delimiter);
			}
		}

		return result.toString();
	}


	/**
	 * Format an {@code int[]} into a {@link String}.
	 *
	 * @param array The {@code int[]} to format.
	 * @param delimiter The separator between elements.
	 * @param prefix The {@link String} to prepend at the beginning of the output.
	 * @param suffix The {@link String} to append at the beginning of the output.
	 * @return A {@link String} representing the {@code int[]}
	 */
	public static final String formatArray(final int[] array, final String delimiter,
			final String prefix, final String suffix) {
		final StringBuilder result = new StringBuilder();

		result.append(prefix);
		for (int i = 0; i < array.length; i++) {
			result.append(array[i]);

			if (i != array.length - 1) {
				result.append(delimiter);
			}
		}
		result.append(suffix);

		return result.toString();
	}

	@Deprecated
	private StringUtils() { throw new UnsupportedOperationException(); }
}
