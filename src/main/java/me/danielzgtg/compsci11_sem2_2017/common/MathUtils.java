package me.danielzgtg.compsci11_sem2_2017.common;

/**
 * Math utility methods.
 *
 * @author Daniel Tang
 * @since 3 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class MathUtils {

	/**
	 * Calculates the sum of all the numbers in the array.
	 *
	 * @param data The array to sum
	 * @return The sum of all numbers in the array.
	 */
	public static final int arraySum(final int[] data) {
		if (data == null) {
			return 0;
		}

		int result = 0;
		for (final int i : data) {
			result += i;
		}

		return result;
	}

	/**
	 * Reverses an array by swapping. This modifies the passed array.
	 *
	 * @param array The array to reverse.
	 */
	public static final void reverseArray(final int[] array) {
		final int length = array.length;

		// ">>> 1" == "/ 2"
		// Next one if even, round down if odd
		final int midpoint = ((length - 1) >> 1) + 1;

		for (int i = 0; i < midpoint; i++) {
			final int tmp = array[i];
			final int otherSide = length - 1 - i;

			array[i] = array[otherSide];
			array[otherSide] = tmp;
		}
	}

	/**
	 * Determines the maximum of the two {@link Comparable}s, or the first if they are equal.
	 *
	 * @param o1 The first {@link Comparable} to compare.
	 * @param o2 The second {@link Comparable} to compare.
	 * @param <T> The type of {@link Comparable}s to compare.
	 * @return The maximum of the two {@link Comparable}s, or the first if they are equal.
	 */
	public static final <T extends Comparable<T>> T getMax(final T o1, final T o2) {
		if (o1 == null) {
			return o2;
		}

		if (o2 == null) {
			return o1;
		}

		return o1.compareTo(o2) < 0 ? o2 : o1;
	}

	/**
	 * Determines the minimum of the two {@link Comparable}s, or the first if they are equal.
	 *
	 * @param o1 The first {@link Comparable} to compare.
	 * @param o2 The second {@link Comparable} to compare.
	 * @param <T> The type of {@link Comparable}s to compare.
	 * @return The minimum of the two {@link Comparable}s, or the first if they are equal.
	 */
	public static final <T extends Comparable<T>> T getMin(final T o1, final T o2) {
		if (o1 == null) {
			return o2;
		}

		if (o2 == null) {
			return o1;
		}

		return o1.compareTo(o2) < 0 ? o1 : o2;
	}

	/**
	 * {@link Math#PI} as a {@code float}.
	 */
	public static final float PI = (float) Math.PI;

	/**
	 * Half of {@link Math#PI} as a {@code float}.
	 */
	public static final float HALF_PI = PI / 2;

	/**
	 * Half of {@link Math#PI} as a {@code double}.
	 */
	public static final double HALF_PI2 = Math.PI / 2;

	/**
	 * Normalizes an angle in degrees.
	 *
	 * @param angle The angle to normalize.
	 * @return An equivalent, but normalized angle.
	 */
	public static final float normalizeAngleDeg(final float angle) {
		return ((angle + 180) % 360) - 180;
	}

	/**
	 * Normalizes an angle in degrees.
	 *
	 * @param angle The angle to normalize.
	 * @return An equivalent, but normalized angle.
	 */
	public static final double normalizeAngleDeg(final double angle) {
		return ((angle + 180) % 360) - 180;
	}

	/**
	 * Normalizes an angle in radians.
	 *
	 * @param angle The angle to normalize.
	 * @return An equivalent, but normalized angle.
	 */
	public static final float normalizeAngleRad(final float angle) {
		return ((angle + HALF_PI) % PI) - HALF_PI;
	}

	/**
	 * Normalizes an angle in radians.
	 *
	 * @param angle The angle to normalize.
	 * @return An equivalent, but normalized angle.
	 */
	public static final double normalizeAngleRad(final double angle) {
		return ((angle + HALF_PI2) % Math.PI) - HALF_PI2;
	}

	@Deprecated
	private MathUtils() { throw new UnsupportedOperationException(); }
}
