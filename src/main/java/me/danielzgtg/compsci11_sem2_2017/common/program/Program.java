package me.danielzgtg.compsci11_sem2_2017.common.program;

import java.util.LinkedList;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A subprogram of one or more threads.
 *
 * @author Daniel Tang
 * @since 16 June 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class Program {

	/**
	 * The name of the {@link Program}.
	 */
	public final String name;

	/**
	 * Whether this {@link Program} is still running.
	 */
	private volatile boolean running = true;

	/**
	 * A list of {@link Thread}s in this {@link Program}.
	 */
	private final List<Thread> threads = new LinkedList<>();

	/**
	 * Creates a new {@link Program}.
	 *
	 * @param name The name of the {@link Program}, non-empty.
	 */
	public Program(final String name) {
		Validate.require(StringUtils.containsNonWhitespace(name));

		this.name = name;
	}

	/**
	 * Gets the name of the {@link Program}.
	 *
	 * @return The name of the {@link Program}.
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * Calculates the hash code for this {@link Program}.
	 * This just uses the hash code of the name.
	 *
	 * @return The hash code for this {@link Program}.
	 */
	@Override
	public final int hashCode() {
		return this.name.hashCode();
	}

	/**
	 * Determines whether this {@link Program} is equal to another {@link Object}.
	 *
	 * @param other The other {@link Object} to compare with.
	 * @return {@code true} if the other {@link Object} is the same instance as this {@link Program}.
	 */
	@Override
	public final boolean equals(final Object other) {
		return other == this;
	}

	/**
	 * Determines whether this {@link Program} has not been requested to stop.
	 *
	 * @return {@code true} if this program has not been requested to stop.
	 */
	public final boolean running() {
		return this.running;
	}

	/**
	 * Requests that this {@link Program} stop.
	 */
	public final void stop() {
		if (this.running) {
			synchronized (this.threads) {
				if (this.running) {
					this.running = false;

					for (final Thread t : this.threads) {
						t.interrupt();
					}
				}
			}
		}
	}

	/**
	 * Requests that this {@link Program} stops, and waits for it to do so.
	 */
	public final void joinStop() {
		this.stop();

		boolean interrupted = false;
		for (final Thread t : this.threads) {
			while (true) {
				try {
					t.join();
					break;
				} catch (final InterruptedException e) {
					interrupted = true;
				}
			}
		}

		// Restore interrupt flag
		if (interrupted) {
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Adds a {@link Thread} to this {@link Program}.
	 *
	 * @param t The {@link Thread} to add, not null.
	 * @param name The name of the {@link Thread}, non-empty.
	 * @throws IllegalArgumentException if this program has been requested to stop.
	 */
	public final void addThread(final Thread t, final String name) {
		Validate.notNull(t);
		Validate.require(StringUtils.containsNonWhitespace(name));

		if (!this.running) {
			throw new IllegalStateException();
		}

		synchronized (this.threads) {
			if (!this.running) {
				throw new IllegalStateException();
			}

			this.threads.add(t);
		}

		t.setName(this.name + ' ' + name);
	}
}
