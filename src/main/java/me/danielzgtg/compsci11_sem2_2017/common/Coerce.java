package me.danielzgtg.compsci11_sem2_2017.common;

import java.util.Collection;
import java.util.Iterator;

/**
 * Tools to consider {@link Object}s as their expected type.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class Coerce {

	/**
	 * Used internally to unbox arrays, and process strings.
	 *
	 * @param o The {@link Object} to process, nullable.
	 * @return A more meaningful {@link Object}.
	 */
	private static final Object helpDecode(final Object o) {
		if (o == null) {
			return null;
		}

		if (o instanceof CharSequence) {
			return o.toString();
		} else if (o instanceof char[]) {
			return new String((char[]) o);
		} else if (o instanceof byte[]) {
			return ((byte[]) o)[0];
		} else if (o instanceof short[]) {
			return ((short[]) o)[0];
		} else if (o instanceof int[]) {
			return ((int[]) o)[0];
		} else if (o instanceof long[]) {
			return ((long[]) o)[0];
		} else if (o instanceof float[]) {
			return ((float[]) o)[0];
		} else if (o instanceof double[]) {
			return ((double[]) o)[0];
		} else if (o instanceof Collection) {
			final Iterator iter = ((Collection) o).iterator();

			return iter.hasNext() ? iter.next() : o;
		} else {
			return o;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@link String}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@link String} that the {@link Object} is estimated to be, not null.
	 */
	public static final String asString(final Object o) {
		final Object result = helpDecode(o);

		return result == null ? "" : result.toString();
	}

	/**
	 * Tries to consider an {@link Object} as a {@code boolean}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code boolean} that the {@link Object} is estimated to be.
	 */
	public static final boolean asBool(Object o) {
		o = helpDecode(o);

		if (o instanceof Boolean) {
			return (Boolean) o;
		} else if (o instanceof boolean[]) {
			return ((boolean[]) o)[0];
		} else {
			if (o instanceof CharSequence) {
				char c0 = ((CharSequence) o).charAt(0);

				switch (c0) {
					case 'T':
					case 't':
					case 'Y':
					case 'y':
						return true;
					//case 'F':
					//case 'f':if
					//case 'N':
					//case 'n':
					default:
						return false;
				}
			} else {
				return false;
			}
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code char}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code char} that the {@link Object} is estimated to be.
	 */
	public static final char asChar(Object o) {
		o = helpDecode(o);

		if (o instanceof Character) {
			return (Character) o;
		} else if (o instanceof CharSequence) {
			return ((String) o).charAt(0);
		} else if (o instanceof char[]) {
			return ((char[]) o)[0];
		} else if (o instanceof Number) {
			return (char) ((Number) o).shortValue();
		} else {
			return ' ';
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code byte}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code byte} that the {@link Object} is estimated to be.
	 */
	public static final byte asByte(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).byteValue();
		} else {
			if (o instanceof String) {
				try {
					return Byte.parseByte((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code short}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code short} that the {@link Object} is estimated to be.
	 */
	public static final short asShort(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).shortValue();
		} else {
			if (o instanceof String) {
				try {
					return Short.parseShort((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code int}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code int} that the {@link Object} is estimated to be.
	 */
	public static final int asInt(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).intValue();
		} else {
			if (o instanceof String) {
				try {
					return Integer.parseInt((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code long}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code long} that the {@link Object} is estimated to be.
	 */
	public static final long asLong(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).longValue();
		} else {
			if (o instanceof String) {
				try {
					return Long.parseLong((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0L;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code float}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code float} that the {@link Object} is estimated to be.
	 */
	public static final float asFloat(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).longValue();
		} else {
			if (o instanceof String) {
				try {
					return Float.parseFloat((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0.0F;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@code double}.
	 *
	 * @param o The {@link Object} to reconsider, nullable.
	 * @return A {@code double} that the {@link Object} is estimated to be.
	 */
	public static final double asDouble(Object o) {
		o = helpDecode(o);

		if (o instanceof Number) {
			return ((Number) o).doubleValue();
		} else {
			if (o instanceof String) {
				try {
					return Double.parseDouble((String) o);
				} catch (final NumberFormatException ignore) {}
			}

			return 0.0D;
		}
	}

	/**
	 * Tries to consider an {@link Object} as a {@link Enum} constant.
	 *
	 * @param type The {@link Enum} {@link Class}.
	 * @param o The {@link Object} to reconsider, nullable.
	 * @param <E> The type of {@link Enum}.
	 * @return A {@link Enum} that the {@link Object} is estimated to be,
	 *         or {@code null} if the reconsidering failed.
	 */
	public static final <E extends Enum<E>> Enum<E> asEnum(final Class<E> type, Object o) {
		o = helpDecode(o);

		if (o instanceof String) {
			return Enum.valueOf(type, (String) o);
		} else {
			final E[] constants = type.getEnumConstants();
			final int index = o instanceof Number ? ((Number) o).intValue() :
					o instanceof Enum ? ((Enum) o).ordinal() : 0;

			return index < constants.length ? constants[index] : null;
		}
	}

	@Deprecated
	private Coerce() { throw new UnsupportedOperationException(); }
}
