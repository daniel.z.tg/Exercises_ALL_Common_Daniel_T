package me.danielzgtg.compsci11_sem2_2017.common.ui;

import javax.swing.JPanel;

import java.awt.GridLayout;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A wrapper for dealing with {@link GridLayout}ed {@link JPanel}s.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class GridManager {

	/**
	 * The width of the grid.
	 */
	public final int width;

	/**
	 * The height of the grid.
	 */
	public final int height;

	/**
	 * The {@link GridLayout}ed {@link JPanel}.
	 */
	public final JPanel gridPanel;

	/**
	 * The cell {@link JPanel}s of the {@link GridLayout}ed {@link JPanel}.
	 */
	private final JPanel[] cellPanels;

	/**
	 * Creates a new {@link GridManager}.
	 *
	 * @param width The width of the grid.
	 * @param height The height of the grid.
	 */
	public GridManager(final int width, final int height) {
		Validate.require(width > 0 && height > 0);

		// Setup backend
		this.gridPanel = SwingUtils.createGrid(width, height);

		// Cell Panels
		final int area = width * height;
		this.cellPanels = new JPanel[area];

		// Fill Cell Panel Array
		for (int i = 0; i < area; i++) {
			final JPanel cellPanel = new JPanel();
			SwingUtils.enableJPanelGrowth(cellPanel);

			this.cellPanels[i] = cellPanel;
			this.gridPanel.add(cellPanel);
		}

		// Store Parameters to Fields
		this.width = width;
		this.height = height;
	}

	/**
	 * Gets the width of the grid.
	 *
	 * @return The width of the grid.
	 */
	public final int getWidth() {
		return this.width;
	}

	/**
	 * Gets the height of the grid.
	 *
	 * @return The height of the grid.
	 */
	public final int getHeight() {
		return this.height;
	}

	/**
	 * Gets the {@link GridLayout}ed {@link JPanel}.
	 *
	 * @return The {@link GridLayout}ed {@link JPanel}.
	 */
	public final JPanel getGridPanel() {
		return this.gridPanel;
	}

	/**
	 * Gets the cell {@link JPanel} at the specified grid coordinates.
	 *
	 * @param x The x-coordinate in the grid.
	 * @param y The y-coordinate in the grid.
	 * @return The cell {@link JPanel} at the specified grid coordinates.
	 * @throws IllegalArgumentException If the coordinates are not in the grid.
	 */
	public final JPanel getCellPanel(final int x, final int y) {
		Validate.require(x >= 0 && y >= 0 && x < this.width && y < this.height);

		return this.cellPanels[(y * this.width) + x];
	}

}
