package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Map;
import java.util.function.Function;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.io.InputStream;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;

/**
 * Useful tools for having consistent Java Swing GUI widgets.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings({"SameParameterValue", "unused", "WeakerAccess"})
public final class Widgets {

	/**
	 * The width of the {@link JFrame} of the simple info box.
	 */
	private final int infoBoxWidth;

	/**
	 * The height of the {@link JFrame} of the simple info box.
	 */
	private final int infoBoxHeight;

	/**
	 * The size of simple horizontal spacer.
	 */
	private final int horizontalSpacerSize;

	/**
	 * The size of simple vertical spacer.
	 */
	private final int verticalSpacerSize;

	/**
	 * The default text for regular {@link JLabel}s.
	 */
	private final String defaultRegularLabelText;

	/**
	 * The default {@link Font} for regular {@link JLabel}s.
	 */
	private final Font defaultRegularLabelFont;

	/**
	 * The default text for title {@link JLabel}s.
	 */
	private final String defaultTitleLabelText;

	/**
	 * The default {@link Font} for title {@link JLabel}s.
	 */
	private final Font defaultTitleLabelFont;

	/**
	 * The default text for regular {@link JButton}s.
	 */
	private final String defaultRegularButtonText;

	/**
	 * The default {@link Font} for regular {@link JButton}s.
	 */
	private final Font defaultRegularButtonFont;

	/**
	 * Creates a consistent {@link Widgets} tool.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param loader The resource loader the {@code path} refers to.
	 * @throws IllegalArgumentException If the specified parameters could not create a proper consistent widgets tool.
	 */
	@SuppressWarnings("MagicConstant")
	public Widgets(final Map<?, ?> constantsMap, final Function<String, InputStream> loader) {
		Validate.notNull(constantsMap);
		Validate.notNull(loader);

		this.infoBoxWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.infoBoxWidth")));
		this.infoBoxHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.infoBoxHeight")));
		this.horizontalSpacerSize = Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.horizontalSpacerSize"));
		this.verticalSpacerSize = Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.verticalSpacerSize"));
		this.defaultRegularLabelText = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularLabelText"));
		this.defaultTitleLabelText = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultTitleLabelText"));
		this.defaultRegularButtonText = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularButtonText"));
		try {
			this.defaultRegularLabelFont = ResourceUtils.loadTTFSafe(Coerce.asString(constantsMap.get(
					"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularLabelFontPath")), loader)
					.deriveFont(Coerce.asInt(constantsMap.get(
							"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularLabelFontStyle")),
							Coerce.asInt(constantsMap.get(
									"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularLabelFontSize")));
			this.defaultTitleLabelFont = ResourceUtils.loadTTFSafe(Coerce.asString(constantsMap.get(
					"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultTitleLabelFontPath")), loader)
					.deriveFont(Coerce.asInt(constantsMap.get(
							"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultTitleLabelFontStyle")),
							Coerce.asInt(constantsMap.get(
									"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultTitleLabelFontSize")));
			this.defaultRegularButtonFont = ResourceUtils.loadTTFSafe(Coerce.asString(constantsMap.get(
					"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularButtonFontPath")), loader)
					.deriveFont(Coerce.asInt(constantsMap.get(
							"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularButtonFontStyle")),
							Coerce.asInt(constantsMap.get(
									"me.danielzgtg.compsci11_sem2_2017.common.widgets.defaultRegularButtonFontSize")));

		} catch (final Exception e) {
			throw new IllegalArgumentException("Consistent Widgets failed to load!", e);
		}
	}

	/**
	 * Shows an info window for the data.
	 *
	 * @param title The title of the window.
	 * @param data The info text to show.
	 */
	public final void simplyShowInfoBox(final String title, final String data) {
		this.simplyShowInfoBox(title, data, null);
	}

	/**
	 * Shows an info window for the data.
	 *
	 * @param title The title of the window.
	 * @param data The info text to show.
	 * @param parent The {@link Component} the launch the {@link JFrame} relative to.
	 */
	public final void simplyShowInfoBox(final String title, final String data, final Component parent) {
		final JFrame frame = SwingUtils.generateOnetimeJFrame(title);

		final JTextArea textArea = new JTextArea(data);
		textArea.setEditable(false);

		SwingUtils.setupJFrameContents(frame, new Component[] {
				SwingUtils.wrapWithScrolling(textArea)
		}, (panel) -> new BorderLayout());

		SwingUtils.finishJFrameSetup(frame, this.infoBoxWidth, this.infoBoxHeight);

		SwingUtils.launchJFrame(frame, parent);
	}

	/**
	 * Creates a new spacer.
	 *
	 * @param vertical {@code true} if the spacer should be along the y-axis, else the x-axis.
	 * @return A new {@link Component} that can be used as a spacer.
	 */
	public final Component createSimpleSpacer(final boolean vertical) {
		return Box.createRigidArea(vertical ? new Dimension(0, this.verticalSpacerSize) :
				new Dimension(this.horizontalSpacerSize, 0));
	}

	/**
	 * Makes a {@link JLabel} regular and look style consistent with others using this method.
	 *
	 * @param label The {@link JLabel} to apply style on.
	 */
	public final void resetRegularLabel(final JLabel label) {
		Validate.notNull(label);

		label.setText(this.defaultRegularLabelText);
		label.setFont(this.defaultRegularLabelFont);
		label.setIcon(null);
	}

	/**
	 * Creates a regular {@link JLabel}, style consistent with others using {@link Widgets#resetRegularLabel(JLabel)}.
	 *
	 * @return A regular {@link JLabel}, style consistent with others using {@link Widgets#resetRegularLabel(JLabel)}.
	 */
	public final JLabel createRegularLabel() {
		final JLabel result = new JLabel();

		this.resetRegularLabel(result);

		return result;
	}

	/**
	 * Creates a regular {@link JLabel}, style consistent with others using {@link Widgets#resetRegularLabel(JLabel)},
	 * with some initial text.
	 *
	 * @param text The initial text.
	 * @return A regular {@link JLabel}, style consistent with others using {@link Widgets#resetRegularLabel(JLabel)}.
	 */
	public final JLabel createRegularLabel(final String text) {
		final JLabel result = new JLabel();

		this.resetRegularLabel(result);
		result.setText(text);

		return result;
	}

	/**
	 * Makes a {@link JLabel} title-like and look style consistent with others using this method.
	 *
	 * @param label The {@link JLabel} to apply style on.
	 */
	public final void resetTitleLabel(final JLabel label) {
		Validate.notNull(label);

		label.setText(this.defaultTitleLabelText);
		label.setFont(this.defaultTitleLabelFont);
		label.setIcon(null);
	}

	/**
	 * Creates a title {@link JLabel}, style consistent with others using {@link Widgets#resetTitleLabel(JLabel)}.
	 *
	 * @return A title {@link JLabel}, style consistent with others using {@link Widgets#resetTitleLabel(JLabel)}.
	 */
	public final JLabel createTitleLabel() {
		final JLabel result = new JLabel();

		this.resetTitleLabel(result);

		return result;
	}

	/**
	 * Creates a title {@link JLabel}, style consistent with others using {@link Widgets#resetTitleLabel(JLabel)},
	 * with some initial text.
	 *
	 * @param text The initial text.
	 * @return A regular {@link JLabel}, style consistent with others using {@link Widgets#resetTitleLabel(JLabel)}.
	 */
	public final JLabel createTitleLabel(final String text) {
		final JLabel result = new JLabel();

		this.resetTitleLabel(result);
		result.setText(text);

		return result;
	}

	/**
	 * Makes a {@link JButton} regular and look style consistent with others using this method.
	 *
	 * @param button The {@link JButton} to apply style on.
	 */
	public final void resetRegularButton(final JButton button) {
		Validate.notNull(button);

		button.setText(this.defaultRegularButtonText);
		button.setFont(this.defaultRegularButtonFont);
		button.setIcon(null);
	}

	/**
	 * Creates a regular {@link JButton}, style consistent with others using {@link Widgets#resetRegularButton(JButton)}.
	 *
	 * @return A regular {@link JButton}, style consistent with others using {@link Widgets#resetRegularButton(JButton)}.
	 */
	public final JButton createRegularButton() {
		final JButton result = new JButton();

		this.resetRegularButton(result);

		return result;
	}

	/**
	 * Creates a regular {@link JButton}, style consistent with others using {@link Widgets#resetRegularButton(JButton)},
	 * with some initial text.
	 *
	 * @param text The initial text.
	 * @return A regular {@link JButton}, style consistent with others using {@link Widgets#resetRegularButton(JButton)}.
	 */
	public final JButton createRegularButton(final String text) {
		final JButton result = new JButton();

		this.resetRegularButton(result);
		result.setText(text);

		return result;
	}
}
