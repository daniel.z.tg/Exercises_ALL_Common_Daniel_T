package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Map;
import java.util.function.Consumer;

import java.io.PrintWriter;
import java.io.StringWriter;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A workaround to prevent simple programs from crashing, by restarting them if necessary.
 * Only useful for single-threaded CLI programs, and may cause poorly-written programs to show state inconsistency.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class Launcher {

	/**
	 * The message to print when the method completes normally.
	 */
	private final String loopSuccessMsg;

	/**
	 * The message to print when an uncaught exception is detected from the method.
	 */
	private final String loopErrorMsg;

	/**
	 * The message to print before the method is re-launched.
	 */
	private final String loopRestartMsg;

	/**
	 * The message when an uncaught exception is detected from a safelaunched method.
	 */
	private final String safelaunchFailMsg;

	/**
	 * The layout for safelaunch exception stack traces.
	 */
	private final String safelaunchStacktraceFormat;

	/**
	 * Prompt layout for asking the user if they want to restart a crashed method.
	 */
	private final String safelaunchCrashRestartPrompt;

	/**
	 * The message to print when the method has crashed, but the user does not want to retry.
	 */
	private final String safelaunchCrashAbortMsg;

	/**
	 * The message to print when the method has crashed, and the user wants to retry.
	 */
	private final String safelaunchCrashRetryMsg;

	/**
	 * Prompt layout for asking the user if they want to restart a ended method, when in looping mode.
	 */
	private final String safelaunchLoopRestartPrompt;

	/**
	 * The message to print when the method has ended, but the user does not want to restart.
	 */
	private final String safelaunchLoopQuitMsg;

	/**
	 * The message to print when the method has ended, and the user wants to restart.
	 */
	private final String safelaunchLoopRestartMsg;

	/**
	 * The {@link Prompter} for obtaining user input.
	 */
	private final Prompter prompter;

	/**
	 * The {@link PrintMessenger} for showing the user some data.
	 */
	private final PrintMessenger messenger;

	/**
	 * Creates a new {@link Launcher}, using the specified parameters.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param prompter The {@link Prompter} to obtain input with, not null.
	 * @param messenger The {@link PrintMessenger} to show the user some data with, not null.
	 */
	public Launcher(final Map<?, ?> constantsMap, final Prompter prompter, final PrintMessenger messenger) {
		Validate.notNull(constantsMap);
		Validate.notNull(prompter);
		Validate.notNull(messenger);

		this.loopSuccessMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.loopSuccessMsg"));
		this.loopErrorMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.loopErrorMsg"));
		this.loopRestartMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.loopRestartMsg"));
		this.safelaunchFailMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchFailMsg"));
		this.safelaunchStacktraceFormat = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchStacktraceFormat"));
		this.safelaunchCrashRestartPrompt = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchCrashRestartPrompt"));
		this.safelaunchCrashAbortMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchCrashAbortMsg"));
		this.safelaunchCrashRetryMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchCrashRetryMsg"));
		this.safelaunchLoopRestartPrompt = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchLoopRestartPrompt"));
		this.safelaunchLoopQuitMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchLoopQuitMsg"));
		this.safelaunchLoopRestartMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.launchutils.safelaunchLoopRestartMsg"));

		this.prompter = prompter;
		this.messenger = messenger;
	}

	/**
	 * Launches a Java main-like method, and detects crashes.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 * @return {@code true} if the method returned successfully.
	 */
	public final boolean safeLaunchWrappedMain(final Consumer<String[]> wrappedMain, final String[] args) {
		try {
			wrappedMain.accept(args);

			return true;
		} catch (final Throwable t) {
			return false;
		}
	}

	/**
	 * Launches a Java main-like method, repeating it if it finishes.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 */
	public final void loopLaunchWrappedMain(final Consumer<String[]> wrappedMain, final String[] args) {
		this.loopLaunchWrappedMain(wrappedMain, args,
				this.loopSuccessMsg, this.loopErrorMsg,
				this.loopRestartMsg);
	}

	/**
	 * Launches a Java main-like method, repeating it if it finishes.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 * @param successMsg The message printed when the method completes normally.
	 * @param errorMsg The message printed when an uncaught exception is detected from the method.
	 * @param restartMsg The message printed before the method is re-launched.
	 */
	public final void loopLaunchWrappedMain(final Consumer<String[]> wrappedMain, final String[] args,
			final String successMsg, final String errorMsg, final String restartMsg) {
		// noinspection InfiniteLoopStatement // IntelliJ
		while (true) {
			try {
				wrappedMain.accept(args);

				System.out.print(successMsg);
			} catch (final Throwable t) {
				System.out.print(errorMsg);
			}

			System.out.print(restartMsg);
		}
	}

	/**
	 * Launches a Java main-like method, and if a crash is detected,
	 * asks in the console if the user would like to retry invoking the method.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 */
	public final void safeLaunchConsoleWrappedMainLooping(
			final Consumer<String[]> wrappedMain, final String[] args) {

		this.commonInternalRelaunchingMethod(wrappedMain, args,
				this.safelaunchFailMsg, this.safelaunchStacktraceFormat,
				this.safelaunchCrashRestartPrompt, this.safelaunchCrashAbortMsg,
				this.safelaunchCrashRetryMsg, false);
	}

	/**
	 * Launches a Java main-like method, and if a crash is detected,
	 * asks in the console if the user would like to retry invoking the method.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 * @param failDetectMsg The message when an uncaught exception is detected.
	 * @param stackTraceFormat The layout for safelaunch exception stack traces.
	 * @param retryPrompt Prompt layout for asking the user if they want to retry.
	 * @param quitMsg The message to print if the user does not want to retry.
	 * @param retryMsg The message to print if the user wants to retry.
	 */
	public final void safeLaunchConsoleWrappedMainLooping(
			final Consumer<String[]> wrappedMain, final String[] args,
			final String failDetectMsg, final String stackTraceFormat,
			final String retryPrompt, final String quitMsg,
			final String retryMsg) {
		this.commonInternalRelaunchingMethod(wrappedMain, args,
				failDetectMsg, stackTraceFormat,
				retryPrompt, quitMsg,
				retryMsg, false);
	}

	/**
	 * Launches a Java main-like method, and asks in the console if the user would like to invoke the method again.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 */
	public final void loopingLaunchConsoleWrappedMainSafe(
			final Consumer<String[]> wrappedMain, final String[] args) {
		this.commonInternalRelaunchingMethod(wrappedMain, args,
				this.safelaunchFailMsg, this.safelaunchStacktraceFormat,
				this.safelaunchLoopRestartPrompt, this.safelaunchLoopQuitMsg,
				this.safelaunchLoopRestartMsg, true);
	}

	/**
	 * Launches a Java main-like method, and asks in the console if the user would like to invoke the method again.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 * @param failDetectMsg The message when an uncaught exception is detected.
	 * @param stackTraceFormat The layout for safelaunch exception stack traces.
	 * @param restartPrompt Prompt layout for asking the user if they want to restart.
	 * @param quitMsg The message to print if the user does not want to restart.
	 * @param restartMsg The message to print if the user wants to restart.
	 */
	public final void loopingLaunchConsoleWrappedMainSafe(
			final Consumer<String[]> wrappedMain, final String[] args,
			final String failDetectMsg, final String stackTraceFormat,
			final String restartPrompt, final String quitMsg,
			final String restartMsg) {
		this.commonInternalRelaunchingMethod(wrappedMain, args,
				failDetectMsg, stackTraceFormat,
				restartPrompt, quitMsg,
				restartMsg, true);
	}

	/**
	 * Method that is used by the other launch methods.
	 *
	 * @param wrappedMain The main-like method function.
	 * @param args The main-like method arguments.
	 * @param failDetectMsg The message when an uncaught exception is detected.
	 * @param stackTraceFormat The layout for safelaunch exception stack traces.
	 * @param relaunchPrompt Prompt layout for asking the user if they want to relaunch.
	 * @param quitMsg The message to print if the user does not want to relaunch.
	 * @param relaunchMsg The message to print if the user wants to relaunch.
	 * @param loopLaunch Whether to show relaunch options on a successful exit.
	 */
	private final void commonInternalRelaunchingMethod(
			final Consumer<String[]> wrappedMain, final String[] args,
			final String failDetectMsg, final String stackTraceFormat, final String relaunchPrompt,
			final String quitMsg, final String relaunchMsg, final boolean loopLaunch) {
		while (true) {
			try {
				wrappedMain.accept(args);

				if (!loopLaunch) return;
			} catch (final Throwable t) {
				this.messenger.print(failDetectMsg);

				// Get a String of what would have been dumped to console.
				final StringWriter sw = new StringWriter();
				t.printStackTrace(new PrintWriter(sw));
				final String stackTraceJoined = sw.toString();

				// Append a prefix, and print it to console, hopefully with less panic.
				for (final String line : stackTraceJoined.split("\n")) {
					this.messenger.format(stackTraceFormat, line);
				}
			}

			// Ask the user if they want to retry the main method, and do so.
			if (!this.prompter.promptBoolean(relaunchPrompt)) {
				System.out.print(quitMsg);

				return;
			} else {
				System.out.print(relaunchMsg);
			}
		}
	}
}
