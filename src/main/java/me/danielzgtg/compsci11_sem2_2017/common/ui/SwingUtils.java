package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;

/**
 * Useful tools for making a Java Swing GUI.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings({"SameParameterValue", "unused", "WeakerAccess", "UnusedReturnValue"})
public final class SwingUtils {

	/**
	 * The minimum size a {@link JFrame} should be resized to.
	 */
	public static final int MAGIC_MINIMUM_GOOD_JFRAME_SIZE = 12;

	/**
	 * Produces a titled {@link JFrame} that does not keep the JVM alive after it is closed.
	 *
	 * @param title The title of the {@link JFrame}.
	 * @return The produced {@link JFrame}.
	 */
	public static final JFrame generateOnetimeJFrame(final String title) {
		Validate.notNull(title);

		final JFrame result = new JFrame(title);

		result.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		return result;
	}

	/**
	 * Adds a callback for deciding whether to allow window closing.
	 *
	 * @param frame The {@link JFrame} to listen for closing.
	 * @param callback The callback for deciding whether to allow window closing.
	 */
	public static final void hookJFrameClosing(final JFrame frame, final BooleanSupplier callback) {
		Validate.notNull(frame);
		Validate.notNull(callback);

		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(final WindowEvent e) {}

			@Override
			public void windowClosing(final WindowEvent e) {
				if (callback.getAsBoolean()) {
					frame.dispose();
				}
			}

			@Override
			public void windowClosed(final WindowEvent e) {}

			@Override
			public void windowIconified(final WindowEvent e) {}

			@Override
			public void windowDeiconified(final WindowEvent e) {}

			@Override
			public void windowActivated(final WindowEvent e) {}

			@Override
			public void windowDeactivated(final WindowEvent e) {}
		});
	}

	/**
	 * Sets up the contents of a {@link JFrame}, using the provided {@link Component}s,
	 * and {@link LayoutManager}. Adds a {@link JPanel} in the process.
	 *
	 * @param frame The JFrame to set up.
	 * @param contents The contents to put inside the JFrame.
	 * @param layoutMgrGenerator A callback to produce a layout manager.
	 * @return The JPanel created inside.
	 */
	public static final JPanel setupJFrameContents(final JFrame frame, final Component[] contents,
			final Function<JPanel, LayoutManager> layoutMgrGenerator) {
		Validate.notNull(contents, "Cannot iterate over a null content array!");
		Validate.notNull(frame, "Null JFrames cannot be set up!");
		Validate.notNull(layoutMgrGenerator, "Null layout manager generator callback cannot be used!");

		final JPanel result = new JPanel();

		// Ask for a non-null layout manager
		final LayoutManager layoutMgr = layoutMgrGenerator.apply(result);
		Validate.notNull(layoutMgr, "This method will not apply a null layout!");

		// then apply it
		result.setLayout(layoutMgr);

		for (final Component component : contents) {
			Validate.notNull(component, "This method will not add a null component!");
			result.add(component);
		}

		// Add the JPanel for actual displaying
		frame.add(result);
		return result;
	}

	/**
	 * Prepares a {@link JFrame} for displaying to the user, by applying layout and size.
	 *
	 * @param frame The target JFrame
	 * @param width The preferred width.
	 * @param height The preferred height.
	 */
	public static final void finishJFrameSetup(final JFrame frame,
			final int width, final int height) {
		Validate.notNull(frame, "A null JFrame cannot be used!");

		frame.setPreferredSize(new Dimension(width, height));
		frame.pack();
	}

	/**
	 * Displays a {@link JFrame} to the user, nicely.
	 *
	 * @param frame The target {@link JFrame}.
	 */
	public static final void launchJFrame(final JFrame frame) {
		launchJFrame(frame, null);
	}

	/**
	 * Displays a {@link JFrame} to the user, nicely.
	 *
	 * @param frame The target {@link JFrame}.
	 * @param parent The {@link Component} the launch the {@link JFrame} relative to.
	 */
	public static final void launchJFrame(final JFrame frame, final Component parent) {
		Validate.notNull(frame, "A null JFrame cannot be used!");

		frame.pack();
		frame.setLocationRelativeTo(parent);
		frame.setVisible(true);
	}

	/**
	 * Adds a listener for {@link Document} updates.
	 *
	 * @param document The  {@link Document} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookDocumentGenericUpdate(
			final Document document, final Consumer<DocumentEvent> listener) {
		Validate.notNull(document, "Document cannot be null!");
		Validate.notNull(listener, "Document update listener cannot be null!");

		document.addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(final DocumentEvent e) {
				listener.accept(e);
			}

			@Override
			public void removeUpdate(final DocumentEvent e) {
				listener.accept(e);
			}

			@Override
			public void changedUpdate(final DocumentEvent e) {
				listener.accept(e);
			}
		});
	}

	/**
	 * Adds a listener for {@link JButton} presses.
	 *
	 * @param button The {@link JButton} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookButtonPress(
			final JButton button, final Runnable listener) {
		Validate.notNull(button, "JButton cannot be null!");
		Validate.notNull(listener, "Listener cannot be null!");

		button.addActionListener((event) -> listener.run());
	}

	/**
	 * Adds a listener for {@link JCheckBox} toggles.
	 *
	 * @param checkBox The {@link JCheckBox} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookCheckboxToggle(
			final JCheckBox checkBox, final Consumer<Boolean> listener) {
		Validate.notNull(checkBox, "JButton cannot be null!");
		Validate.notNull(listener, "Listener cannot be null!");

		checkBox.addActionListener((event) -> listener.accept(((AbstractButton) event.getSource()).isSelected()));
	}

	/**
	 * Sets the look and feel for the platform.
	 */
	public static final void setupSystemLookAndFeel() {
		Log.log("[SwingUtils] Trying to set platform look and feel.");

		try {
			final PlatformUtils.OperatingSystem os = PlatformUtils.getOS();

			if (os == PlatformUtils.OperatingSystem.LINUX) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			} else if (os == PlatformUtils.OperatingSystem.WINDOWS) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} else if (os == PlatformUtils.OperatingSystem.MAC) {
				// MAC has own L&F
				Log.log("[SwingUtils] macOS L&F already set??");
			} else {
				Log.log("[SwingUtils] Did not set Look and Feel (Unknown OS!)");
			}
		} catch (final Exception e) {
			Log.log("[SwingUtils] Failed to set Look and Feel!");
			Log.log(e);
		}

		Log.log("[SwingUtils] Setting Look and Feel finished without error.");
	}

	/**
	 * Wraps the {@link Component} in a {@link JPanel}, so that it does not get collapsed.
	 *
	 * @param c The {@link Component} to wrap.
	 * @return A {@link JPanel} wrapping the component.
	 */
	public static final JPanel wrapInGrowingJPanel(final Component c) {
		Validate.notNull(c, "Need a Component to wrap!");

		//final JPanel result = new JPanel();
		final JPanel result = new JPanel(new BorderLayout());

		// BorderLayout grows
		//SwingUtils.enableJPanelGrowth(result);
		result.add(c);

		return result;
	}

	/**
	 * Gets a separating {@link Component} that will allow elements to move apart, when place in between.
	 *
	 * @return A separating {@link Component} that will allow elements to move apart, when place in between.
	 */
	public static final Component generateSeparatingComponent() {
		//final JPanel result = new JPanel();
		//final JPanel result = new JPanel(new BorderLayout());

		// BorderLayout grows
		//SwingUtils.enableJPanelGrowth(result);

		//return result;
		return new JPanel(new BorderLayout());
	}

	/**
	 * Wraps the {@link Component} in a {@link JPanel}, so that it gets centered.
	 *
	 * @param c The {@link Component} to wrap.
	 * @return A {@link JPanel} wrapping the component.
	 */
	public static final JPanel centerInJPanel(final Component c) {
		Validate.notNull(c, "Need a Component to wrap!");


		//final JPanel result = new JPanel();
		final JPanel result = new JPanel(new GridBagLayout());

		// GridBagLayout centers
		//result.setLayout(new GridBagLayout());
		result.add(c);

		return result;
	}

	/**
	 * Wraps the {@link Component} in a {@link JScrollPane}, which allows scrolling.
	 *
	 * @param c The {@link Component} to wrap.
	 * @return A {@link JScrollPane} wrapping the component.
	 */
	public static final JScrollPane wrapWithScrolling(final Component c) {
		Validate.notNull(c, "Need a Component to wrap!");

		return new JScrollPane(c,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//final JScrollPane result = new JScrollPane();
		//
		//result.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//result.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		//result.setViewportView(c);
		//
		//return result;
	}

	/**
	 * Divides two {@link Component}s into a {@link JScrollPane}, space distributed evenly.
	 *
	 * @param horizontal {@link true} if the division should be horizontal, else vertical.
	 * @param first The first {@link Component}.
	 * @param second The second {@link Component}.
	 * @return A {@link JScrollPane} dividing the components.
	 */
	public static final JSplitPane wrapDivideContentsEven(
			final boolean horizontal,
			final Component first, final Component second) {
		Validate.notNull(first, "First wrapped Component must not be null!");
		Validate.notNull(second, "Second wrapped Component must not be null!");

		//final JSplitPane result = new JSplitPane();
		final JSplitPane result  = new JSplitPane(
				horizontal ? JSplitPane.HORIZONTAL_SPLIT : JSplitPane.VERTICAL_SPLIT,
				first, second
		);

		//result.setOrientation(horizontal ? JSplitPane.HORIZONTAL_SPLIT : JSplitPane.VERTICAL_SPLIT);
		//result.setTopComponent(first);
		//result.setBottomComponent(second);

		result.setResizeWeight(0.5D);
		result.setContinuousLayout(PlatformUtils.getHighPerformance());

		return result;
	}

	/**
	 * Forces the {@link Dimension}s on the {@link Component}
	 * to convince {@link LayoutManager}s to use them.
	 *
	 * @param target The {@link Component} to force the size on.
	 * @param size The desired {@link Dimension} size.
	 */
	public static final void forceDimensionSize(final Component target, final Dimension size) {
		Validate.notNull(target, "Need a Component to act on!");
		Validate.notNull(size, "Need a Dimension to set!");

		target.setMinimumSize(size);
		target.setPreferredSize(size);
		target.setMaximumSize(size);
	}

	/**
	 * Adds a listener for {@link JFrame} closing.
	 *
	 * @param target The {@link JFrame} to listen to.
	 * @param callback The callback to run upon the closing.
	 */
	public static final void hookFrameClosing(final JFrame target, final Runnable callback) {
		Validate.notNull(target, "Need a JFrame to hook onto!");
		Validate.notNull(callback, "Need a callback to install!");

		target.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(final WindowEvent e) {}

			@Override
			public void windowClosing(final WindowEvent e) {
				callback.run();
			}

			@Override
			public void windowClosed(final WindowEvent e) {}

			@Override
			public void windowIconified(final WindowEvent e) {}

			@Override
			public void windowDeiconified(final WindowEvent e) {}

			@Override
			public void windowActivated(final WindowEvent e) {}

			@Override
			public void windowDeactivated(final WindowEvent e) {}
		});
	}

	/**
	 * Replaces the font family in a {@link Component} using a {@link Font} derived using
	 * {@link GraphicsUtils#copyFontSettings(Font, Font)}.
	 *
	 * @param target The {@link Component} to update.
	 * @param base The {@link Font} to copy the font family from.
	 */
	public static final void replaceFontFamily(final Component target, final Font base) {
		Validate.notNull(target);
		Validate.notNull(base);

		target.setFont(GraphicsUtils.copyFontSettings(base, target.getFont()));
	}

	/**
	 * Replaces the font size in a {@link Component} using a {@link Font} derived using
	 * {@link Font#deriveFont(float)}.
	 *
	 * @param target The {@link Component} to update.
	 * @param newSize The new font size.
	 */
	public static final void replaceFontSize(final Component target, final float newSize) {
		Validate.notNull(target);

		target.setFont(target.getFont().deriveFont(newSize));
	}

	/**
	 * Disables the editing of the specified {@link JTable}.
	 *
	 * @param table The {@link JTable} to disable editing of.
	 */
	public static final void disableTableEditing(final JTable table) {
		Validate.notNull(table);

		table.setDefaultEditor(Object.class, null);
	}

	/**
	 * Sets the {@link JPanel}'s title, by setting its border.
	 *
	 * @param panel The {@link JPanel} to set the border on, not null.
	 * @param title The title to set. {@code null} removes the border that would contain a title.
	 * @return The {@link Border} that was set on the {@link JPanel}.
	 *         Will be the result of {@link BorderFactory#createEmptyBorder()} if the title was {code null},
	 *         else {@link TitledBorder#TitledBorder(String)}.
	 */
	public static final Border setTitle(final JPanel panel, final String title) {
		Validate.notNull(panel);

		final Border result = title == null ?
				BorderFactory.createEmptyBorder() :
				new TitledBorder(title);

		panel.setBorder(result);

		return result;
	}

	/**
	 * Sets the {@link JPanel}'s flow direction, and enables it to flow, by setting a {@link BoxLayout} on it.
	 *
	 * @param panel The {@link JPanel} to set the layout on.
	 * @param horizontal {@code true} if the flow should be across the x-axis, else the y-axis.
	 */
	public static final void setFlowDirection(final JPanel panel, final boolean horizontal) {
		Validate.notNull(panel);

		panel.setLayout(new BoxLayout(panel, horizontal ? BoxLayout.X_AXIS : BoxLayout.Y_AXIS));
	}

	/**
	 * Enables the {@link JPanel} to grow, by setting its layout to a {@link BorderLayout}.
	 *
	 * @param panel The {@link JPanel} to set the layout on.
	 */
	public static final void enableJPanelGrowth(final JPanel panel) {
		Validate.notNull(panel);

		panel.setLayout(new BorderLayout());
	}

	/**
	 * Creates a {@link JPanel} laid out as a grid.
	 * This uses {@link GridLayout} to do so.
	 *
	 * @param width The width of the grid, {@code 0} if it should grow instead.
	 * @param height The height of the grid, {@code 0} if it should grow instead.
	 * @return A {@link JPanel} laid out as a grid.
	 */
	public static final JPanel createGrid(final int width, final int height) {
		Validate.require(width >= 0 && height >= 0);

		final JPanel result = new JPanel();
		result.setLayout(new GridLayout(height, width));

		return result;
	}

	/**
	 * Makes a {@link JButton} transparent.
	 *
	 * @param button The {@link JButton} to make transparent.
	 */
	public static final void makeTransparent(final JButton button) {
		Validate.notNull(button);

		button.setOpaque(false);
		button.setBorderPainted(false);
		button.setContentAreaFilled(false);
	}

	/**
	 * Adds a listener for {@link Component} key code typing events.
	 *
	 * @param component The {@link Component} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookKeyCodeType(final Component component, final Consumer<Integer> listener) {
		Validate.notNull(component);
		Validate.notNull(listener);

		component.setFocusable(true);
		component.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}

			@Override
			public void keyPressed(final KeyEvent e) {}

			@Override
			public void keyReleased(final KeyEvent e) {}
		});
	}

	/**
	 * Adds a listener for {@link Component} key code up events.
	 *
	 * @param component The {@link Component} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookKeyCodeUp(final Component component, final Consumer<Integer> listener) {
		Validate.notNull(component);
		Validate.notNull(listener);

		component.setFocusable(true);
		component.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {}

			@Override
			public void keyPressed(final KeyEvent e) {}

			@Override
			public void keyReleased(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}
		});
	}

	/**
	 * Adds a listener for {@link Component} key code down events.
	 *
	 * @param component The {@link Component} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final void hookKeyCodeDown(final Component component, final Consumer<Integer> listener) {
		Validate.notNull(component);
		Validate.notNull(listener);

		component.setFocusable(true);
		component.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {}

			@Override
			public void keyPressed(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}

			@Override
			public void keyReleased(final KeyEvent e) {}
		});
	}

	@Deprecated
	private SwingUtils() { throw new UnsupportedOperationException(); }
}
