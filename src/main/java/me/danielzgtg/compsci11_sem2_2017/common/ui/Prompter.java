package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import java.io.InputStream;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Useful tools for prompting input from a CLI.
 *
 * @author Daniel Tang
 * @since 3 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class Prompter {

	/**
	 * The error message when the user did not enter a valid integer.
	 */
	private final String errorMsgIntUnknown;

	/**
	 * The error message when the user did not enter a valid natural integer.
	 */
	private final String errorMsgIndexUnknown;

	/**
	 * The error message when the user did not enter a valid boolean.
	 */
	private final String errorMsgBooleanUnknown;

	/**
	 * The message before prompting the user again.
	 */
	private final String tryAgainPrompt;

	/**
	 * The lowercase list of aliases that the user could choose a "yes" answer with.
	 */
	private final List<String> booleanNamesTrue;

	/**
	 * The lowercase list of aliases that the user could choose a "no" answer with.
	 */
	private final List<String> booleanNamesFalse;

	/**
	 * The {@link Scanner} to obtain input with.
	 */
	private final Scanner scanner;

	/**
	 * The output to prompt input with.
	 */
	private final PrintMessenger out;

	/**
	 * Creates a new prompter.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param in The input to obtain input with, not null.
	 * @param out The output to prompt input with, not null.
	 */
	@SuppressWarnings("unchecked")
	public Prompter(final Map<?, ?> constantsMap, final InputStream in, final PrintMessenger out) {
		Validate.notNull(constantsMap);
		Validate.notNull(in);
		Validate.notNull(out);

		// Setup IO
		this.scanner = new Scanner(in);
		this.out = out;

		// Load Strings
		this.errorMsgIndexUnknown = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.unknownIndex"));
		this.errorMsgIntUnknown = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.unknownInt"));
		this.errorMsgBooleanUnknown = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.unknownBool"));
		this.tryAgainPrompt = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.tryAgain"));

		// Load String Lists

		// Populate the boolean names lists
		final Object tryTrueList = constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.trueNames");
		Validate.isType(tryTrueList, List.class);
		final List<?> trueList = new ArrayList<>((List<?>) tryTrueList);
		Validate.allAreType(trueList, String.class);
		this.booleanNamesTrue = Collections.unmodifiableList((List<String>) trueList);

		final Object tryFalseList = constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.consoleutils.falseNames");
		Validate.isType(tryFalseList, List.class);
		final List<?> falseList = new ArrayList<>((List<?>) tryFalseList);
		Validate.allAreType(falseList, String.class);
		this.booleanNamesFalse = Collections.unmodifiableList((List<String>) falseList);
	}

	/**
	 * Obtains a yes/no answer from the user.
	 *
	 * @param prompt The prompt message to prompt the user with.
	 * @return {@code true} if the user responded yes, else {@code false}.
	 */
	public final boolean promptBoolean(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		while (true) {
			this.out.print(prompt);
			final String input = this.scanner.nextLine().toLowerCase();

			if (this.booleanNamesTrue.contains(input)) { // true if it is thought as "yes"
				return true;
			} else if (this.booleanNamesFalse.contains(input)) { // false if it is thought as "no"
				return false;
			} else {
				this.out.println(this.errorMsgBooleanUnknown);
				this.out.println(this.tryAgainPrompt);
			}
		}
	}

	/**
	 * Tries to obtain a yes/no answer from the user.
	 *
	 * @param prompt The prompt message to prompt the user with.
	 * @return {@link Boolean#TRUE} if the user responded yes,
	 * 	{@link Boolean#FALSE} if the user responded no, {@code null} if the response was not understood.
	 */
	public final Boolean promptBooleanOnce(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		this.out.print(prompt);
		final String input = this.scanner.nextLine().toLowerCase();

		if (this.booleanNamesTrue.contains(input)) { // true if it is thought as "yes"
			return true;
		} else if (this.booleanNamesFalse.contains(input)) { // false if it is thought as "no"
			return false;
		} else {
			return null;
		}
	}

	/**
	 * Obtains an {@code int} from the user.
	 *
	 * @param prompt The message to prompt the user with.
	 * @return The obtained {@code int}.
	 */
	public final int promptInt(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		while (true) {
			this.out.print(prompt);
			final String input = this.scanner.nextLine();

			try {
				return Integer.parseInt(input);
			} catch (final NumberFormatException ignore) {}

			this.out.println(this.errorMsgIntUnknown);
			this.out.println(this.tryAgainPrompt);
		}
	}

	/**
	 * Obtains an positive {@code int} from the user.
	 *
	 * @param prompt The message to prompt the user with.
	 * @return The obtained positive {@code int}.
	 */
	public final int promptIndex(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		while (true) {
			this.out.print(prompt);
			final String input = this.scanner.nextLine();

			try {
				final int result = Integer.parseInt(input);

				if (result > 0) return result;
			} catch (final NumberFormatException ignore) {}

			this.out.println(this.errorMsgIndexUnknown);
			this.out.println(this.tryAgainPrompt);
		}
	}

	/**
	 * Tries to obtain an {@code Integer} from the user.
	 *
	 * @param prompt The message to prompt the user with.
	 * @return The obtained {@code Integer}, else {@code null}.
	 */
	public final Integer promptIntOnce(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		this.out.print(prompt);
		final String input = this.scanner.nextLine();

		try {
			return Integer.parseInt(input);
		} catch (final NumberFormatException nfe) {
			return null;
		}
	}

	/**
	 * Obtains a {@code String} from the user.
	 *
	 * @param prompt The message to prompt the user with.
	 * @return  The obtained {@code String}.
	 */
	public final String promptString(final String prompt) {
		Validate.notNull(prompt, "Cannot prompt with a null String!");

		this.out.print(prompt);

		return this.scanner.nextLine();
	}
}
