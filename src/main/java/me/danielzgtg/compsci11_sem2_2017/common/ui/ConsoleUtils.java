package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Useful tools for user interactions using a CLI.
 *
 * @author Daniel Tang
 * @since 3 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue", "UnusedReturnValue"})
public final class ConsoleUtils {

	/**
	 * Formats a 2d list of {@link String}s into a (when monospaced) text table {@link String},
	 * by adding dividing characters.
	 *
	 * @param data The 2d list of {@link String}s
	 * @param rowDivider The divider {@code char} between rows, and the horizontal edges.
	 * @param columnDivider The divider {@code char} between columns, and the vertical edges.
	 * @param otherDivider The divider {@code char} when the row and column dividers intersect.
	 * @param rightAlign If {@code true} spaces will be added from the left side in the cells.
	 * @return A {@link String} representing a text table when viewed in a monospace font.
	 */
	public static final String formatTable(final List<List<String>> data,
			final char rowDivider, final char columnDivider, final char otherDivider,
			final boolean rightAlign) {
		if (data == null || data.isEmpty()) {
			// Table doesn't exist when data is empty
			return "";
		}

		// Expected number of columns
		final int columnsCheck;
		{
			final List<String> firstRow = data.get(0);
			Validate.notNull(firstRow, "Null Row 0!");

			columnsCheck = firstRow.size();
		}

		if (columnsCheck <= 0) {
			// Table doesn't exist when no rows in data
			return "";
		}

		// Calculate the smallest required column size that will fit every row's data
		int[] lengths = new int[columnsCheck];
		for (int i = 0; i < data.size(); i++) {
			final List<String> row = data.get(i);

			if (row == null) {
				throw new IllegalArgumentException("Null Row " + i + "!");
			}

			if (columnsCheck != row.size()) {
				throw new IllegalArgumentException(
						"Inconsistent # of columns in table! (Row" + i + "=" + row.size()
								+ ", expected=" + columnsCheck + ")");
			}

			for (int j = 0; j < columnsCheck; j++) {
				final String text = row.get(j);

				if (text == null) {
					throw new IllegalArgumentException("Null text cell! (" + i + "," + j + ")");
				}

				lengths[j] = Math.max(text.length(), lengths[j]);
			}
		}

		final StringBuilder result = new StringBuilder();

		// Add the header
		result.append(otherDivider);
		for (final int length : lengths) {
			// Add the row divider
			result.append(StringUtils.stringWithChar(length, rowDivider));
			// Divider intersecting dividers of both row and column, between rows, (and right edge)
			result.append(otherDivider);
		}

		// Add the data rows, and the dividers below them (last row's is also footer)
		for (final List<String> row : data) {
			result.append('\n'); // Data rows responsible for newline
			result.append(columnDivider); // Left edge divider
			for (int i = 0; i < columnsCheck; i++) {
				// Add data Strings, padded to length
				result.append(StringUtils.padString(row.get(i), lengths[i], rightAlign));
				// Add column dividers (last column's is also right edge divider)
				result.append(columnDivider);
			}

			// Add the row divider (and also footer)
			result.append('\n');
			result.append(otherDivider); // Left edge divider intersecting dividers of both row and column
			for (final int length : lengths) {
				// Add the row divider
				result.append(StringUtils.stringWithChar(length, rowDivider));
				// Divider intersecting dividers of both row and column, between rows, (and right edge)
				result.append(otherDivider);
			}
		}

		return result.toString();
	}

	/**
	 * Formats an {@code int[]} into a set of ordered pairs, with the specified format.
	 *
	 * @param format The format to produce the ordered pairs with.
	 * @param array The {@code int[]} to format, will be unchanged.
	 * @param beginAtOne {@code true} to display the x-values as 1 greater than actual.
	 * @return A {@link String} representing a list of ordered pairs.
	 */
	public static final String formatArrayValues(final String format, final int[] array,
			final boolean beginAtOne) {
		Validate.notNull(format);
		Validate.notNull(array);

		final StringBuilder result = new StringBuilder();

		for (int x = 0; x < array.length; x++) {
			result.append(String.format(format, x + (beginAtOne ? 1 : 0), /*y=*/ array[x]));
			result.append('\n');
		}

		return result.toString();
	}

	/**
	 * Graphs an {@code int[]} into horizontal bar graph, with the specified format.
	 *
	 * @param format The graph to produce the ordered pairs with.
	 * @param array The {@code int[]} to graph, will be unchanged.
	 * @param stamp The {@code char} to use as graph bars.
	 * @param scale The scale to apply to the y-values.
	 * @param scaleUp {@code true} to multiply the y-values, else divide.
	 * @param beginAtOne {@code true} to display the x-values as 1 greater than actual.
	 * @return A {@link String} representing a list of ordered pairs.
	 */
	public static final String graphArrayValues(final String format, final int[] array,
			final char stamp, final int scale, final boolean scaleUp, final boolean beginAtOne) {
		Validate.notNull(format);
		Validate.notNull(array);

		final StringBuilder result = new StringBuilder();

		for (int x = 0; x < array.length; x++) {
			result.append(String.format(format, x + (beginAtOne ? 1 : 0), StringUtils.stringWithChar(
					scaleUp ? array[x] * scale : array[x] / scale, stamp)));
			result.append('\n');
		}

		return result.toString();
	}

	@Deprecated
	private ConsoleUtils() { throw new UnsupportedOperationException(); }
}
