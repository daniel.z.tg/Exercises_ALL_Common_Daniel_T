package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.io.PrintStream;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A set of wrapper methods around a {@link PrintStream}.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class PrintMessenger {

	/**
	 * The underlying {@link PrintStream} to output to.
	 */
	private final PrintStream out;

	/**
	 * Wraps a new {@link PrintMessenger} around a {@link PrintStream}.
	 *
	 * @param out the {@link PrintStream} to wrap.
	 */
	public PrintMessenger(final PrintStream out) {
		Validate.notNull(out);

		this.out = out;
	}

	/**
	 * Prints a formatted {@link String} to the console,
	 * using the specified format {@link String}, and arguments.
	 *
	 * @param format The format {@link String}.
	 * @param args The arguments to format.
	 */
	public final void format(final String format, final Object ... args) {
		this.out.format(format, args);
	}

	/**
	 * Prints a formatted {@link String} to the console,
	 * using the specified format {@link String}, and arguments,
	 * and then starts a new line.
	 *
	 * @param format The format {@link String}.
	 * @param args The arguments to format.
	 */
	public final void formatln(final String format, final Object ... args) {
		this.out.format(format, args);
		this.out.println();
	}

	/**
	 * Prints the {@link String} to the console.
	 *
	 * @param s The {@link String} to print.
	 */
	public final void print(final String s) {
		this.out.print(s);
	}

	/**
	 * Prints the {@link Object} to the console.
	 *
	 * @param o The {@link Object} to print.
	 */
	public final void print(final Object o) {
		this.out.print(o);
	}

	/**
	 * Prints the {@code byte} to the console.
	 *
	 * @param b The {@code byte} to print.
	 */
	public final void print(final byte b) {
		this.out.print(b);
	}
	/**
	 * Prints the {@code short} to the console.
	 *
	 * @param s The {@code short} to print.
	 */
	public final void print(final short s) {
		this.out.print(s);
	}

	/**
	 * Prints the {@code char} to the console.
	 *
	 * @param c The {@code char} to print.
	 */
	public final void print(final char c) {
		this.out.print(c);
	}

	/**
	 * Prints the {@code int} to the console.
	 *
	 * @param i The {@code int} to print.
	 */
	public final void print(final int i) {
		this.out.print(i);
	}

	/**
	 * Prints the {@code long} to the console.
	 *
	 * @param l The {@code long} to print.
	 */
	public final void print(final long l) {
		this.out.print(l);
	}

	/**
	 * Prints the {@code float} to the console.
	 *
	 * @param f The {@code float} to print.
	 */
	public final void print(final float f) {
		this.out.print(f);
	}

	/**
	 * Prints the {@code double} to the console.
	 *
	 * @param d The {@code double} to print.
	 */
	public final void print(final double d) {
		this.out.print(d);
	}

	/**
	 * Prints the {@link String} to the console, and then starts a new line.
	 *
	 * @param s The {@link String} to print.
	 */
	public final void println(final String s) {
		this.out.println(s);
	}

	/**
	 * Prints the {@link Object} to the console, and then starts a new line.
	 *
	 * @param o The {@link Object} to print.
	 */
	public final void println(final Object o) {
		this.out.println(o);
	}

	/**
	 * Prints the {@code byte} to the console, and then starts a new line.
	 *
	 * @param b The {@code byte} to print.
	 */
	public final void println(final byte b) {
		this.out.println(b);
	}

	/**
	 * Prints the {@code short} to the console, and then starts a new line.
	 *
	 * @param s The {@code short} to print.
	 */
	public final void println(final short s) {
		this.out.println(s);
	}

	/**
	 * Prints the {@code char} to the console, and then starts a new line.
	 *
	 * @param c The {@code char} to print.
	 */
	public final void println(final char c) {
		this.out.println(c);
	}

	/**
	 * Prints the {@code int} to the console, and then starts a new line.
	 *
	 * @param i The {@code int} to print.
	 */
	public final void println(final int i) {
		this.out.println(i);
	}

	/**
	 * Prints the {@code long} to the console, and then starts a new line.
	 *
	 * @param l The {@code long} to print.
	 */
	public final void println(final long l) {
		this.out.println(l);
	}

	/**
	 * Prints the {@code float} to the console, and then starts a new line.
	 *
	 * @param f The {@code float} to print.
	 */
	public final void println(final float f) {
		this.out.println(f);
	}

	/**
	 * Prints the {@code double} to the console, and then starts a new line.
	 *
	 * @param d The {@code double} to print.
	 */
	public final void println(final double d) {
		this.out.println(d);
	}

	/**
	 * Starts a new line on the console.
	 */
	public final void println() {
		this.out.println();
	}
}
