package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.function.Function;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.ReflectionUtils;

/**
 * A mechanism for translating between human-readable {@link String}s and machine-usable {@link T}s.
 *
 * @param <T> The type of machine-readable {@link Object} that this operates on.
 *
 * @author Daniel Tang
 * @since 13 May 2017
 */
@SuppressWarnings("unused")
public final class HumanDataManipulator<T> {

	/**
	 * The {@link Function} that translates from human-readable {@link String}s to machine-usable {@link T}s.
	 */
	private final Function<String, T> dataParser;

	/**
	 * The {@link Function} that translates from machine-usable {@link T}s to human-readable {@link String}s.
	 */
	private final Function<T, String> dataVisualizer;

	/**
	 * Creates a new {@link HumanDataManipulator} wrapping the specified {@link Function} that translate back and forth.
	 *
	 * @param dataParser The {@link Function} that translates
	 *                   from human-readable {@link String}s to machine-usable {@link T}s.
	 * @param dataVisualizer The {@link Function} that translates
	 *                       from machine-usable {@link T}s to human-readable {@link String}s.
	 */
	public HumanDataManipulator(final Function<String, T> dataParser,
			final Function<T, String> dataVisualizer) {
		Validate.notNull(dataParser);
		Validate.notNull(dataVisualizer);

		this.dataParser = dataParser;
		this.dataVisualizer = dataVisualizer;
	}

	/**
	 * Translates a machine-usable {@link T} to a human-readable {@link String}.
	 *
	 * @param machineData The machine-usable {@link T} to translate,
	 *                    nullable if the translating {@link Function} accepts that.
	 * @return A human-readable {@link String} representing the data; the result from the translating {@link Function},
	 *         unless if the result was {@code null}, {@code "null"} is returned,
	 *         or if an {@link Exception} was thrown,
	 *         {@code "VisualizationError?"} + {@link ReflectionUtils#defaultToString(Object)}.
	 */
	public final String toHumanFormat(final T machineData) {
		String result;

		try {
			result = this.dataVisualizer.apply(machineData);
		} catch (final Exception e) {
			return "VisualizationError?" + ReflectionUtils.defaultToString(machineData);
		}

		return result == null ? "null" : result;
	}

	/**
	 * Translates a machine-usable {@link T} to a human-readable {@link String}.
	 *
	 * @param humanData The human-readable {@link String} to translate, not null.
	 * @return A machine-usable {@link T} representing the data;
	 *         the result of the translating {@link Function}, nullable if it is.
	 * @throws IllegalArgumentException If {@code humanData} is {@code null}.
	 */
	public final T toMachineFormat(final String humanData) {
		Validate.notNull(humanData);

		return this.dataParser.apply(humanData);
	}

	/**
	 * Determines if the human-readable {@link String} translates into a valid machine-usable {@link T}.
	 * This is done by checking calling the translating {@link Function}, detecting any {@link Exception}s,
	 * and throwing away the result.
	 *
	 * @param humanData The human-readable {@link String} to check translatability, not null.
	 * @return {@code true} if the translating {@link Function} did NOT throw any {@link Exception}.
	 */
	public final boolean isValidHumanFormat(final String humanData) {
		try {
			Validate.notNull(humanData);

			this.dataParser.apply(humanData);
		} catch (final Exception e) {
			return false;
		}

		return true;
	}
}
