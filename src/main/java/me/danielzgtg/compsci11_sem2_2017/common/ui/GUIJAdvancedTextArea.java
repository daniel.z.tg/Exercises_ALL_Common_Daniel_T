package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TextUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;

import me.danielzgtg.compsci11_sem2_2017.common.Log;

/**
 * A link {@link JTextArea} with a picture.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class GUIJAdvancedTextArea extends JTextArea {

	/**
	 * A map of class names, and their replacement {@link Class}es to instantiate and put in their place.
	 */
	private static final Map<String, Class> REPLACEMENT_UI_CLASSES;

	/**
	 * The {@link BufferedImage} to show inside the text area.
	 *
	 * A {@code null} image will act as a fully transparent one.
	 */
	/*packaged*/ BufferedImage image;

	/**
	 * Whether we are falling back to the platform-independent method for drawing the image.
	 */
	private boolean fallbackEnabled = false;

	/**
	 * The last known opacity.
	 */
	private boolean lastSetOpaque = true;

	/**
	 * Gets the {@link BufferedImage} shown inside the text area.
	 *
	 * @return The {@link BufferedImage} shown inside the text area.
	 */
	public BufferedImage getImage() {
		return this.image;
	}

	/**
	 * Sets the opacity of this {@link GUIJAdvancedTextArea}.
	 * This is done by calling {@link JTextArea#setOpaque(boolean)} if the fallback is not in use.
	 *
	 * @param opaque The new opacity setting.
	 * @see JTextArea#setOpaque(boolean)
	 */
	@Override
	public void setOpaque(final boolean opaque) {
		this.setLastSetOpaque(opaque);

		if (!this.fallbackEnabled) {
			this.jTextAreaSetOpaque(opaque);
		}
	}

	/**
	 * Sets the last known opacity, used by the fallback.
	 *
	 * @param opaque The new last known opacity.
	 */
	protected final void setLastSetOpaque(final boolean opaque) {
		this.lastSetOpaque = opaque;
	}

	/**
	 * Returns the last known opacity, used by the fallback.
	 *
	 * @return The last known opacity.
	 */
	protected final boolean getLastSetOpaque() {
		return this.lastSetOpaque;
	}

	/**
	 * Calls {@link JTextArea#setOpaque(boolean)}, ignoring if the platform-independent fallback is in use.
	 *
	 * @param opaque The new opacity setting.
	 */
	protected final void jTextAreaSetOpaque(final boolean opaque) {
		super.setOpaque(opaque);
	}

	/**
	 * Sets the {@link BufferedImage} to show inside the text area.
	 *
	 * @param image The {@link BufferedImage} to show inside the text area.
	 */
	public void setImage(final BufferedImage image) {
		this.image = image;
	}

	/**
	 * Creates a new GUIJAdvancedTextArea with the initial text.
	 *
	 * @param initialText The initial text.
	 **/
	public GUIJAdvancedTextArea(final String initialText) {
		super(initialText);
	}

	/**
	 * Creates a new GUIJAdvancedTextArea with the {@link BufferedImage}.
	 *
	 * @param image The initial {@link BufferedImage}.
	 */
	public GUIJAdvancedTextArea(final BufferedImage image) {
		this.setImage(image);
	}

	/**
	 * Creates a new GUIJAdvancedTextArea with the initial text, and the {@link BufferedImage}.
	 *
	 * @param initialText The initial text.
	 * @param image The initial {@link BufferedImage}.
	 */
	public GUIJAdvancedTextArea(final String initialText, final BufferedImage image) {
		super(initialText);

		this.setImage(image);
	}

	/**
	 * Creates a new GUIJAdvancedTextArea without an image.
	 * This behaves in the same way as a JPanel.
	 */
	public GUIJAdvancedTextArea() {}

	/**
	 * Reloads and invalidates the UI.
	 * The new interface will not draw backgrounds.
	 */
	@Override
	public void updateUI() {
		this.doSetUI();
		this.invalidate(); // mark for update
	}

	/**
	 * Actually sets a new UI.
	 */
	@SuppressWarnings("unchecked")
	private void doSetUI() {
		if (this.fallbackEnabled) return;

		// Determine the old UI class name
		final String defaultUIName;
		{
			final ComponentUI defaultUI = UIManager.getUI(this);

			if (defaultUI == null) {
				this.enableFallback(
						"[GUIAdvancedTextArea] " +
								"Default UI that would have been installed should not be null.");
				return;
			}

			defaultUIName = defaultUI.getClass().getName();
		}

		// Look up the new UI class based on the old UI class name
		final Class replacementUIclass = REPLACEMENT_UI_CLASSES.get(defaultUIName);
		if (replacementUIclass == null) {
			this.enableFallback(
					"[GUIAdvancedTextArea] Don't know how to deal with replacing: "
							+ defaultUIName);
			return;
		}

		// Construct the new UI
		final TextUI newUI;
		try {
			/*
			 * This try block is equivalent to the following pseudocode:
			 *
			 * newUI = new [insert contents of replacementUIclass](this);
			 */
			final Constructor ctor = replacementUIclass.getConstructor(GUIJAdvancedTextArea.class);
			newUI = (TextUI) ctor.newInstance(this);
		} catch (final Exception e) {
			this.enableFallback(
					"[GUIAdvancedTextArea] There was a problem instantiating the replacement UI.");
			return;
		}

		this.setUI(newUI);
	}

	/**
	 * Starts to fall back to the platform-independent method of drawing the image.
	 */
	protected final void enableFallback() {
		this.fallbackEnabled = true;
		this.setOpaque(false);
	}

	/**
	 * Gets whether we are falling back to the platform-independent method for drawing the image.
	 *
	 * @return {@code true} if we are falling back to the platform-independent method for drawing the image.
	 */
	protected final boolean isFallbackEnabled() {
		return this.fallbackEnabled;
	}

	/**
	 * Starts to fall back to the platform-independent method of drawing the image, and prints a log message.
	 *
	 * @param errorMessage The message to explain why the fallback needs to be used.
	 */
	private void enableFallback(final String errorMessage) {
		this.enableFallback();

		Log.log(errorMessage);
	}

	/**
	 * Paints the component, using {@link JTextArea#paintComponent(Graphics)},
	 * and {@link GUIJAdvancedTextArea#uiPaint(Graphics)} before that here if the fallback is enabled.
	 */
	@Override
	protected void paintComponent(final Graphics g) {
		if (this.fallbackEnabled) {
			this.uiPaint(g);
		}

		super.paintComponent(g);
	}

	/**
	 * Actually draws the background {@link Color}, if opaque,
	 * and the {@link BufferedImage}, if not {@code null}.
	 *
	 * @param g The {@link Graphics} to draw with.
	 */
	/*packaged*/ final void uiPaint(final Graphics g) {
		// Fix background color being on top, by taking over the drawing process of it
		// Replace background color, and draw before image
		if (this.fallbackEnabled ? this.lastSetOpaque : this.isOpaque()) {
			g.setColor(this.getBackground());
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
		}

		this.drawImage(g);
	}

	/**
	 * Draws the contained image.
	 *
	 * @param g The {@link Graphics} to draw with.
	 */
	private final void drawImage(final Graphics g) {
		if (this.image != null) {
			// ">>> 1" == "/ 2"
			final int x = (this.getWidth() - this.image.getWidth(null)) >>> 1;
			final int y = (this.getHeight() - this.image.getHeight(null)) >>> 1;

			g.drawImage(this.image, x, y, this);
		}
	}

	static {
		// Populate the replacement UI map
		final HashMap<String, Class> replacementUIs = new HashMap<>();

		replacementUIs.put("javax.swing.plaf.basic.BasicTextAreaUI", GUIAdvancedBasicTextAreaUI.class);
		replacementUIs.put("javax.swing.plaf.synth.SynthTextAreaUI", GUIAdvancedSynthTextAreaUI.class);
		replacementUIs.put("com.sun.java.swing.plaf.windows.WindowsTextAreaUI", GUIAdvancedWindowsTextAreaUI.class);

		REPLACEMENT_UI_CLASSES = Collections.unmodifiableMap(replacementUIs);
	}

}
