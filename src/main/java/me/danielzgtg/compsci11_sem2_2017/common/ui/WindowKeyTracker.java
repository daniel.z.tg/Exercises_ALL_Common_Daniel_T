package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.LinkedList;
import java.util.List;
import java.util.WeakHashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.swing.JFrame;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A way to track all key events that occur when a window is in focus.
 *
 * @author Daniel Tang
 * @since 19 May 2017
 */
@SuppressWarnings({"WeakerAccess", "unused", "UnusedReturnValue"})
public final class WindowKeyTracker {

	/**
	 * The map of the {@link KeyListener}s in {@link JFrame}s.
	 */
	private static final WeakHashMap<JFrame, List<KeyListener>> listeners = new WeakHashMap<>();

	/**
	 * Adds a {@link KeyListener} for all key events in a {@link JFrame}.
	 *
	 * @param frame The {@link JFrame} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static void addListener(final JFrame frame, final KeyListener listener) {
		Validate.notNull(frame);
		Validate.notNull(listener);

		synchronized (listeners) {
			final List<KeyListener> keyListeners;

			init();

			{
				final List<KeyListener> tryKeyListeners = listeners.get(frame);
				if (tryKeyListeners != null) {
					keyListeners = tryKeyListeners;
				} else {
					listeners.put(frame, keyListeners = new LinkedList<>());
				}
			}

			keyListeners.add(listener);
		}
	}

	/**
	 * Adds a {@link KeyListener} for all key typing events in a {@link JFrame}.
	 *
	 * @param frame The {@link JFrame} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final KeyListener hookKeyCodeType(final JFrame frame, final Consumer<Integer> listener) {
		final KeyListener result = new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}

			@Override
			public void keyPressed(final KeyEvent e) {}

			@Override
			public void keyReleased(final KeyEvent e) {}
		};

		addListener(frame, result);

		return result;
	}

	/**
	 * Adds a {@link KeyListener} for all key up events in a {@link JFrame}.
	 *
	 * @param frame The {@link JFrame} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final KeyListener hookKeyCodeUp(final JFrame frame, final Consumer<Integer> listener) {
		final KeyListener result = new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {}

			@Override
			public void keyPressed(final KeyEvent e) {}

			@Override
			public void keyReleased(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}
		};

		addListener(frame, result);

		return result;
	}

	/**
	 * Adds a {@link KeyListener} for all key down events in a {@link JFrame}.
	 *
	 * @param frame The {@link JFrame} to monitor.
	 * @param listener The listener to install as a hook.
	 */
	public static final KeyListener hookKeyCodeDown(final JFrame frame, final Consumer<Integer> listener) {
		final KeyListener result = new KeyListener() {
			@Override
			public void keyTyped(final KeyEvent e) {}

			@Override
			public void keyPressed(final KeyEvent e) {
				listener.accept(e.getKeyCode());
			}

			@Override
			public void keyReleased(final KeyEvent e) {}
		};

		addListener(frame, result);

		return result;
	}

	/**
	 * Removes a {@link KeyListener} for all key events in a {@link JFrame}.
	 *
	 * @param frame The {@link JFrame} to remove the listener from.
	 * @param listener The listener to remove.
	 */
	public static void removeListener(final JFrame frame, final KeyListener listener) {
		Validate.notNull(frame);
		Validate.notNull(listener);

		synchronized (listeners) {
			final List<KeyListener> keyListeners = listeners.get(frame);

			if (keyListeners == null) return;

			keyListeners.remove(listener);
		}
	}

	/**
	 * Whether the listener was installed.
	 */
	private static volatile boolean didInit = false;

	/**
	 * Installs the listener that allows listening for key events in a window, if necessary.
	 */
	public static final void init() {
		if (!didInit) {
			synchronized (listeners) {
				doInit();
			}
		}
	}

	/**
	 * Actually installs the listener that allows listening for key events in a window, if necessary.
	 * The {@link WindowKeyTracker#init()} wraps this one, to avoid unnecessary synchronization overhead.
	 */
	private static final void doInit() {
		if (didInit) {
			return;
		}

		didInit = true;

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((event) -> {
			final BiConsumer<KeyListener, KeyEvent> eventHandlerMethod;
			switch(event.getID()) {
				case KeyEvent.KEY_TYPED:
					eventHandlerMethod = KeyListener::keyTyped;
					break;
				case KeyEvent.KEY_PRESSED:
					eventHandlerMethod = KeyListener::keyPressed;
					break;
				case KeyEvent.KEY_RELEASED:
					eventHandlerMethod = KeyListener::keyReleased;
					break;
				default:
					throw new AssertionError();
			}

			Component component = event.getComponent();
			while (component != null) {
				if (component instanceof JFrame) {
					final JFrame frameComponent = (JFrame) component;
					final List<KeyListener> windowKeyListeners;

					synchronized(listeners) {
						windowKeyListeners = listeners.get(frameComponent);
					}

					if (windowKeyListeners != null) {
						for (final KeyListener keyListener : windowKeyListeners) {
							eventHandlerMethod.accept(keyListener, event);
						}
					}

					return false;
				}

				component = component.getParent();
			}

			return false;
		});
	}

	@Deprecated
	private WindowKeyTracker() { throw new UnsupportedOperationException(); }
}
