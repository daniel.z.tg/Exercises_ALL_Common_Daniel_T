package me.danielzgtg.compsci11_sem2_2017.common.ui;

import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * A {@link JPanel} that can hold a {@link BufferedImage}.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class GUIJImagePanel extends JPanel {

	/**
	 * Gets the {@link BufferedImage} shown inside the {@link GUIJImagePanel}.
	 *
	 * @return The {@link BufferedImage} shown inside the {@link GUIJImagePanel}.
	 */
	public BufferedImage getImage() {
		return this.image;
	}

	/**
	 * Sets the {@link BufferedImage} to show inside the {@link GUIJImagePanel}.
	 *
	 * @param image The {@link BufferedImage} to show inside the {@link GUIJImagePanel}.
	 */
	public void setImage(final BufferedImage image) {
		this.image = image;

		if (image != null) {
			SwingUtils.forceDimensionSize(this, new Dimension(image.getWidth(), image.getHeight()));
			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * The {@link BufferedImage} to show inside the {@link GUIJImagePanel}.
	 *
	 * A {@code null} {@link BufferedImage} will act as a fully transparent one.
	 */
	private BufferedImage image;

	/**
	 * Creates a new {@link GUIJImagePanel} with the {@link BufferedImage}.
	 *
	 * @param image The initial {@link BufferedImage}.
	 */
	public GUIJImagePanel(final BufferedImage image) {
		this();
		this.setImage(image);
	}

	/**
	 * Creates a new {@link GUIJImagePanel} without an {@link BufferedImage}.
	 * This behaves in the same way as a {@link JPanel}.
	 */
	public GUIJImagePanel() {
		this.setLayout(null);
	}

	@Override
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);

		if (this.image != null) {
			// Paint the image centered
			// ">>> 1" == "/ 2"
			final int x = (this.getWidth() - this.image.getWidth(null)) >>> 1;
			final int y = (this.getHeight() - this.image.getHeight(null)) >>> 1;

			g.drawImage(this.image, x, y, this);
		}
	}
}
