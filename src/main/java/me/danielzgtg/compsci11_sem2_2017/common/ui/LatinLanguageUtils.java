package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.regex.Pattern;

/**
 * Useful tools for human-friendly latin-like text.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class LatinLanguageUtils {

	/**
	 * A pattern that can match individual words, regardless of punctuation.
	 */
	public static final Pattern WORD_PATTERN = Pattern.compile("\\b[^\\s]+\\b");

	/**
	 * Determines if a letter is always an English vowel, (case-insensitive a, e, i, o, u)
	 *
	 * @param letter The char to check
	 * @return {@code true} is the char is indeed a voxel
	 */
	public static final boolean alwaysIsEnglishVowel(final char letter) {
		switch (letter) {
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				return true;
			default:
				return false;
		}
	}

	/**
	 * Determines if a letter is might be an English vowel, (case-insensitive a, e, i, o, u, y)
	 *
	 * @param letter The char to check
	 * @return {@code true} is the char is indeed a voxel
	 */
	public static final boolean mightBeEnglishVowel(final char letter) {
		switch (letter) {
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
			case 'y':
			case 'Y':
				return true;
			default:
				return false;
		}
	}

	/**
	 * Formats a time duration in minutes to a human-friendly String of hours and minutes
	 *
	 * @param toFormatMinutes The time duration to format, in minutes.
	 * @return A String representing the formatted time duration.
	 */
	public static final String formatMinsToMinsHrs(final long toFormatMinutes) {
		return String.format("%01dH:%02dM", toFormatMinutes / 60, toFormatMinutes % 60);
	}

	/**
	 * Formats a word so that the only capitalized letter is the first one.
	 * A {@code null} input, or a zero-length input will return {@code ""}.
	 *
	 * @param word The word as a {@link String}.
	 * @return A word, which only capitalized letter is the first one.
	 */
	public static final String asCapitalizedWord(final String word) {
		if (word == null || word.length() == 0) {
			return "";
		}

		return Character.toUpperCase(word.charAt(0)) + word.substring(1).toLowerCase();
	}

	@Deprecated
	private LatinLanguageUtils() { throw new UnsupportedOperationException(); }
}
