package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;

/**
 * A command-driven command-line user interface subprogram.
 *
 * @param <S> The state object type.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public final class CommandDrivenCLI<S> implements Runnable {

	/**
	 * The error message when the user did not enter any command.
	 */
	private final String commandRequiredMsg;

	/**
	 * NOP {@link Function} that keep the state the same.
	 */
	private static final Function<CommandDrivenCLI<Object>, Object> PASSTHROUGH_STATE_FUNCTION =
			CommandDrivenCLI::getState;

	/**
	 * NOP {@link BiFunction} that keep the state the same.
	 */
	private static final BiFunction<CommandDrivenCLI<Object>, Object, Object> PASSTHROUGH_STATE_BIFUNCTION =
			(system, other) -> system.getState();

	/**
	 * The function invoked when the command is unknown.
	 */
	private final BiFunction<CommandDrivenCLI<S>, String[], S> unknownCommandFunction;

	/**
	 * The map of names to commands.
	 */
	private final Map<String, BiFunction<CommandDrivenCLI<S>, String[], S>> commands;

	/**
	 * The console prompt String.
	 */
	private final String consolePrompt;

	/**
	 * The current state Object.
	 */
	private S state;

	/**
	 * A callback for when this CLI is about to start.
	 */
	private final Function<CommandDrivenCLI<S>, S> introduction;

	/**
	 * A callback for the beginning of each loop.
	 */
	private final Function<CommandDrivenCLI<S>, S> loopBeginning;

	/**
	 * A callback with the raw {@link String}{@code []} before the commands are looked up.
	 */
	private final BiFunction<CommandDrivenCLI<S>, String[], S> beforeCommandLookup;

	/**
	 * A callback for looking up the command.
	 */
	private final BiFunction<CommandDrivenCLI<S>, String[],
			BiFunction<CommandDrivenCLI<S>, String[], S>> commandLookup;

	/**
	 * A callback before the command function is run.
	 */
	private final BiFunction<CommandDrivenCLI<S>, BiFunction<CommandDrivenCLI<S>, String[], S>, S> beforeCommandExec;

	/**
	 * A callback for after the command function is run,  but before the loop condition is re-checked.
	 */
	private final Function<CommandDrivenCLI<S>, S> afterCommand;

	/**
	 * A callback for when this CLI is about to end.
	 */
	private final Consumer<CommandDrivenCLI<S>> conclusion;

	/**
	 * The {@link Prompter} for obtaining user input.
	 */
	private final Prompter prompter;

	/**
	 * The {@link PrintMessenger} for showing the user some data.
	 */
	private final PrintMessenger messenger;

	/**
	 * Creates a customized {@link CommandDrivenCLI}.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param consolePrompt The prompt to show when asking to the command, {@code null} for default.
	 * @param unknownCommandFunction The {@link Function} to run when the user's command could not be found.
	 * @param commands The usable lowercase commands, and their behaviours,
	 *                 {@code return null} to exit the command loop, none null.
	 * @param initialState The initial state of the command-drive interface, not null.
	 * @param generateDefaultHelp Whether to use the default help command.
	 * @param introduction The callback to run before the command loop is entered, nullable for default.
	 * @param loopBeginning The callback to run at the beginning of the command loop,
	 *                      before the next command is prompted, nullable for default.
	 * @param beforeCommandLookup The callback to run before the command is looked up, even if the command is missing,
	 *                            nullable for default.
	 * @param commandLookup The transformer to lookup the command, nullable for default.
	 * @param beforeCommandExec The callback to run before the command function is run, nullable for default.
	 * @param afterCommand The callback to run after the command function is run, nullable for default.
	 * @param conclusion The callback to run after the command loop is left, nullable for default.
	 * @param prompter The {@link Prompter} to obtain input with, not null.
	 * @param messenger The {@link PrintMessenger} to show the user some data with, not null.
	 */
	@SuppressWarnings({"unchecked", "RedundantCast"})
	public CommandDrivenCLI(
			final Map<?, ?> constantsMap, final String consolePrompt,
			final BiFunction<CommandDrivenCLI<S>, String[], S> unknownCommandFunction,
			final Map<String, BiFunction<CommandDrivenCLI<S>, String[], S>> commands,
			final S initialState, final boolean generateDefaultHelp,
			final Function<CommandDrivenCLI<S>, S> introduction,
			final Function<CommandDrivenCLI<S>, S> loopBeginning,
			final BiFunction<CommandDrivenCLI<S>, String[], S> beforeCommandLookup,
			final BiFunction<CommandDrivenCLI<S>, String[],
					BiFunction<CommandDrivenCLI<S>, String[], S>> commandLookup,
			final BiFunction<CommandDrivenCLI<S>, BiFunction<CommandDrivenCLI<S>, String[], S>, S> beforeCommandExec,
			final Function<CommandDrivenCLI<S>, S> afterCommand,
			final Consumer<CommandDrivenCLI<S>> conclusion,
			final Prompter prompter,
			final PrintMessenger messenger) {
		// Early Checks
		Validate.notNull(constantsMap);
		Validate.notNull(initialState);
		Validate.notNull(commands);
		Validate.notNull(prompter);
		Validate.notNull(messenger);

		final Map<String, BiFunction<CommandDrivenCLI<S>, String[], S>> commandMap = new HashMap<>(commands);
		Validate.withoutNull(commandMap);

		// Default Help
		final String defaultHelpCommandName = Coerce.asString(
				constantsMap.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.helpCommandName"));
		if (generateDefaultHelp) {
			final String availableCommandsHeader = Coerce.asString(
					constantsMap.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.availableCommandHeader"));

			commandMap.put(defaultHelpCommandName,
					(system, args) -> {
						System.out.println(system.getCommands().keySet().stream().collect(
								Collectors.joining(", ", availableCommandsHeader, ".")));
						return system.getState();
					});
		}

		// Setup callbacks
		// Some fields are set to default when parameter is null
		this.consolePrompt = consolePrompt != null ? consolePrompt : Coerce.asString(
				constantsMap.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.consolePrompt"));
		if (unknownCommandFunction != null) {
			this.unknownCommandFunction = unknownCommandFunction;
		} else {
			final String unknownCommandMsgWithHelp = Coerce.asString(constantsMap
					.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.unknownCommandFormatWithHelp"));
			final String unknownCommandMsgSimple = Coerce.asString(constantsMap
					.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.unknownCommandMsgSimple"));

			this.unknownCommandFunction = (system, args) -> {
				System.out.println(system.getCommands().containsKey(defaultHelpCommandName) ?
						String.format(unknownCommandMsgWithHelp, defaultHelpCommandName) :
						unknownCommandMsgSimple);
				return system.getState();
			};
		}

		this.introduction = introduction != null ? introduction :
				(Function) PASSTHROUGH_STATE_FUNCTION;
		this.loopBeginning = loopBeginning != null ? loopBeginning :
				(Function) PASSTHROUGH_STATE_FUNCTION;
		this.beforeCommandLookup = beforeCommandLookup != null ? beforeCommandLookup :
				(BiFunction) PASSTHROUGH_STATE_BIFUNCTION;
		this.commandLookup = commandLookup != null ? commandLookup :
				(container, commandArgs) -> this.getCommands().get(commandArgs[0].toLowerCase());
		this.beforeCommandExec = beforeCommandExec != null ? beforeCommandExec :
				(BiFunction) PASSTHROUGH_STATE_BIFUNCTION;
		this.afterCommand = afterCommand != null ? afterCommand :
				(Function) PASSTHROUGH_STATE_FUNCTION;
		this.conclusion = conclusion != null ? conclusion : CommonLambdas.NOP_CONSUMER;

		// Load remaining Behavioural Constants
		this.commandRequiredMsg = Coerce.asString(constantsMap
				.get("me.danielzgtg.compsci11_sem2_2017.common.commandcli.commandRequiredMsg"));

		// Store Parameters to Fields
		this.commands = Collections.unmodifiableMap(commandMap);
		this.state = initialState;
		this.prompter = prompter;
		this.messenger = messenger;
	}

	/**
	 * Gets the state object stored.
	 *
	 * @return The stored state object.
	 */
	public final S getState() {
		return this.state;
	}

	/**
	 * Gets the map of commands.
	 *
	 * @return The map of commands.
	 */
	public final Map<String, BiFunction<CommandDrivenCLI<S>, String[], S>> getCommands() {
		return this.commands;
	}

	/**
	 * Runs the CLI UI, until the state is {@code null}.
	 */
	@Override
	public final void run() {
		this.state = this.introduction.apply(this);

		while (this.state != null) {
			this.state = this.loopBeginning.apply(this);

			final String commandStr = this.prompter.promptString(this.consolePrompt);
			final String[] commandStrComponents = commandStr.split(" "); // Split into args

			this.state = this.beforeCommandLookup.apply(this, commandStrComponents);

			if (commandStrComponents.length < 1 || "".equals(commandStrComponents[0])) {
				this.messenger.println(this.commandRequiredMsg);
				continue;
			}

			BiFunction<CommandDrivenCLI<S>, String[], S> commandFunc =
					this.commandLookup.apply(this, commandStrComponents);

			if (commandFunc == null) {
				commandFunc = this.unknownCommandFunction;
			}

			this.state = this.beforeCommandExec.apply(this, commandFunc);
			this.state = commandFunc.apply(this, commandStrComponents);
			this.state = this.afterCommand.apply(this);
		}

		this.conclusion.accept(this);
	}

	/**
	 * Adds commands for exiting a {@link CommandDrivenCLI}
	 * to a command list that can be used as a constructor parameter for instantiating one.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param commandList The command list for a {@link CommandDrivenCLI}.
	 * @param <S> The generic type for the {@link CommandDrivenCLI}.
	 */
	@SuppressWarnings("unchecked")
	public static final <S> void addQuitFunctions(final Map<?, ?> constantsMap,
			final Map<String, BiFunction<CommandDrivenCLI<S>, String[], S>> commandList) {
		Validate.notNull(commandList);
		Validate.notNull(constantsMap);

		final Object tryQuitNames = constantsMap.get("me.danielzgtg.compsci11_sem2_2017.common.cli.quitNames");
		Validate.isType(tryQuitNames, Collection.class);
		final List<?> quitNames = (List<?>) tryQuitNames;

		for (final Object o : quitNames) {
			Validate.isType(o, String.class);
			commandList.put((String) o, CommonLambdas.NULL_BIFUNCTION);
		}
	}

	/**
	 * Gets the {@link Prompter} for obtaining user input.
	 *
	 * @return The {@link Prompter} for obtaining user input.
	 */
	public final Prompter getPrompter() {
		return this.prompter;
	}

	/**
	 * Gets the {@link PrintMessenger} for showing the user some data.
	 *
	 * @return The {@link PrintMessenger} for showing the user some data.
	 */
	public final PrintMessenger getMessenger() {
		return this.messenger;
	}
}
