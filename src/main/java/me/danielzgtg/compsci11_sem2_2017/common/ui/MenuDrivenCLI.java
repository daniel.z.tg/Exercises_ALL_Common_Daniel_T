package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A menu-driven command-line user interface subprogram.
 *
 * @author Daniel Tang
 * @since 16 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class MenuDrivenCLI implements Runnable {

	/**
	 * The underlying {@link CommandDrivenCLI}.
	 */
	private final CommandDrivenCLI<Object> backend;

	/**
	 * The list of selections that can be chosen from by the user.
	 */
	private final List<Pair<Function<Integer, Object>, String>> selections;

	/**
	 * Creates a new {@link MenuDrivenCLI}, based on the specified parameters.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param selections The choices in the menu, and their behaviours,
	 *                   {@code return null} to exit the menu loop, not null.
	 * @param introduction The callback to run before the menu loop is entered, nullable for default.
	 * @param conclusion The callback to run after the menu loop is left, nullable for default.
	 * @param afterSelection The callback to run after the selection is run, nullable for default.
	 * @param invalidSelectionMsg The message to show when the user's selection is not in the menu,
	 *                            nullable for default.
	 * @param consolePrompt The prompt to show when asking to the selection, nullable for default.
	 * @param menuTitle The menu title message, nullable for default.
	 * @param menuItemLayout The layout of the menu choices, in the form {@code "%d%s"}, nullable for default.
	 * @param prompter The {@link Prompter} to obtain input with, not null.
	 * @param messenger The {@link PrintMessenger} to show the user some data with, not null.
	 */
	public MenuDrivenCLI(final Map<?, ?> constantsMap,  final List<Pair<Function<Integer, Object>, String>> selections,
			final Runnable introduction, final Runnable conclusion, final Runnable afterSelection,
			final String invalidSelectionMsg, final String consolePrompt,
			final String menuTitle, final String menuItemLayout,
			final Prompter prompter,
			final PrintMessenger messenger) {
		Validate.withoutNull(selections);
		Validate.notNull(invalidSelectionMsg);
		this.selections = Collections.unmodifiableList(selections);

		final Map<String, BiFunction<CommandDrivenCLI<Object>, String[], Object>> commands = new HashMap<>();

		for (int i = 0; i < this.selections.size(); i++) {
			final Function<Integer, Object> option = this.selections.get(i).getLeft();
			Validate.notNull(option);

			final int targetIndex = i + 1;
			commands.put(String.valueOf(targetIndex), (system, args) -> {
				if (args.length != 1) {
					messenger.print(invalidSelectionMsg);

					return system.getState();
				}

				final int gotIndex = Integer.valueOf(args[0]);
				Validate.require(gotIndex == targetIndex);
				return option.apply(gotIndex);
			});
		}

		this.backend = new CommandDrivenCLI<>(constantsMap, consolePrompt, (system, args) -> {
			messenger.print(invalidSelectionMsg);

			return system.getState();
		}, commands, Boolean.TRUE, false, introduction == null ? null : (system) -> {
			introduction.run();

			return system.getState();
		}, (system) -> {
			messenger.println(menuTitle);

			final List<Pair<Function<Integer, Object>, String>> optionz = this.selections;
			for (int i = 0; i < optionz.size(); i++) {
				messenger.formatln(menuItemLayout, i, optionz.get(i).getRight());
			}

			return system.getState();
		}, null, null,
				null, afterSelection == null ? null : (system) -> {
			afterSelection.run();
			return system.getState();
		}, conclusion == null ? null : (system) -> conclusion.run(),
				prompter, messenger);
	}

	/**
	 * Interacts with the user using this {@link MenuDrivenCLI}.
	 */
	@Override
	public void run() {
		this.backend.run();
	}
}
