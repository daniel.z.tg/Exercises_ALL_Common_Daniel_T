package me.danielzgtg.compsci11_sem2_2017.common.ui;

import javax.swing.plaf.synth.SynthTextAreaUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * A {@link SynthTextAreaUI} used with {@link GUIJAdvancedTextArea} that does not
 * draw a background, so that the background can be drawn properly somewhere else.
 *
 * @author Daniel Tang
 * @since 21 April 2017
 */
/*packaged*/ final class GUIAdvancedSynthTextAreaUI extends SynthTextAreaUI {

	/**
	 * The {@link GUIJAdvancedTextArea} this UI is linked to.
	 */
	private final GUIJAdvancedTextArea parent;

	/**
	 * Creates a new {@link GUIAdvancedSynthTextAreaUI} linked to its {@link GUIJAdvancedTextArea}.
	 *
	 * @param parent The {@link GUIJAdvancedTextArea} to be linked to.
	 */
	public GUIAdvancedSynthTextAreaUI(final GUIJAdvancedTextArea parent) {
		this.parent = parent;
	}

	/**
	 * Draws the background {@link Color}, if opaque,
	 * and the {@link BufferedImage}, if not {@code null}.
	 *
	 * @param g The {@link Graphics} to draw with.
	 */
	@Override
	protected void paintBackground(final Graphics g) {
		this.parent.uiPaint(g);
	}
}
