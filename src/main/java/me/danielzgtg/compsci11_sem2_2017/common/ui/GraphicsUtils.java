package me.danielzgtg.compsci11_sem2_2017.common.ui;

import java.util.Random;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Tools to deal with images.
 *
 * @author Daniel Tang
 * @since 22 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class GraphicsUtils {

	/**
	 * Calculates a new down-tinted {@link BufferedImage},
	 * which has color channels linearly scaled
	 * to no brighter than the channels in the color specified.
	 *
	 * @param original The original {@link BufferedImage}.
	 * @param red The red color channel scale.
	 * @param green The green color channel scale.
	 * @param blue The blue color channel scale.
	 * @return A new down-tinted {@link BufferedImage},
	 * which has color channels linearly scaled
	 * to no brighter than the channels in the color specified.
	 */
	public static final BufferedImage scaleImageChannels(final BufferedImage original,
			final double red, final double green, final double blue) {
		Validate.notNull(original, "A null image cannot be tinted!");

		final int width = original.getWidth();
		final int height = original.getHeight();

		final BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		// Transform pixels
		for (int x = width - 1; x >= 0; x--) {
			for (int y = height - 1; y >= 0; y--) {
				// 0xAARRGGBB
				final int oldARGB = original.getRGB(x, y);

				// Preserve alpha
				final int alpha = (oldARGB >>> 24) & 0xFF;

				// Get components
				final int oldR =  (oldARGB >>> 16) & 0xFF;
				final int oldG =  (oldARGB >>>  8) & 0xFF;
				final int oldB =  (oldARGB       ) & 0xFF;

				// Scale each channel individually
				final int newR = (int) (oldR * red  );
				final int newG = (int) (oldG * green);
				final int newB = (int) (oldB * blue );

				// Repack into ARGB
				result.setRGB(x, y,
						(alpha << 24) | (newR << 16) | (newG << 8) | (newB));
			}
		}

		return result;
	}

	/**
	 * Calculates a new down-tinted {@link BufferedImage},
	 * which has {@link Color} channels linearly scaled
	 * to no brighter than the channels in the color specified.
	 *
	 * @param original The original {@link BufferedImage}.
	 * @param color The maximum {@link Color}.
	 * @return A new down-tinted {@link BufferedImage},
	 * which has {@link Color} channels linearly scaled
	 * to no brighter than the channels in the color specified.
	 */
	public static final BufferedImage asTintedImage(final BufferedImage original, final Color color) {
		Validate.notNull(color, "Color cannot be null!");

		return scaleImageChannels(original,
				color.getRed() / 255.0D, color.getGreen() / 255.0D, color.getBlue() / 255.0D);
	}

	/**
	 * Selects a random {@link Color} using {@link Math#random()}.
	 *
	 * @return A random {@link Color}.
	 */
	public static final Color randomColor() {
		return new Color((int) Math.round(Math.random() * 0xFFFFFF));
	}

	/**
	 * Selects a random {@link Color} using the specified {@link Random}.
	 *
	 * @param rng The {@link Random} to pick the color with.
	 * @return A random {@link Color}.
	 */
	public static final Color randomColor(final Random rng) {
		Validate.notNull(rng, "Cannot use a null RNG!");

		return new Color(rng.nextInt(0xFFFFFF + 1));
	}

	/**
	 * Smoothly scales a {@link BufferedImage} up or down, according to the specified dimensions.
	 *
	 * Smoothness is attempted using: smooth rendering hints for both directions,
	 * and progressive steps when scaling down.
	 *
	 * @param original The {@link BufferedImage} to scale.
	 * @param newWidth The desired width.
	 * @param newHeight The desired height.
	 * @return A new {@link BufferedImage} representing the original, at the new dimensions.
	 */
	public static final BufferedImage smoothScaleImage(final BufferedImage original,
			final int newWidth, final int newHeight) {
		Validate.notNull(original, "A null image cannot be scaled!");

		BufferedImage work = original;
		int targetWidth = original.getWidth();
		int targetHeight = original.getHeight();
		boolean again;
		do {
			// Determine progressive scale targets
			//boolean again = (targetWidth == newWidth) && (targetHeight == newHeight);
			again = ((targetWidth ^ newWidth) | (targetHeight ^ newHeight)) != 0;
			//final int halfTargetWidth = targetWidth / 2;
			final int halfTargetWidth = targetWidth >>> 1;
			// Stop downscaling at target.
			if (halfTargetWidth > newWidth) {
				targetWidth = halfTargetWidth;
			} else {
				targetWidth = newWidth;
			}

			final int halfTargetHeight = targetHeight >>> 1;
			if (halfTargetHeight > newHeight) {
				targetHeight = halfTargetHeight;
			} else {
				targetHeight = newHeight;
			}

			// Setup scale step
			final BufferedImage result = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
			final Graphics2D g = result.createGraphics();

			// Smooth scaling hints
			g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			// Scale step
			g.drawImage(work, 0, 0, targetWidth, targetHeight,
					0, 0, work.getWidth(), work.getHeight(),
					null);

			// Cleanup scale step
			g.dispose();

			// Post scale step results
			work = result;
		} while (again);

		return work;
	}

	/**
	 * Copies the font settings from one {@link Font} to another.
	 *
	 * @param base The base {@link Font} to derive a new one with copied settings,
	 *             font family is guaranteed to be preserved.
	 * @param toCopy The {@link Font} to copy settings from.
	 * @return A new {@link Font} with target as a base, and toCopy as the settings.
	 */
	public static final Font copyFontSettings(final Font base, final Font toCopy) {
		Validate.notNull(base);
		Validate.notNull(toCopy);

		return base.deriveFont(toCopy.getStyle(), toCopy.getSize2D());
	}

	/**
	 * Divides a {@link BufferedImage} into a grid.
	 *
	 * @param original The {@link BufferedImage} to divide.
	 * @param cols The number of columns in the grid.
	 * @param rows The number of rows in the grid.
	 * @return A grid array of subimages.
	 */
	public static final BufferedImage[][] splitImage(final BufferedImage original, final int cols, final int rows) {
		Validate.require(cols > 0 && rows > 0);
		Validate.notNull(original);

		final int width = original.getWidth();
		final int height = original.getHeight();
		Validate.require(width > 0 && height > 0);
		final int xSize = width / (cols + 1);
		final int ySize = height / (rows + 1);
		final BufferedImage[][] result = new BufferedImage[cols][rows];

		int xPos = width - xSize;
		for (int x = cols - 1; x > 0; x--) {
			int yPos = height - ySize;
			for (int y = rows - 1; y > 0; y--) {
				result[x][y] = original.getSubimage(xPos, yPos, xSize, ySize);
				yPos -= ySize;
			}
			result[x][0] = original.getSubimage(xPos, 0, xSize, yPos);
			xPos -= xSize;
		}
		{
			int yPos = height - ySize;
			for (int y = rows - 1; y > 0; y--) {
				result[0][y] = original.getSubimage(0, yPos, xPos, ySize);
				yPos -= ySize;
			}
			result[0][0] = original.getSubimage(0, 0, xPos, yPos);
		}

		return result;
	}

	@Deprecated
	private GraphicsUtils() { throw new UnsupportedOperationException(); }
}
