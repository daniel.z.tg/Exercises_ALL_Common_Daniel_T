package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.Objects;

/**
 * A generic pair class to be used for various purposes.
 *
 * @author Daniel Tang
 * @since 20 March 2017
 *
 * @param <L> The type for the left field
 * @param <R> The type for the right field
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Pair<L, R> {

	/**
	 * The left {@link Object} in the {@link Pair}.
	 */
	public final L left;

	/**
	 * The right {@link Object} in the {@link Pair}.
	 */
	public final R right;

	/**
	 * Creates a immutable {@link Object} {@link Pair}ing.
	 *
	 * @param left The {@link Pair#left}-side {@link Object} to be paired.
	 * @param right The {@link Pair#right}-side {@link Object} to be paired.
	 */
	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * Gets the {@link Pair#left}-side {@link Object} in the {@link Pair}.
	 *
	 * @return The {@link Pair#left}-side {@link Object}.
	 */
	public final L getLeft() {
		return this.left;
	}

	/**
	 * Get the {@link Pair#right}-side {@link Object} in the {@link Pair}.
	 *
	 * @return The {@link Pair#right}-side {@link Object}.
	 */
	public final R getRight() {
		return this.right;
	}

	/**
	 * Computes a hash code for this {@link Pair}.
	 * Implemented as just a generic {@link Objects#hash} using the {@link Pair#left} and {@link Pair#right} components.
	 *
	 * @see Object#hashCode()
	 * @return The hash code.
	 */
	@Override
	public final int hashCode() {
		return Objects.hash(this.left, this.right);
	}

	/**
	 * Determines if this {@link Pair} is equal to another {@link Object}.
	 *
	 * Implemented as just checking if the other {@link Object} is a {@link Pair},
	 * and the contained {@link Object}s are equal.
	 *
	 * @return {@code true} if {@link Objects#equals} returns {@code true} for both the contained {@link Objects}s.
	 * @see Object#equals(Object)
	 */
	@Override
	public final boolean equals(final Object other) {
		if (!(other instanceof Pair)) return false;
		final Pair<?, ?> otherPair = (Pair<?, ?>) other;

		return Objects.equals(otherPair.left, otherPair.right);
	}

	/**
	 * Gets an equivalent, but separately modifiable {@link Pair}.
	 * Since {@link Pair}s are immutable, and cannot be modified in the first place,
	 * this just returns the same {@link Pair}.
	 *
	 * @return {@code this}.
	 */
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	@Override
	public Object clone() {
		return this;
	}

	/**
	 * Produces a human-readable {@link String} representation of the {@link Pair}.
	 *
	 * The {@link String} returned is the {@link String#valueOf(Object)} of the {@link Pair#left} {@link Object},
	 * with indicators, and the same for the {@link Pair#right} {@link Object}.
	 *
	 * @return The human-readable {@link String} representation of this {@link Pair}.
	 */
	@Override
	public String toString() {
		return "Pair { L:" + String.valueOf(this.left) + " R:" + String.valueOf(this.right) + " }";
	}
}
