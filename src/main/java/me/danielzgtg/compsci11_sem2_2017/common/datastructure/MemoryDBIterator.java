package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.Iterator;
import java.util.NoSuchElementException;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * An {@link Iterator} for a {@link MemoryDB}.
 *
 * @author Daniel Tang
 * @since 30 April 2017
 *
 * @param <T> The type of the elements in the database.
 */
@SuppressWarnings("WeakerAccess")
public class MemoryDBIterator<T> implements Iterator<T> {

	/**
	 * Instantiates a new {@link MemoryDBIterator}, representing a {@link MemoryDB}.
	 *
	 * @param firstRow The first {@link MemoryDBRow} of the {@link MemoryDB}.
	 */
	/*packaged*/ MemoryDBIterator(final MemoryDBRow<T> firstRow) {
		Validate.notNull(firstRow);

		this.next = firstRow;
	}

	/**
	 * The current element.
	 */
	private MemoryDBRow<T> current;

	/**
	 * The next element.
	 */
	private MemoryDBRow<T> next;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return this.next != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		if (this.next == null) {
			throw new NoSuchElementException();
		}

		this.current = this.next;
		this.next = this.next.after;

		return this.current.data;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		if (this.current == null) {
			throw new IllegalStateException();
		}

		this.current.remove();
		this.current = null;
	}
}
