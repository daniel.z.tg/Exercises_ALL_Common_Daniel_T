package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * An iterator representing an empty {@link Iterable}.
 *
 * @author Daniel Tang
 * @since 30 April 2017
 *
 * @param <T> The type of the {@link Iterable}'s elements to represent.
 */
public class NullIterator<T> implements ListIterator<T> {

	/**
	 * A publicly available reusable instance of {@link NullIterator}.
	 */
	public static final NullIterator INSTANCE = new NullIterator();

	/**
	 * Determines whether there are further elements.
	 * Will always return {@code false}, because the underlying {@link Iterable} is thought to be empty.
	 *
	 * @return {@code false}.
	 */
	@Override
	public boolean hasNext() {
		return false;
	}

	/**
	 * Gets the next element.
	 *
	 * Will always throw a {@link NoSuchElementException},
	 * because the underlying {@link Iterable} is thought to be empty.
	 *
	 * @return Will not return.
	 * @throws NoSuchElementException Always.
	 */
	@Override
	public T next() {
		throw new NoSuchElementException();
	}

	/**
	 * Determines whether there are previous elements.
	 * Will always return {@code false}, because the underlying {@link Iterable} is thought to be empty.
	 *
	 * @return {@code false}.
	 */
	@Override
	public boolean hasPrevious() {
		return false;
	}

	/**
	 * Gets the previous element.
	 *
	 * Will always throw a {@link NoSuchElementException},
	 * because the underlying {@link Iterable} is thought to be empty.
	 *
	 * @return Will not return.
	 * @throws NoSuchElementException Always.
	 */
	@Override
	public T previous() {
		throw new NoSuchElementException();
	}


	/**
	 * Gets the index of the next item.
	 *
	 * Will always return {@code 1},
	 * because the underlying {@link Iterable} is thought to be empty,
	 * and {@link NullIterator#next()} cannot return.
	 *
	 * @return {@code 1}.
	 */
	@Override
	public int nextIndex() {
		return 1;
	}

	/**
	 * Gets the index of the previous item.
	 *
	 * Will always return {@code -1},
	 * because the underlying {@link Iterable} is thought to be empty,
	 * and {@link NullIterator#next()} cannot return.
	 *
	 * @return {@code -1}.
	 */
	@Override
	public int previousIndex() {
		return -1;
	}

	/**
	 * Removes the current element.
	 *
	 * Will always throw a {@link IllegalStateException},
	 * because the underlying {@link Iterable} is thought to be empty,
	 * and {@link NullIterator#next()} cannot return.
	 *
	 * @throws IllegalStateException Always.
	 */
	@Override
	public void remove() {
		throw new IllegalStateException();
	}

	/**
	 * Changes the current element.
	 *
	 * Will always throw a {@link IllegalStateException},
	 * because the underlying {@link Iterable} is thought to be empty,
	 * and {@link NullIterator#next()} cannot return.
	 *
	 * @throws IllegalStateException Always.
	 */
	@Override
	public void set(final Object o) {
		throw new IllegalStateException();
	}

	/**
	 * Adds a new item.
	 *
	 * Will always throw a {@link IllegalStateException},
	 * because the underlying {@link Iterable} is thought to be empty,
	 * and must always stay empty.
	 *
	 * @throws IllegalStateException Always.
	 */
	@Override
	public void add(final Object o) {
		throw new IllegalStateException();
	}
}
