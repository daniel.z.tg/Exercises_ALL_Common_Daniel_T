package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

/**
 * A container of a single {@link volatile boolean}.
 *
 * @since 18 June 2017
 * @author Daniel Tang
 */
public final class Flag {

	/**
	 * Creates a {@link Flag} set to {@code false}.
	 */
	public Flag() {
		this.value = false;
	}

	/**
	 * Creates a {@link Flag} with an initial value.
	 *
	 * @param initialValue The initial state of the flag's value.
	 */
	public Flag(final boolean initialValue) {
		this.value = initialValue;
	}

	/**
	 * The value of this flag.
	 */
	public volatile boolean value;
}
