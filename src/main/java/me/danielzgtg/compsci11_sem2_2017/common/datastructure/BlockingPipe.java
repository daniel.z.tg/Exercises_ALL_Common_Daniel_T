package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A cross-thread blocking pipe.
 *
 * @author Daniel Tang
 * @since 18 June 2017
 *
 * @param <T> The type of packet.
 */
public class BlockingPipe<T> {

	/**
	 * The notify to wake up the listening {@link Thread}.
	 */
	private final Object notify = new Object();

	/**
	 * Used to avoid locking problems with {@link BlockingPipe#notify}.
	 */
	private volatile boolean notified = true;

	/**
	 * The {@link BlockingQueue} to send jobs to the listening {@link Thread}.
	 */
	private final BlockingQueue<Pair<T, Pair<Flag, Container<Exception>>>> jobs =
			new LinkedBlockingQueue<>();

	/**
	 * Whether the listening {@link Thread} should be released.
	 */
	private volatile boolean broken = true;

	/**
	 * Sends a {@link T} packet to the listening {@link Thread},
	 * waits for it to be processed, and propagates {@link Exception}s.
	 *
	 * @param packet The {@link T} to send.
	 * @param block Whether to wait for results.
	 */
	@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
	public final void send(final T packet, final boolean block) {
		Validate.notNull(packet);

		final Container<Exception> result = new Container<>();
		final Pair<T, Pair<Flag, Container<Exception>>> job = new Pair<>(packet, new Pair<>(new Flag(!block), result));

		boolean interrupted = false;

		while (this.broken) {
			try {
				Thread.sleep(1L);
			} catch (final InterruptedException ie) {
				interrupted = true;
			}
		}

		synchronized (job) {
			this.jobs.add(job);

			synchronized (this.notify) {
				if (!this.notified) {
					this.notify.notify();
					this.notified = true;
				}
			}

			if (block) {
				try {
					job.wait();
				} catch (final InterruptedException ie) {
					interrupted = true;
				}
			}
		}

		if (block) {
			if (interrupted) {
				final Flag f = job.getRight().getLeft();
				while (!f.value) {
					try {
						Thread.sleep(1L);
					} catch (final InterruptedException ignore) {}
				}

				Thread.currentThread().interrupt();
			}

			if (result.value != null) {
				throw new RuntimeException(result.value);
			}
		}
	}

	/**
	 * Releases the listening {@link Thread}.
	 */
	public final void release() {
		this.broken = true;

		synchronized (this.notify) {
			if (!this.notified) {
				this.notify.notify();
				this.notified = true;
			}
		}
	}

	/**
	 * Traps a listening {@link Thread}, and makes it process packets.
	 *
	 * @param processor The {@link Consumer} of {@link T} packets.
	 */
	@SuppressWarnings({"SynchronizationOnLocalVariableOrMethodParameter", "InfiniteLoopStatement"})
	public final void trap(final Consumer<T> processor) {
		Validate.notNull(processor);

		if (!this.broken) {
			throw new IllegalArgumentException();
		}

		this.broken = false;

		do {
			Pair<T, Pair<Flag, Container<Exception>>> job;
			while ((job = this.jobs.poll()) != null) {
				synchronized (job) {
					try {
						final T packet = job.getLeft();
						Validate.notNull(packet);

						processor.accept(packet);

						final Flag flag = job.getRight().getLeft();
						if (!flag.value) {
							job.notify();
							flag.value = true;
						}
					} catch (final Exception e) {
						job.getRight().getRight().value = e;
					}
				}
			}

			synchronized (this.notify) {
				this.notified = false;

				try {
					this.notify.wait();
				} catch (final InterruptedException ignore) {}
			}
		} while (!this.broken);
	}
}
