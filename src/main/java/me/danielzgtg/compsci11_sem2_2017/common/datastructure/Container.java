package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

/**
 * A container of a single {@link T}.
 *
 * @author Daniel Tang
 * @since 18 June 2017
 *
 * @param <T> The type fo the container's contents.
 */
public final class Container<T> {

	/**
	 * Creates a {@link Container} containing {@code null}.
	 */
	public Container() {
		this.value = null;
	}

	/**
	 * Creates a {@link Container} with the initial value.
	 *
	 * @param initialValue The initial contents of this container.
	 */
	public Container(final T initialValue) {
		this.value = initialValue;
	}

	/**
	 * The value of this container.
	 */
	public volatile T value;
}
