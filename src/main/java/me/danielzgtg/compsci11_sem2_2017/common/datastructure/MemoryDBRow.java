package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.function.Function;

/**
 * A row in a {@link MemoryDB}.
 *
 * @author Daniel Tang
 * @since 28 April 2017
 *
 * @param <T> The type of item in the {@link MemoryDB}.
 */
@SuppressWarnings({"UnusedReturnValue", "unused", "WeakerAccess"})
public final class MemoryDBRow<T> {
	/**
	 * The contained data.
	 */
	/*packaged*/ volatile T data;

	/**
	 * The parent {@link MemoryDB} that this {@link MemoryDBRow} is or was in.
	 */
	private final MemoryDB<T> parent;

	/**
	 * The {@link MemoryDBRow} that is before this one, or was before its removal.
	 */
	/*packaged*/ volatile MemoryDBRow<T> before;

	/**
	 * The {@link MemoryDBRow} that is after this one, or was before its removal.
	 */
	/*packaged*/ volatile MemoryDBRow<T> after;

	/**
	 * A flag set to {@code true} when this {@link MemoryDBRow} is no longer part of its parent {@link MemoryDB}.
	 */
	private volatile boolean removed = false;

	/**
	 * Creates a new {@link MemoryDBRow}. This method is for internal use only for {@link MemoryDB}-related classes.
	 *
	 * @param data The initial data.
	 * @param parent The parent {@link MemoryDB}.
	 */
	/*packaged*/ MemoryDBRow(final T data, final MemoryDB<T> parent) {
		assert parent != null;

		this.data = data;
		this.parent = parent;
	}

	/**
	 * Gets the parent {@link MemoryDB} that this {@link MemoryDBRow} is or was in.
	 * @return The parent {@link MemoryDB} that this {@link MemoryDBRow} is or was in.
	 */
	public final MemoryDB<T> getParent() {
		return this.parent;
	}

	/**
	 * Gets the contained data.
	 *
	 * @return The contained data.
	 */
	public final T getData() {
		return this.data;
	}

	/**
	 * Sets the contained data.
	 *
	 * @param data The new data.
	 * @return The previous data.
	 */
	public final synchronized T setData(final T data) {
		final T previous = this.data;

		this.data = data;

		return previous;
	}

	/**
	 * Applies a {@link Function} to transform the contained data.
	 *
	 * @param transformer The {@link Function} to transform the contained data with.
	 */
	public final synchronized void transformData(final Function<T, T> transformer) {
		this.data = transformer.apply(this.data);
	}

	/**
	 * Inserts a new {@link MemoryDBRow} contained the specified data, after this row.
	 *
	 * @param data The data to insert.
	 * @return The created {@link MemoryDBRow} if insertion was successful, else {@link null}.
	 */
	public final MemoryDBRow<T> insertAfter(final T data) {
		synchronized (this.parent) {
			if (this.removed) return null;

			final MemoryDBRow<T> row = new MemoryDBRow<>(data, this.parent);

			if (this.after == null) {
				this.parent.lastRow = row;
			}

			row.before = this;
			row.after = this.after;
			this.after = row;

			this.parent.size++;

			return row;
		}
	}

	/**
	 * Inserts a new {@link MemoryDBRow} contained the specified data, before this row.
	 *
	 * @param data The data to insert.
	 * @return The created {@link MemoryDBRow} if insertion was successful, else {@link null}.
	 */
	public final MemoryDBRow<T> insertBefore(final T data) {
		synchronized (this.parent) {
			if (this.removed) return null;

			final MemoryDBRow<T> row = new MemoryDBRow<>(data, this.parent);

			if (this.before == null) {
				this.parent.firstRow = row;
			}

			row.after = this;
			row.before = this.before;
			this.before = row;

			this.parent.size++;

			return row;
		}
	}

	/**
	 * Removes this {@link MemoryDBRow} from its parent {@link MemoryDB}.
	 *
	 * @return The contained contents, same as if {@link MemoryDBRow#getData()} was called instead.
	 */
	public final T remove() {
		synchronized (this.parent) {
			if (this.removed) return this.data;
			this.removed = true;

			if (this.before == null) {
				this.parent.firstRow = this.after;
			} else {
				this.before.after = this.after;
			}

			if (this.after == null) {
				this.parent.lastRow = this.before;
			} else {
				this.after.before = this.before;
			}

			this.parent.size--;
		}

		return this.data;
	}

	/**
	 * Gets the {@link MemoryDBRow} that is after this one, or was before its removal.
	 *
	 * @return The {@link MemoryDBRow} that is after this one, or was before its removal.
	 */
	public final MemoryDBRow<T> next() {
		return this.after;
	}

	/**
	 * Gets the {@link MemoryDBRow} that is before this one, or was before its removal.
	 *
	 * @return The {@link MemoryDBRow} that is before this one, or was before its removal.
	 */
	public final MemoryDBRow<T> prev() {
		return this.before;
	}
}
