package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Some common lambda functions.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings({"unused", "JavaDoc"})
public final class CommonLambdas {

	public static final Runnable NOP_RUNNABLE = () -> {};
	public static final Consumer NOP_CONSUMER = (x) -> {};
	public static final BiConsumer NOP_BICONSUMER = (a, b) -> {};
	public static final Supplier NULL_SUPPLIER = () -> null;
	public static final Function NULL_FUNCTION = (x) -> null;
	public static final BiFunction NULL_BIFUNCTION = (a, b) -> null;
	public static final BiFunction FIRSTARG_BIFUNCTION = (a, b) -> a;
	public static final BiFunction SECONDARG_BIFUNCTION = (a, b) -> b;
	public static final Function PASSTHROUGH_FUNCTION = (x) -> x;
	public static final Predicate TRUE_PREDICATE = (x) -> true;
	public static final Predicate FALSE_PREDICATE = (x) -> false;
	public static final Predicate NULL_PREDICATE = Objects::isNull;
	public static final Predicate NONNULL_PREDICATE = Objects::nonNull;
	public static final BooleanSupplier TRUE_BOOLEANSUPPLIER = () -> true;
	public static final BooleanSupplier FALSE_BOOLEANSUPPLIER = () -> false;

	@Deprecated
	private CommonLambdas() { throw new UnsupportedOperationException(); }
}
