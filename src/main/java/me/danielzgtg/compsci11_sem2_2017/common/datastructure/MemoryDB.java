package me.danielzgtg.compsci11_sem2_2017.common.datastructure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A simple memory-backed database.
 *
 * @author Daniel Tang
 * @since 28 April 2017
 *
 * @param <T> The type of database item.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class MemoryDB<T> implements List<T> {

	/**
	 * A reference to the first {@link MemoryDBRow}.
	 */
	/*packaged*/ volatile MemoryDBRow<T> firstRow;

	/**
	 * A reference to the last {@link MemoryDBRow}.
	 */
	/*packaged*/ volatile MemoryDBRow<T> lastRow;

	/**
	 * The size of the {@link MemoryDB}.
	 */
	/*packaged*/ volatile int size = 0;

	/**
	 * Creates a new empty {@link MemoryDB}.
	 */
	public MemoryDB() {
	}

	/**
	 * Creates a {@link MemoryDB}, with the initial contents copied from the specified {@link Collection}.
	 *
	 * @param c The {@link Collection} to copy the initial contents from.
	 */
	public MemoryDB(final Collection<T> c) {
		this();
		this.addAll(c);
	}

	/**
	 * Adds an item to the beginning of the {@link MemoryDB}.
	 *
	 * @param o The {@link T} to add.
	 */
	public final void addFirst(final T o) {
		final MemoryDBRow<T> row = new MemoryDBRow<>(o, this);

		synchronized (this) {
			if (this.firstRow != null) {
				this.firstRow.before = row;
				row.after = this.firstRow;
			}

			if (this.size == 0) {
				this.lastRow = row;
			}

			//row.before = null;
			this.firstRow = row;

			this.size++;
		}
	}

	/**
	 * Adds an item to the end of the {@link MemoryDB}.
	 *
	 * @param o The {@link T} to add.
	 */
	public final void addLast(final T o) {
		final MemoryDBRow<T> row = new MemoryDBRow<>(o, this);

		synchronized (this) {
			if (this.lastRow != null) {
				this.lastRow.after = row;
				row.before = this.lastRow;
			}

			if (this.size == 0) {
				this.firstRow = row;
			}

			//row.after = null;
			this.lastRow = row;

			this.size++;
		}
	}

	/**
	 * Removes {@link MemoryDBRow}s if the {@link Predicate} agrees.
	 *
	 * @param selector The {@link Predicate} to select {@link MemoryDBRow}s.
	 */
	public final void remove(final Predicate<T> selector) {
		Validate.notNull(selector);

		MemoryDBRow<T> row = this.firstRow;
		synchronized (this) {
			while (row != null) {
				if (selector.test(row.data)) {
					row.remove();
				}

				row = row.after;
			}
		}
	}

	/**
	 * Runs a {@link Consumer} on the data of {@link MemoryDBRow}s if the {@link Predicate} agrees.
	 *
	 * @param selector The {@link Predicate} to select {@link MemoryDBRow}s.
	 * @param action The {@link Consumer} to run on the data.
	 */
	public final void forEach(final Predicate<T> selector, final Consumer<T> action) {
		Validate.notNull(selector);
		Validate.notNull(action);

		MemoryDBRow<T> row = this.firstRow;
		//synchronized (this) {
		while (row != null) {
			final T data = row.data;

			if (selector.test(data)) {
				action.accept(data);
			}

			row = row.after;
		}
		//}
	}

	/**
	 * Runs a {@link Function} to change the data of {@link MemoryDBRow}s if the {@link Predicate} agrees.
	 *
	 * @param selector The {@link Predicate} to select {@link MemoryDBRow}s.
	 * @param transformer The {@link Function} to change the data.
	 */
	public final void transform(final Predicate<T> selector, final Function<T, T> transformer) {
		Validate.notNull(selector);
		Validate.notNull(transformer);

		MemoryDBRow<T> row = this.firstRow;
		//synchronized (this) {
		while (row != null) {
			final T data = row.data;

			if (selector.test(data)) {
				row.transformData(transformer);
			}

			row = row.after;
		}
		//}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(final Object o) {
		MemoryDBRow<T> row = this.firstRow;
		//synchronized (this) {
		while (row != null) {
			final T data = row.data;

			if (Objects.equals(data, o)) {
				return true;
			}

			row = row.after;
		}
		//}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Iterator<T> iterator() {
		final MemoryDBRow<T> firstRow = this.firstRow;

		if (firstRow == null) {
			return NullIterator.INSTANCE;
		} else {
			return new MemoryDBIterator<>(firstRow);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		final Object[] result;
		final int size;

		synchronized (this) {
			size = this.size;
			result = new Object[size];
		}

		if (result.length == 0) {
			return result;
		}

		MemoryDBRow<T> row = this.firstRow;

		for (int i = 0; i < size; i++) {
			if (row == null) {
				throw new ConcurrentModificationException();
			}

			result[i] = row.data;
			row = row.after;
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(final T o) {
		this.addLast(o);

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(final Object o) {
		MemoryDBRow<T> row = this.firstRow;
		synchronized (this) {
			while (row != null) {
				if (Objects.equals(row.data, o)) {
					row.remove();
					return true;
				}

				row = row.after;
			}
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(final Collection<? extends T> c) {
		//if (c == null) {
		//	throw new NullPointerException();
		//}

		if (c.isEmpty()) {
			return false;
		}

		synchronized (this) {
			for (final T item : c) {
				this.addLast(item);
			}
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(final int index, final Collection<? extends T> c) {
		//if (c == null) {
		//	throw new NullPointerException();
		//}

		if (c.isEmpty()) {
			return false;
		}

		if (index != 0) {
			MemoryDBRow<T> row = this.getRow(index);
			synchronized (this) {
				for (final T item : c) {
					assert row != null;

					row = row.insertBefore(item);
				}
			}
		} else {
			synchronized (this) {
				for (final T item : c) {
					this.addFirst(item);
				}
			}
		}

		return true;
	}

	@Override
	public void clear() {
		MemoryDBRow<T> row = this.firstRow;
		synchronized (this) {
			while (row != null) {
				row.remove();

				row = row.after;
			}
		}
	}

	/**
	 * Gets the {@link MemoryDBRow} at the specified index.
	 * (not the data at the specified index like {@link MemoryDB#get(int)})
	 *
	 * @param index The index of the requested row.
	 * @return The {@link MemoryDBRow} at the specified index.
	 */
	public MemoryDBRow<T> getRow(final int index) {
		synchronized (this) {
			if (this.size == 0 || index >= this.size) {
				throw new IndexOutOfBoundsException(String.valueOf(index));
			}

			if (index == 0) {
				return this.firstRow;
			}

			if (index == (this.size - 1)) {
				return this.lastRow;
			}

			MemoryDBRow<T> row = this.firstRow;
			for (int i = 0; i < index; i++) {
				row = row.after;
			}
			return row;
		}
	}

	/**
	 * Gets a {@link List} of all the rows in this {@link MemoryDB}.
	 * (not a {@link List} of all of the row data as if using this {@link MemoryDB} directly)
	 *
	 * @return A {@link List} of all the rows in this {@link MemoryDB}.
	 */
	public List<MemoryDBRow<T>> getAllRows() {
		final List<MemoryDBRow<T>> result = new LinkedList<>();

		MemoryDBRow<T> row = this.firstRow;
		//synchronized (this) {
		while (row != null) {
			result.add(row);

			row = row.after;
		}
		//}

		return Collections.unmodifiableList(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get(final int index) {
		return this.getRow(index).data;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T set(final int index, final T data) {
		return this.getRow(index).setData(data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final int index, final T element) {
		if (index == 0) {
			this.addFirst(element);
		} else {
			this.getRow(index - 1).insertAfter(element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T remove(final int index) {
		return this.getRow(index).remove();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int indexOf(final Object o) {
		int index = 0;
		MemoryDBRow<T> row = this.firstRow;

		if (row == null) {
			return -1;
		}

		while (true) {
			if (Objects.equals(row.data, o)) {
				return index;
			}

			row = row.after;

			if (row == null) {
				return -1;
			}

			index++;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int lastIndexOf(final Object o) {
		int index = this.size;
		MemoryDBRow<T> row = this.lastRow;

		while (true) {
			if (row == null) {
				return -1;
			}

			index--;

			if (Objects.equals(row.data, o)) {
				return index;
			}

			row = row.before;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<T> listIterator() {
		throw new UnsupportedOperationException("NYI");
		// TODO implement
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<T> listIterator(final int index) {
		throw new UnsupportedOperationException("NYI");
		// TODO implement
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<T> subList(final int fromIndex, final int toIndex) {
		if (fromIndex < 0 || fromIndex > toIndex) {
			throw new IndexOutOfBoundsException();
		}

		if (fromIndex == toIndex) {
			return Collections.emptyList();
		}

		final int length = toIndex - fromIndex + 1;
		final List<T> result = new ArrayList<>(length);

		MemoryDBRow<T> row;
		synchronized (this) {
			if (toIndex >= this.size) {
				throw new IndexOutOfBoundsException();
			}

			row = this.getRow(fromIndex);
		}

		for (int i = 0; i < length; i++) {
			if (row == null) {
				throw new ConcurrentModificationException();
			}

			result.add(row.data);
			row = row.after;
		}

		return Collections.unmodifiableList(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean retainAll(final Collection c) {
		if (c == null) {
			throw new NullPointerException();
		}

		boolean result = false;
		MemoryDBRow<T> row = this.firstRow;
		while (row != null) {
			if (!c.contains(row.data)) {
				row.remove();
				result = true;
			}

			row = row.after;
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeAll(final Collection c) {
		if (c == null) {
			throw new NullPointerException();
		}

		boolean result = false;
		MemoryDBRow<T> row = this.firstRow;
		while (row != null) {
			if (c.contains(row.data)) {
				row.remove();
				result = true;
			}

			row = row.after;
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsAll(final Collection c) {
		if (c == null) {
			throw new NullPointerException();
		}

		for (final Object o : c) {
			if (!this.contains(o)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] toArray(final Object[] a) {
		final Object[] result;
		final int size;

		synchronized (this) {
			size = this.size;
			result = (a == null || a.length < size) ? new Object[size] : a;
		}

		if (result.length == 0) {
			return result;
		}

		MemoryDBRow<T> row = this.firstRow;

		for (int i = 0; i < size; i++) {
			if (row == null) {
				throw new ConcurrentModificationException();
			}

			result[i] = row.data;
			row = row.after;
		}

		return result;
	}
}
