package me.danielzgtg.compsci11_sem2_2017.common;

/**
 * Represents something that needs to update itself.
 *
 * @author Daniel Tang
 * @since 16 June 2017
 */
public abstract interface Tickable {

	/**
	 * Requests this {@link Tickable} to self-update.
	 */
	public abstract void tick();
}
