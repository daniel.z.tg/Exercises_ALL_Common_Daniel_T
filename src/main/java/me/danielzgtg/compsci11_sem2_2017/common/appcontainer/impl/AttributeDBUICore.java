package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDB;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Attribute;
import me.danielzgtg.compsci11_sem2_2017.common.ui.HumanDataManipulator;

/**
 * The underlying interaction between the user interface and the database for an AttributeDB app.
 *
 * @param <T> The type of database row contents.
 *
 * @author Daniel Tang
 * @since 3 May 2017
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue"})
public final class AttributeDBUICore<T> {

	// Type data
	/**
	 * The class of the type of {@link MemoryDBRow} contents.
	 */
	public final Class<T> type;

	/**
	 * The {@link Attribute}s of the {@link MemoryDBRow} contents that will be shown to the user.
	 *
	 * The user will be able to do zero, one, or both of the following to the contents: view, modify.
	 * The stored data is visualized to the user, and user data is interpreted into stored data,
	 * using the {@link HumanDataManipulator}.
	 */
	public final List<Attribute<T, Object>> attributes;

	// Data reference
	/**
	 * The target {@link MemoryDB} database.
	 */
	public final MemoryDB<T> database;

	// UI data
	/**
	 * The list of {@link MemoryDBRow} the user has selected.
	 */
	public final List<MemoryDBRow<T>> selection;

	/**
	 * Instantiates a new {@link AttributeDBUICore}, used internally.
	 *
	 * This simply copies the parameters into the instance constants, without any checking.
	 *
	 * @param type The class of the type of {@link MemoryDBRow} contents.
	 * @param attributes The {@link Attribute}s of the {@link MemoryDBRow} contents that will be shown to the user.
	 * @param database The target {@link MemoryDB}.
	 * @param selection The list of {@link MemoryDBRow} the user has selected.
	 */
	private AttributeDBUICore(
			final Class<T> type,
			final List<Attribute<T, Object>> attributes,
			final MemoryDB<T> database,
			final List<MemoryDBRow<T>> selection) {
		this.type = type;
		this.attributes = attributes;
		this.database = database;
		this.selection = selection;
	}

	/**
	 * Creates a new {@link AttributeDBUICore} with the specified parameters.
	 *
	 * @param type The class of the type of {@link MemoryDBRow} contents, not null.
	 * @param attributes The {@link Attribute}s of the {@link MemoryDBRow} contents
	 *                   that will be shown to the user, not null.
	 * @param database The target {@link MemoryDB} database, not null.
	 * @param <T> The type of the type of database row contents.
	 * @return A new {@link AttributeDBUICore} containing the specified parameters.
	 */
	public static final <T> AttributeDBUICore<T> of(
			final Class<T> type,
			final List<Attribute<T, Object>> attributes,
			final MemoryDB<T> database) {
		Validate.notNull(type);
		Validate.notNull(attributes);
		Validate.notNull(database);

		final List<Attribute<T, Object>> attributesImmutable =
				Collections.unmodifiableList(new LinkedList<>(attributes));

		for (final Attribute<T, Object> attribute : attributesImmutable) {
			Validate.notNull(attribute);
		}

		return new AttributeDBUICore<>(type, attributesImmutable, database, new LinkedList<>());
	}

	/**
	 * Retrieves the class of the type of {@link MemoryDBRow} contents.
	 *
	 * @return The class of the type of {@link MemoryDBRow} contents.
	 */
	public final Class<T> getType() {
		return this.type;
	}

	/**
	 * Retrieves the {@link Attribute}s of the {@link MemoryDBRow} contents that will be shown to the user.
	 *
	 * @return The {@link Attribute}s of the {@link MemoryDBRow} contents that will be shown to the user.
	 */
	public final List<Attribute<T, Object>> getAttributes() {
		return this.attributes;
	}

	/**
	 * Retrieves the target {@link MemoryDB} database.
	 *
	 * @return The target {@link MemoryDB} database.
	 */
	public final MemoryDB<T> getDatabase() {
		return this.database;
	}

	/**
	 * Retrieves the list of {@link MemoryDBRow} the user has selected.
	 *
	 * @return The list of {@link MemoryDBRow} the user has selected.
	 */
	public final List<MemoryDBRow<T>> getSelection() {
		return this.selection;
	}

	/**
	 * Clears the selection.
	 */
	public final void clearSelection() {
		this.selection.clear();
	}

	/**
	 * Sets the selection to the entire database.
	 */
	public final void fillSelection() {
		this.selection.clear();
		this.selection.addAll(this.database.getAllRows());
	}

	/**
	 * Computes a hash code for this {@link AttributeDBUICore}.
	 *
	 * Implemented as hashing only the type properties
	 * ({@link AttributeDBUICore#type} and {@link AttributeDBUICore#attributes})
	 * using their {@link Object#hashCode()} methods.
	 *
	 * @see Class#hashCode()
	 * @see List#hashCode()
	 * @return The hash code.
	 */
	@Override
	public final int hashCode() {
		return this.type.hashCode() ^ this.attributes.hashCode();
		// Don't hash mutable attributes to keep hash code constant.
		//^ this.database.hashCode() ^ this.selection.hashCode();
	}

	/**
	 * Determines if this {@link AttributeDBUICore} is equal to another {@link Object}
	 *
	 * Implemented as just checking if the other {@link Object} is an {@link AttributeDBUICore},
	 * and the contained {@link Object}s are equal.
	 *
	 * @return {@code true} if {@link Object#equals(Object)} returns {@code true}
	 *         for all of the {@link AttributeDBUICore}s' properties.
	 */
	@Override
	public final boolean equals(final Object obj) {
		if (!(obj instanceof AttributeDBUICore)) {
			return false;
		}

		final AttributeDBUICore other = (AttributeDBUICore) obj;

		return this.type.equals(other.type) && this.attributes.equals(other.attributes)
				&& this.database.equals(other.database) && this.selection.equals(other.selection);
	}

	/**
	 * Gets an equivalent, but separately modifiable {@link AttributeDBUICore}.
	 *
	 * This copies the type data ({@link AttributeDBUICore#type} and {@link AttributeDBUICore#attributes}),
	 * and the database reference ({@link AttributeDBUICore#database}), but clones
	 * the selection ({@link AttributeDBUICore#selection}).
	 *
	 * @return An equivalent, but separately modifiable {@link AttributeDBUICore}.
	 */
	@Override
	public final AttributeDBUICore<T> clone() {
		return new AttributeDBUICore<>(this.type, this.attributes, this.database, new LinkedList<>(this.selection));
	}

	/**
	 * Produces a human-readable {@link String} representation of the {@link AttributeDBUICore}.
	 *
	 * The {@link String} returned is the
	 * {@link Class#getName()} of the {@link AttributeDBUICore#type}, and the
	 * {@link String#valueOf(Object)} of
	 * the {@link AttributeDBUICore#attributes}, the {@link AttributeDBUICore#database},
	 * and the {@link AttributeDBUICore#selection}, all with indicators.
	 *
	 * @return The human-readable {@link String} representation of this {@link AttributeDBUICore}.
	 */
	@Override
	public final String toString() {
		return "AttributeDBUICore: { type=" + this.type.getName() + ", attributes=" + this.attributes
				+ ", database=" + this.database + ", selection=" + this.selection + "}";
	}

	/**
	 * Determines is a {@link MemoryDBRow} passes the specified filter.
	 *
	 * @param row The {@link MemoryDBRow} to check.
	 * @param excludeElseInclude Whether the specified {@link Predicate} includes or excludes.
	 * @param particulars The {@link Predicate} to use to test.
	 * @return {@code true} if it passed the specified filter.
	 */
	public final boolean passesFilter(final MemoryDBRow<T> row, final boolean excludeElseInclude,
			final Predicate<T> particulars) {
		return excludeElseInclude ^ particulars.test(row.getData());
	}

	/**
	 * Filters further the selection of {@link MemoryDBRow}s to the specified filter.
	 *
	 * @param excludeElseInclude Whether the specified {@link Predicate} includes or excludes.
	 * @param particulars The {@link Predicate} to use to test.
	 */
	public final void applyFilter(final boolean excludeElseInclude, final Predicate<T> particulars) {
		final List<MemoryDBRow<T>> toRemove = new LinkedList<>();

		for (final MemoryDBRow<T> row : this.selection) {
			if (!excludeElseInclude ^ this.passesFilter(row, excludeElseInclude, particulars)) {
				toRemove.add(row);
			}
		}

		this.selection.removeAll(toRemove);
	}

	/**
	 * Determines is a {@link MemoryDBRow} passes the specified simple filter parameters.
	 *
	 * @param row The {@link MemoryDBRow} to check.
	 * @param excludeElseInclude Whether the specified filter includes or excludes.
	 * @param typeFilter The {@link Class} to filter the {@link MemoryDBRow} with.
	 * @param exactType Whether the type filter does not accepts subclasses.
	 * @param attributeFilters The map of {@link Attribute} data to required data.
	 * @param exceptionHandler The handler to call when a {@link ReflectiveOperationException} is encountered.
	 * @return {@code true} if it passed the specified filter.
	 */
	public final boolean passesFilter(final MemoryDBRow<T> row, final boolean excludeElseInclude,
			final Class<? extends T> typeFilter, final boolean exactType,
			final Map<Attribute<T, Object>, Object> attributeFilters,
			final Consumer<ReflectiveOperationException> exceptionHandler) {
		final T data = row.getData();
		if (typeFilter != null) {
			if (!excludeElseInclude ^ (exactType ?
					typeFilter.equals(data.getClass()) :
					typeFilter.isAssignableFrom(data.getClass()))) {
				return false;
			}
		}

		if (attributeFilters.isEmpty()) {
			return excludeElseInclude;
		} else {
			for (final Map.Entry<Attribute<T, Object>, Object> filter : attributeFilters.entrySet()) {
				try {
					if (!excludeElseInclude ^ filter.getValue().equals(filter.getKey().get(data))) {
						return false;
					}
				} catch (final ReflectiveOperationException e) {
					if (exceptionHandler != null) {
						exceptionHandler.accept(e);
					}
				}
			}
		}

		// Did not fail, must have passed
		return true;
	}

	/**
	 * Filters further the selection of {@link MemoryDBRow}s to the simple filter parameters.
	 *
	 * @param excludeElseInclude Whether the specified filter includes or excludes.
	 * @param typeFilter The {@link Class} to filter the {@link MemoryDBRow}s with.
	 * @param exactType Whether the type filter does not accepts subclasses.
	 * @param attributeFilters The map of {@link Attribute} data to required data.
	 */
	public final void applySimpleFilter(final boolean excludeElseInclude,
			final Class<? extends T> typeFilter, final boolean exactType,
			final Map<Attribute<T, Object>, Object> attributeFilters) {
		final List<MemoryDBRow<T>> toRemove = new LinkedList<>();

		for (final MemoryDBRow<T> row : this.selection) {
			if (!this.passesFilter(
					row, excludeElseInclude, typeFilter, exactType, attributeFilters, null)) {
				toRemove.add(row);
			}
		}

		this.selection.removeAll(toRemove);
	}
}
