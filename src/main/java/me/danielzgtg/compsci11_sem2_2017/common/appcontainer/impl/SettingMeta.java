package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.function.Function;
import java.util.function.Predicate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Some metadata describing a setting.
 *
 * @param <T> The type of setting value.
 *
 * @author Daniel Tang
 * @since 21 May 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class SettingMeta<T> {

	/**
	 * The class of the type of this setting.
	 */
	public final Class<T> type;

	/**
	 * The id of where this setting is stored.
	 */
	public final String id;

	/**
	 * The human-readable label of this setting.
	 */
	public final String label;

	/**
	 * The {@link Predicate} validating whether the input is correct.
	 */
	public final Predicate<Object> validator;

	/**
	 * The {@link Function} that describes the setting's current value, when it is valid.
	 */
	public final Function<Object, String> descriptionFunc;

	/**
	 * The {@link Function} that describes the setting's current value, when it is not valid.
	 */
	public final Function<Object, String> invalidDescriptionFunc;

	/**
	 * Creates new metadata describing a setting.
	 *
	 * @param type The class of the type of this setting, not null.
	 * @param id The id of where this setting is stored, not null.
	 * @param label The human-readable label of this setting, not null.
	 * @param validator The {@link Predicate} validating whether the input is correct, not null.
	 * @param descriptionFunc The {@link Function} that describes the setting's current value,
	 *                        when it is valid, not null.
	 * @param invalidDescriptionFunc The {@link Function} that describes the setting's current value,
	 *                               when it is not valid, not null.
	 */
	public SettingMeta(final Class<T> type, final String id, final String label, final Predicate<Object> validator,
			final Function<Object, String> descriptionFunc, final Function<Object, String> invalidDescriptionFunc) {
		Validate.notNull(type);
		Validate.notNull(id);
		Validate.notNull(label);
		Validate.notNull(validator);
		Validate.notNull(descriptionFunc);
		Validate.notNull(invalidDescriptionFunc);

		this.type = type;
		this.id = id;
		this.label = label;
		this.validator = validator;
		this.descriptionFunc = descriptionFunc;
		this.invalidDescriptionFunc = invalidDescriptionFunc;
	}

	/**
	 * Computes a hash code for this {@link SettingMeta}.
	 * Implemented as XORing all of the fields' hash codes together.
	 *
	 * @see Object#hashCode()
	 * @return The hash code.
	 */
	@Override
	public int hashCode() {
		return this.type.hashCode() ^ this.id.hashCode() ^ this.validator.hashCode() ^
				this.descriptionFunc.hashCode() ^ this.invalidDescriptionFunc.hashCode();
	}

	/**
	 * Determines if this {@link SettingMeta} is equal to another {@link Object}.
	 *
	 * Implemented as just checking if each of the fields are equal to each other.
	 *
	 * @return {@code true} if the {@link Object} is equal.
	 */
	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof SettingMeta)) {
			return false;
		}

		final SettingMeta other = (SettingMeta) o;

		return this.type.equals(other.type) && this.id.equals(other.id) && this.validator.equals(other.validator) &&
				this.descriptionFunc.equals(other.descriptionFunc) &&
				this.invalidDescriptionFunc.equals(other.descriptionFunc);

	}

	/**
	 * Gets an equivalent, but separately modifiable {@link SettingMeta}.
	 * Since {@link SettingMeta}s are immutable, and cannot be modified in the first place,
	 * this just returns the same {@link SettingMeta}.
	 *
	 * @return {@code this}.
	 */
	@Override
	protected Object clone() {
		return this;
	}

	/**
	 * Produces a human-readable {@link String} representation of the {@link SettingMeta}.
	 * This just returns {@code "SettingMeta: " + } {@link SettingMeta#id}.
	 *
	 * @return A human-readable {@link String} representation of the {@link SettingMeta}.
	 */
	@Override
	public String toString() {
		return "SettingMeta: " + this.id;
	}

	/**
	 * Gets the class of the type of this setting.
	 *
	 * @return The class of the type of this setting.
	 */
	public Class<T> getType() {
		return type;
	}

	/**
	 * Gets the id of where this setting is stored.
	 *
	 * @return The id of where this setting is stored.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Gets the human-readable label of this setting.
	 *
	 * @return The human-readable label of this setting.
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * Gets the {@link Predicate} validating whether the input is correct.
	 *
	 * @return The {@link Predicate} validating whether the input is correct.
	 */
	public Predicate<Object> getValidator() {
		return this.validator;
	}

	/**
	 * Gets the {@link Function} that describes the setting's current value, when it is valid.
	 *
	 * @return The {@link Function} that describes the setting's current value, when it is valid.
	 */
	public Function<Object, String> getDescriptionFunc() {
		return this.descriptionFunc;
	}

	/**
	 * Gets the {@link Function} that describes the setting's current value, when it is not valid.
	 *
	 * @return The {@link Function} that describes the setting's current value, when it is not valid.
	 */
	public Function<Object, String> getInvalidDescriptionFunc() {
		return this.invalidDescriptionFunc;
	}
}
