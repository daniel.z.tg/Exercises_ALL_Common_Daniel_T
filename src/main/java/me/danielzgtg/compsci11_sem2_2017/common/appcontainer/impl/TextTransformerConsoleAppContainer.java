package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.ui.CommandDrivenCLI;
import me.danielzgtg.compsci11_sem2_2017.common.ui.PrintMessenger;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Prompter;

/**
 * A CLI app container for simple text transformer apps.
 *
 * @author Daniel Tang
 * @since 16 April 2017
 */
@SuppressWarnings("unused")
public final class TextTransformerConsoleAppContainer extends AppContainer {

	/**
	 * The actual text transformer.
	 */
	private final Function<String, String> textTransformer;

	/**
	 * The message to display before the app logic is run.
	 */
	private final String introduction;

	/**
	 * The message to display after the app logic is finished.
	 */
	private final String conclusion;

	/**
	 * The underlying {@link CommandDrivenCLI} that this {@link TextTransformerConsoleAppContainer} uses.
	 */
	private /*final*/ CommandDrivenCLI<Object> backend;

	/**
	 * The {@link PrintMessenger} for showing the user some data.
	 */
	private final PrintMessenger messenger;

	/**
	 * Creates a console text transformer container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param textTransformer The text transformer {@link Function}.
	 * @param introduction The message to show before the translation interface loop.
	 * @param conclusion The message to show after the translation interface loop.
	 * @param prompter The {@link Prompter} to obtain input with, not null.
	 * @param messenger The {@link PrintMessenger} to show the user some data with, not null.
	 */
	@SuppressWarnings("unchecked")
	public TextTransformerConsoleAppContainer(final Map<?, ?> constantsMap,
			final Function<String, String> textTransformer,
			final String introduction, final String conclusion,
			final Prompter prompter,
			final PrintMessenger messenger) {
		// Early Validation
		Validate.notNull(textTransformer);
		Validate.notNull(introduction);
		Validate.notNull(conclusion);
		Validate.notNull(constantsMap);
		Validate.notNull(prompter);
		Validate.notNull(messenger);

		final String translationSourceTextMissingErr = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.texttransformerconsole.translationSourceTextMissingErr"));

		final String translationBackendFunctionErrorMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.texttransformerconsole.translationBackendFunctionErrorMsg"));

		final Map<String, BiFunction<CommandDrivenCLI<Object>, String[], Object>> commands = new HashMap<>();

		CommandDrivenCLI.addQuitFunctions(constantsMap, commands);

		this.textTransformer = textTransformer; // Required for callback
		commands.put(Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.texttransformerconsole.translateCommand")), (system, args)
				-> {
			if (args.length < 2) {
				messenger.println(translationSourceTextMissingErr);
			} else {
				try {
					messenger.println(this.textTransformer.apply(StringUtils.joinStrings(
							// Remove "translate" command name, and re-add spaces
							Arrays.copyOfRange(args, 1, args.length), " ")));
				} catch (final Exception e) {
					messenger.println(translationBackendFunctionErrorMsg);
				}
			}

			return system.getState(); // Continue
		});

		// Setup Backend
		this.backend = new CommandDrivenCLI<>(constantsMap, null, null, commands,
				Boolean.TRUE, true, null, null, null,
				null, null, null, null, prompter, messenger);

		// Store Parameters to Fields
		this.messenger = messenger;
		this.introduction = introduction;
		this.conclusion = conclusion;
	}

	/**
	 * Starts the {@link TextTransformerConsoleAppContainer} in another {@link Thread}.
	 */
	@Override
	protected final void doLaunch() {
		// Run in different thread, because launching should not block the calling thread.
		new Thread(this::doRun).start();
	}

	/**
	 * Actually runs the {@link TextTransformerConsoleAppContainer}.
	 */
	private final void doRun() {
		this.messenger.print(this.introduction);

		this.backend.run();
		this.backend = null;

		this.messenger.print(this.conclusion);
	}
}
