package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import java.awt.Color;
import java.awt.Component;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GUIJAdvancedTextArea;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

/**
 * A Swing app container for simple text transformer apps.
 *
 * @author Daniel Tang
 * @since 16 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class TextTransformerGraphicalAppContainer extends AppContainer {

	/**
	 * The actual text transformer.
	 */
	private final Function<String, String> textTransformer;

	/**
	 * The title of the window.
	 */
	private final String title;

	/**
	 * The empty output box text.
	 */
	private final String emptyOutputBoxText;

	/**
	 * The input box.
	 */
	private final GUIJAdvancedTextArea inputBox;

	/**
	 * The {@link JScrollPane} wrapping the input box.
	 */
	private final JScrollPane scrollInputBox;

	/**
	 * The growing {@link JPanel} wrapping the {@link JScrollPane} wrapping the input box.
	 */
	private final JPanel growScrollInputBox;

	/**
	 * The output box.
	 */
	private final GUIJAdvancedTextArea outputBox;

	/**
	 * The {@link JScrollPane} wrapping the output box.
	 */
	private final JScrollPane scrollOutputBox;

	/**
	 * The growing {@link JPanel} wrapping the {@link JScrollPane} wrapping the output box.
	 */
	private final JPanel growScrollOutputBox;

	/**
	 * The {@link JSplitPane} splitting the wrapped input and output boxes.
	 */
	private final JSplitPane splitViewInputOutputBoxes;

	/**
	 * The growing {@link JPanel} wrapping the {@link JSplitPane} splitting the wrapped input and output boxes.
	 */
	private final JPanel growSplitViewInputOutputBoxes;

	/**
	 * The bottom bar area
	 */
	private final JPanel bottomPanel;

	/**
	 * The manual translate button
	 */
	private final JButton updateButton;

	/**
	 * The auto update toggle.
	 */
	private final JCheckBox autoUpdateToggle;

	/**
	 * The credits.
	 */
	private final JLabel creditsLabel;

	/**
	 * Whether the text translation output should display in realtime.
	 */
	private volatile boolean realtime;

	/**
	 * The custom setup callback for finishing touches on the {@link JFrame}.
	 */
	private /*final*/ Consumer<JFrame> prelaunchJFrameCustomSetup;

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	/**
	 * Creates a new simple text transformer Swing app container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param textTransformer The text transformer {@link Function}.
	 * @param title The title of the app.
	 * @param credits The credits at the bottom, may be {@code null} to disable.
	 */
	public TextTransformerGraphicalAppContainer(final Map<?, ?> constantsMap,
			final Function<String, String> textTransformer,
			final String title, final String credits) {
		this(constantsMap, textTransformer, title, credits,
				PlatformUtils.getHighPerformance(), null);
	}

	/**
	 * Creates a new lightly-customized text transformer Swing app container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param textTransformer The text transformer {@link Function}.
	 * @param title The title of the app.
	 * @param credits The credits at the bottom, may be {@code null} to disable.
	 * @param initialAutoUpdate Whether the app should auto-update on input changes in realtime.
	 */
	public TextTransformerGraphicalAppContainer(final Map<?, ?> constantsMap,
			final Function<String, String> textTransformer,
			final String title, final String credits, final boolean initialAutoUpdate) {
		this(constantsMap, textTransformer, title, credits,
				initialAutoUpdate, null);
	}

	/**
	 * Creates a new lightly-customized text transformer Swing app container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param textTransformer The text transformer {@link Function}.
	 * @param title The title of the app.
	 * @param credits The credits at the bottom, may be {@code null} to disable.
	 * @param prelaunchJFrameCustomSetup The custom setup callback for finishing touches on the {@link JFrame},
	 *                                   may be {@code null} to disable.
	 */
	public TextTransformerGraphicalAppContainer(final Map<?, ?> constantsMap,
			final Function<String, String> textTransformer,
			final String title, final String credits,
			final Consumer<JFrame> prelaunchJFrameCustomSetup) {
		this(constantsMap, textTransformer, title, credits,
				PlatformUtils.getHighPerformance(), prelaunchJFrameCustomSetup);
	}

	/**
	 * Creates a new customized text transformer Swing app container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param textTransformer The text transformer {@link Function}.
	 * @param title The title of the app.
	 * @param credits The credits at the bottom, may be {@code null} to disable.
	 * @param initialAutoUpdate Whether the app should auto-update on input changes in realtime.
	 * @param prelaunchJFrameCustomSetup The custom setup callback for finishing touches on the {@link JFrame},
	 *                                   may be {@code null} to disable
	 */
	public TextTransformerGraphicalAppContainer(final Map<?, ?> constantsMap,
			final Function<String, String> textTransformer,
			final String title, final String credits,
			final boolean initialAutoUpdate, final Consumer<JFrame> prelaunchJFrameCustomSetup) {
		Validate.notNull(constantsMap);
		Validate.notNull(textTransformer);
		Validate.notNull(title);

		// Setup consistent widgets
		final Widgets widgets = new Widgets(constantsMap, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		// Text Boxes
		{
			// Input Box
			{
				// Create Box
				this.inputBox = new GUIJAdvancedTextArea(Coerce.asString(constantsMap.get(
								"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.initialInputBoxText")));
				this.scrollInputBox = SwingUtils.wrapWithScrolling(this.inputBox);
				this.growScrollInputBox = SwingUtils.wrapInGrowingJPanel(this.scrollInputBox);

				// Input Box Settings
				this.inputBox.setLineWrap(true);
				this.inputBox.setBackground(new Color(Coerce.asInt(constantsMap.get(
								"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.inputBoxColor"))));
			}

			// Link Input to Output
			SwingUtils.hookDocumentGenericUpdate(this.inputBox.getDocument(), (ignore2) -> {
				synchronized (this.getAutoUpdateToggle()) {
					if (this.realtime) {
						this.updateUI();
					}
				}
			});

			// Output Box
			{
				// Load String
				this.emptyOutputBoxText = Coerce.asString(constantsMap.get(
								"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.emptyOutputBoxText"));

				// Create Box
				this.outputBox = new GUIJAdvancedTextArea(this.emptyOutputBoxText);
				this.scrollOutputBox = SwingUtils.wrapWithScrolling(this.outputBox);
				this.growScrollOutputBox = SwingUtils.wrapInGrowingJPanel(this.scrollOutputBox);

				// Output Box Settings
				this.outputBox.setLineWrap(true);
				this.outputBox.setEditable(false); // Output should be always linked to input
				this.outputBox.setBackground(new Color(Coerce.asInt(constantsMap.get(
								"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.outputBoxColor"))));
			}

			// Input & Output Boxes Resize into each other
			this.splitViewInputOutputBoxes = SwingUtils.wrapDivideContentsEven(
					false, this.growScrollInputBox, this.growScrollOutputBox);
			this.growSplitViewInputOutputBoxes = SwingUtils.wrapInGrowingJPanel(this.splitViewInputOutputBoxes);
		}

		// Bottom Bar
		{
			// Create Bottom Bar
			this.bottomPanel = new JPanel();
			this.bottomPanel.setLayout(new BoxLayout(this.bottomPanel, BoxLayout.X_AXIS));

			// Update Button
			this.updateButton = new JButton(Coerce.asString(constantsMap.get(
							"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.translateButtonText")));
			SwingUtils.hookButtonPress(this.updateButton, this::updateUI);

			// Credits Label
			this.creditsLabel = new JLabel();
			if (credits != null) {
				this.creditsLabel.setText(credits);
				this.bottomPanel.add(widgets.createSimpleSpacer(false)); // Spacer
			}

			// Realtime Checkbox
			this.autoUpdateToggle = new JCheckBox(Coerce.asString(constantsMap.get(
							"me.danielzgtg.compsci11_sem2_2017.common.texttransformergui.realtimeToggleText")),
					initialAutoUpdate);
			SwingUtils.hookCheckboxToggle(this.autoUpdateToggle, (selected) -> {
				this.realtime = selected;

				if (selected) {
					this.updateUI();
				}
			});

			// Add Bottom Bar Contents
			this.bottomPanel.add(this.updateButton);
			this.bottomPanel.add(this.creditsLabel);
			this.bottomPanel.add(this.autoUpdateToggle);
		}

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowHeight")));

		// Copy Parameters to Fields
		this.realtime = initialAutoUpdate;
		this.prelaunchJFrameCustomSetup = prelaunchJFrameCustomSetup;
		this.textTransformer = textTransformer;
		this.title = title;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doLaunch() {
		final JFrame frame = SwingUtils.generateOnetimeJFrame(this.title);

		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.growSplitViewInputOutputBoxes,
				this.bottomPanel
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth, this.initialWindowHeight);

		if (this.prelaunchJFrameCustomSetup != null) {
			this.prelaunchJFrameCustomSetup.accept(frame);
			this.prelaunchJFrameCustomSetup = null; // Free
		}

		SwingUtils.launchJFrame(frame);
	}

	/**
	 * Updates the content areas.
	 */
	private final void updateUI() {
		final String rawText = this.inputBox.getText();
		if (rawText == null) {
			this.outputBox.setText(":( [1]");
			return;
		}

		final String result;

		if ("".equals(rawText)) {
			result = this.emptyOutputBoxText;
		} else {
			try {
				result = this.textTransformer.apply(rawText);
			} catch (final Exception e) {
				this.outputBox.setText(":( [2]");
				return;
			}
		}

		this.outputBox.setText(result);
	}

	/**
	 * Gets the {@link GUIJAdvancedTextArea} for the input box.
	 *
	 * @return The {@link GUIJAdvancedTextArea} for the input box.
	 */
	public final GUIJAdvancedTextArea getInputBox() {
		return this.inputBox;
	}

	/**
	 * Gets the {@link JScrollPane} wrapping the input box.
	 *
	 * @return The {@link JScrollPane} wrapping the input box.
	 */
	public final JScrollPane getScrollInputBox() {
		return this.scrollInputBox;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the scroll pane wrapping the input box.
	 *
	 * @return The growing {@link JPanel} wrapping the scroll pane wrapping the input box.
	 */
	public final JPanel getGrowScrollInputBox() {
		return this.growScrollInputBox;
	}

	/**
	 * Gets the {@link GUIJAdvancedTextArea} for the output box.
	 *
	 * @return The {@link GUIJAdvancedTextArea} for the output box.
	 */
	public final GUIJAdvancedTextArea getOutputBox() {
		return this.outputBox;
	}

	/**
	 * Gets the {@link JScrollPane} wrapping the output box.
	 *
	 * @return The {@link JScrollPane} wrapping the output box.
	 */
	public final JScrollPane getScrollOutputBox() {
		return this.scrollOutputBox;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the scroll pane wrapping the output box.
	 *
	 * @return The growing {@link JPanel} wrapping the scroll pane wrapping the output box.
	 */
	public final JPanel getGrowScrollOutputBox() {
		return this.growScrollOutputBox;
	}

	/**
	 * Gets the {@link JSplitPane} splitting the wrapped input and output boxes.
	 *
	 * @return The {@link JSplitPane} splitting the wrapped input and output boxes.
	 */
	public final JSplitPane getSplitViewInputOutputBoxes() {
		return this.splitViewInputOutputBoxes;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping
	 * the {@link JSplitPane} splitting the wrapped input and output boxes.
	 *
	 * @return The growing {@link JPanel} wrapping
	 * the {@link JSplitPane} splitting the wrapped input and output boxes.
	 */
	public final JPanel getGrowSplitViewInputOutputBoxes() {
		return this.growSplitViewInputOutputBoxes;
	}

	/**
	 * Gets the {@link JPanel} for the bottom bar.
	 *
	 * @return The {@link JPanel} for the bottom bar.
	 */
	public final JPanel getBottomPanel() {
		return this.bottomPanel;
	}

	/**
	 * Gets the {@code JButton} for manual update.
	 *
	 * @return The {@code JButton} for manual update.
	 */
	public final JButton getUpdateButton() {
		return this.updateButton;
	}

	/**
	 * Gets the {@code JCheckBox} for the auto-update toggle.
	 *
	 * @return The {@code JCheckBox} for the auto-update toggle.
	 */
	public final JCheckBox getAutoUpdateToggle() {
		return this.autoUpdateToggle;
	}

	/**
	 * Gets the {@link JLabel} for the credits.
	 *
	 * @return The {@link JLabel} for the credits.
	 */
	public final JLabel getCreditsLabel() {
		return this.creditsLabel;
	}

	/**
	 * Gets whether the output will live-update.
	 *
	 * @return {@code true} if the output will live-update
	 */
	public final boolean isRealtime() {
		return this.realtime;
	}

	/**
	 * Sets whether the output live-updates.
	 *
	 * @param realtime Whether to live-update.
	 */
	public final void setRealtime(final boolean realtime) {
		synchronized (this.autoUpdateToggle) {
			this.autoUpdateToggle.setSelected(realtime);
			this.realtime = realtime;
		}

		if (realtime) {
			this.updateUI();
		}
	}
}
