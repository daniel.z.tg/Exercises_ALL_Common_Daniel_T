package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.function.Consumer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import java.awt.Component;
import java.lang.reflect.Constructor;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDB;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Attribute;
import me.danielzgtg.compsci11_sem2_2017.common.ui.HumanDataManipulator;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

/**
 * A Swing app container for a database of rows with attributes.
 *
 * @author Daniel Tang
 * @since 30 April 2017
 *
 * @param <T> The type of the type of database row contents.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class AttributeDBGraphicalAppContainer<T> extends AppContainer {

	// Core data
	/**
	 * The backend.
	 */
	public final AttributeDBUICore<T> core;

	// Temp Data
	/**
	 * The custom setup callback for finishing touches on the {@link JFrame}.
	 */
	private /*final*/ Consumer<JFrame> prelaunchJFrameCustomSetup;

	/**
	 * The title of the window. (used to setup the window)
	 */
	private /*final*/ String tmpInitialTitle;

	// GUI backing data
	/**
	 * The table data of the selection table.
	 */
	private final DefaultTableModel selectionTableData;

	/**
	 * The manipulators for interpreting the database data to and from the user.
	 */
	private final Map<Attribute<T, Object>, HumanDataManipulator<Object>> humanDataManipulatorMap = new HashMap<>();

	/**
	 * The user's currently specified modifiers.
	 */
	private final Map<Attribute<T, Object>, String> attributeModifiers = new HashMap<>();

	/**
	 * The {@link String} name modifier for the type {@link Class}. {@code null} means to ignore this modifier.
	 */
	private String typeNameModifier = null;

	/**
	 * {@code true} when the {@link AttributeDBGraphicalAppContainer#typeNameModifier}
	 * matches only the specified {@link Class}, else subclasses as well.
	 */
	private boolean exactType = false;

	// GUI Objects
	// Selection section
	/**
	 * The {@link JPanel} wrapping the selection section.
	 */
	private final JPanel selectionSection;

	// Selection table, and wrappers
	/**
	 * The {@link JTable} of selected database rows.
	 */
	private final JTable selectionTable;

	/**
	 * The {@link JScrollPane} wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 */
	private final JScrollPane scrollSelectionTable;

	/**
	 * The growing {@link JPanel} wrapping the {@link JScrollPane}
	 * wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 */
	private final JPanel growScrollSelectionTable;

	// Selection action bar and buttons
	/**
	 * The {@link JPanel} toolbar of {@link JButton}s that do not depend on the modifier list.
	 */
	private final JPanel selectionActionBar;

	/**
	 * The {@link JButton} for adding a new {@link MemoryDBRow}.
	 */
	private final JButton addNewButton;

	/**
	 * The {@link JButton} for replacing the selection with all of the {@link MemoryDBRow}s in the database.
	 */
	private final JButton selectAllButton;

	/**
	 * The {@link JButton} for displaying info on the all of the selected {@link MemoryDBRow}s.
	 */
	private final JButton infoButton;

	/**
	 * The {@link JButton} for removing all of the selected {@link MemoryDBRow}s.
	 */
	private final JButton removeButton;

	// Modifier section
	/**
	 * The {@link JPanel} wrapping the modifier section.
	 */
	private final JPanel modifierSection;

	// Modifier list, and wrappers
	/**
	 * The {@link JPanel} of the list of modifiers.
	 */
	private final JPanel modifierPanel;

	/**
	 * The {@link JScrollPane} wrapping the {@link JPanel} of the list of modifiers.
	 */
	private final JScrollPane scrollModifierPanel;

	/**
	 * The growing {@link JPanel} wrapping the {@link JScrollPane} wrapping the {@link JPanel} of the list of modifiers.
	 */
	private final JPanel growScrollModifierPanel;

	// Modifier action bar and buttons
	/**
	 * The {@link JPanel} toolbar of {@link JButton}s that depend on the modifier list, plus a credits {@link JLabel}.
	 */
	private final JPanel modifierActionBar;

	/**
	 * The {@link JLabel} for the credits.
	 */
	private final JLabel creditsLabel;

	/**
	 * The {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 * to only the specified modifier list particulars.
	 */
	private final JButton filterOnlyButton;

	/**
	 * The {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 * to exclude the specified modifier list particulars.
	 */
	private final JButton filterOutButton;

	/**
	 * The {@link JButton} to try to apply the modifier list particulars so that
	 * the selection of {@link MemoryDBRow}s conforms to it.
	 */
	private final JButton applyButton;

	// Wrappers of the two sections
	/**
	 * The {@link JSplitPane} splitting the selection and modifier sections.
	 */
	private final JSplitPane splitViewSectionsContainer;

	/**
	 * The growing {@link JPanel} wrapping the {@link JSplitPane} splitting the selection and modifier sections.
	 */
	private final JPanel growSplitViewSectionsContainer;

	/**
	 * The consistent widget tool.
	 */
	private final Widgets widgets;

	/**
	 * The error message for when the user's requested type could not be resolved.
	 */
	private final String unresolvedTypeFilterErrMsg;

	/**
	 * The title for the error box, when an {@link Attribute} could not be set.
	 */
	private final String attributeSetErrTitle;

	/**
	 * The layout for the error box contents, when an {@link Attribute} could not be set.
	 */
	private final String attributeSetErrLayout;

	/**
	 * The title for the info box showing selection contents.
	 */
	private final String selectionInfoTitle;

	/**
	 * The title for the error box, when an {@link Attribute} could not be parsed.
	 */
	private final String attributeFilterParseErrTitle;

	/**
	 * The layout for the error box contents, when an {@link Attribute} could not be parsed.
	 */
	private final String attributeFilterParseErrLayout;

	/**
	 * The prompt for adding a {@link MemoryDBRow}.
	 */
	private final String addRowPrompt;

	/**
	 * The error message for when a {@link MemoryDBRow} could not be added.
	 */
	private final String addRowErrMsg;

	/**
	 * The placeholder if an {@link Attribute} is not applicable to a {@link MemoryDBRow}'s contents.
	 */
	private final String attributeNotApplicableMsg;

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	/**
	 * Creates a new {@link AttributeDBGraphicalAppContainer}.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param title The title of the app.
	 * @param prelaunchJFrameCustomSetup The custom setup callback for finishing touches on the {@link JFrame},
	 *                                   may be {@code null} to disable
	 * @param attributePairsRaw The list of attributes and data interpreters, not null.
	 * @param db The backing database, not null.
	 * @param credits The credits at the bottom, may be {@code null} to disable.
	 * @param type The type of database row data, should be the same as {@link T}.
	 */
	public AttributeDBGraphicalAppContainer(
			final Map<?, ?> constantsMap,
			final String title,
			final Consumer<JFrame> prelaunchJFrameCustomSetup,
			final List<Pair<Attribute<T, Object>, HumanDataManipulator<Object>>> attributePairsRaw,
			final MemoryDB<T> db, final String credits,
			final Class<T> type) {
		// Simple null checks
		Validate.notNull(title);
		Validate.notNull(db);
		Validate.notNull(constantsMap);

		// Clone attribute pairs list
		final List<Pair<Attribute<T, Object>, HumanDataManipulator<Object>>> attributePairs =
				Collections.unmodifiableList(new LinkedList<>(attributePairsRaw));

		// Unpack attributes, and validate attribute pairs
		final List<Attribute<T, Object>> attributeList = new LinkedList<>();
		for (final Pair<Attribute<T, Object>, HumanDataManipulator<Object>> pair : attributePairs) {
			Validate.notNull(pair);

			final Attribute<T, Object> attribute = pair.getLeft();
			final HumanDataManipulator<Object> humanDataManipulator = pair.getRight();

			Validate.notNull(attribute);
			Validate.notNull(humanDataManipulator);

			attributeList.add(attribute);
			this.humanDataManipulatorMap.put(attribute, humanDataManipulator);
		}

		// Initialize backing
		this.core = AttributeDBUICore.of(type, attributeList, db);
		this.selectionTableData = new DefaultTableModel();

		// Load Shared Strings
		final String classTypeDescriptor = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.classTypeDescriptor"));

		// Add columns
		this.selectionTableData.addColumn(classTypeDescriptor); // Type is special first column
		for (final Attribute<T, Object> attribute : attributeList) { // Rest of columns are attributes
			this.selectionTableData.addColumn(attribute.getName());
		}

		// Setup consistent widgets
		this.widgets = new Widgets(constantsMap, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		// Selection Section
		{
			// Create & Setup selection table
			{
				this.selectionTable = new JTable(this.selectionTableData);
				this.scrollSelectionTable = SwingUtils.wrapWithScrolling(this.selectionTable);
				this.growScrollSelectionTable = SwingUtils.wrapInGrowingJPanel(this.scrollSelectionTable);

				SwingUtils.disableTableEditing(this.selectionTable);
				SwingUtils.setTitle(this.growScrollSelectionTable, Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.selectionTableHeader")));
			}

			// Selection action bar
			{
				// Create selection action bar buttons
				this.addNewButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.addRowButtonLabel")));
				this.selectAllButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.selectAllRowsButtonLabel")));
				this.infoButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.rowInfoButtonLabel")));
				this.removeButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.removeRowButtonLabel")));

				// Link selection action bar buttons
				SwingUtils.hookButtonPress(this.addNewButton, this::promptAddRow);

				SwingUtils.hookButtonPress(this.selectAllButton, () -> {
					this.core.fillSelection();
					this.updateSelectionDisplay();
				});

				SwingUtils.hookButtonPress(this.removeButton, () -> {
					for (final MemoryDBRow<T> row : this.core.selection) {
						row.remove();
					}

					this.core.fillSelection();
					this.updateSelectionDisplay();
				});

				SwingUtils.hookButtonPress(this.infoButton, this::showSelectionInfo);

				// Create & Setup selection action bar
				this.selectionActionBar = new JPanel();
				this.selectionActionBar.setLayout(new BoxLayout(this.selectionActionBar, BoxLayout.X_AXIS));

				// Add buttons to selection action bar
				this.selectionActionBar.add(this.addNewButton);
				this.selectionActionBar.add(this.selectAllButton);
				this.selectionActionBar.add(this.infoButton);
				this.selectionActionBar.add(this.removeButton);
			}

			// Create & Setup selection section panel
			this.selectionSection = new JPanel();
			this.selectionSection.setLayout(new BoxLayout(this.selectionSection, BoxLayout.Y_AXIS));

			// Add selection table and selection action bar
			this.selectionSection.add(this.growScrollSelectionTable);
			this.selectionSection.add(this.selectionActionBar);
		}

		// Modifier Section
		{
			// Modifier list panel
			{
				// Setup modifier list panel
				this.modifierPanel = new JPanel();
				this.modifierPanel.setLayout(new BoxLayout(this.modifierPanel, BoxLayout.Y_AXIS));
				this.scrollModifierPanel = SwingUtils.wrapWithScrolling(this.modifierPanel);
				this.growScrollModifierPanel = SwingUtils.wrapInGrowingJPanel(this.scrollModifierPanel);
				SwingUtils.setTitle(this.growScrollModifierPanel, Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.modifierListHeader")));

				// Load Shared Modifier Strings
				final String modifierEnableLabel = Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.modifierEnableLabel"));
				final String modifierInitialText = Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.modifierInitialText"));

				// Add list of modifiers to panel
				this.setupTypeModifier(
						classTypeDescriptor,
						modifierEnableLabel,
						Coerce.asString(constantsMap.get(
								"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.exactClassLabel")),
						modifierInitialText);
				for (final Pair<Attribute<T, Object>, HumanDataManipulator<Object>> pair : attributePairs) {
					this.setupAttributeModifiers(pair, modifierEnableLabel, modifierInitialText);
				}
			}

			// Modifier action bar
			{
				// Create modifier action bar buttons
				this.filterOnlyButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.filterOnlyRowsButtonLabel")));
				this.filterOutButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.filterOutRowsButtonLabel")));
				this.applyButton = new JButton(Coerce.asString(constantsMap.get(
						"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.applyToDataButtonLabel")));

				// Link modifier action bar buttons
				SwingUtils.hookButtonPress(this.filterOnlyButton, () -> this.applyFilters(false));
				SwingUtils.hookButtonPress(this.filterOutButton, () -> this.applyFilters(true));
				SwingUtils.hookButtonPress(this.applyButton, this::applyFiltersAsAttributes);


				// Create & Setup modifier action bar
				this.modifierActionBar = new JPanel();
				this.modifierActionBar.setLayout(new BoxLayout(this.modifierActionBar, BoxLayout.X_AXIS));


				// Create, Setup & Add credits label
				this.creditsLabel = new JLabel();
				this.modifierActionBar.add(this.creditsLabel);
				if (credits != null) {
					this.creditsLabel.setText(credits);
					this.modifierActionBar.add(this.widgets.createSimpleSpacer(false));
				}

				// Add buttons to modifier action bar
				this.modifierActionBar.add(this.filterOnlyButton);
				this.modifierActionBar.add(this.filterOutButton);
				this.modifierActionBar.add(this.applyButton);
			}

			// Create & Setup modifier section panel
			this.modifierSection = new JPanel();
			this.modifierSection.setLayout(new BoxLayout(this.modifierSection, BoxLayout.Y_AXIS));

			// Add modifier list panel and modifier action bar
			this.modifierSection.add(this.growScrollModifierPanel);
			this.modifierSection.add(this.modifierActionBar);
		}

		// Sections Resize into each other
		{
			this.splitViewSectionsContainer = SwingUtils.wrapDivideContentsEven(
					false, this.selectionSection, this.modifierSection);
			this.growSplitViewSectionsContainer = SwingUtils.wrapInGrowingJPanel(this.splitViewSectionsContainer);
		}

		// Load Strings
		this.unresolvedTypeFilterErrMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.unresolvedTypeFilterErrMsg"));
		this.attributeSetErrTitle = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.attributeSetErrTitle"));
		this.attributeSetErrLayout = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.attributeSetErrLayout"));
		this.selectionInfoTitle = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.selectionInfoTitle"));
		this.attributeFilterParseErrTitle = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.attributeFilterParseErrTitle"));
		this.attributeFilterParseErrLayout = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.attributeFilterParseErrLayout"));
		this.addRowPrompt = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.addRowPrompt"));
		this.addRowErrMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.addRowErrMsg"));
		this.attributeNotApplicableMsg = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.attributedbgui.attributeNotApplicableMsg"));

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowHeight")));

		// Initial Refresh
		this.core.fillSelection();
		this.updateSelectionDisplay();

		// Simple passthrough init
		this.tmpInitialTitle = title;
		this.prelaunchJFrameCustomSetup = prelaunchJFrameCustomSetup;
	}

	/**
	 * Applies the attributes in the filters to the selection objects.
	 */
	public void applyFiltersAsAttributes() {
		final Map<Attribute<T, Object>, Object> newAttributeData = this.parseAttributeFilters();

		for (final MemoryDBRow<T> row : this.core.selection) {
			final T target = row.getData();

			for (final Map.Entry<Attribute<T, Object>, Object> attributeAndNewData : newAttributeData.entrySet()) {
				final Attribute<T, Object> attribute = attributeAndNewData.getKey();

				try {
					attribute.set(row.getData(), attributeAndNewData.getValue());
				} catch (final Exception e) {
					this.widgets.simplyShowInfoBox(this.attributeSetErrTitle,
							String.format(this.attributeSetErrLayout, attribute.getName(), target),
							this.applyButton);
				}
			}
		}

		this.updateSelectionDisplay();
	}

	/**
	 * Shows the info for the {@link Object#toString()} of {@link MemoryDBRow#getData()} for all of the selection rows.
	 */
	public void showSelectionInfo() {
		for (final MemoryDBRow row : this.core.selection) {
			this.widgets.simplyShowInfoBox(this.selectionInfoTitle, String.valueOf(row.getData()), this.selectionTable);
		}
	}

	/**
	 * Shows a prompt for adding a row.
	 */
	public void promptAddRow() {
		final String input = JOptionPane.showInputDialog(this.addRowPrompt);

		if (input == null) return;

		try {
			final Class<T> clazz;

			{
				final Class<?> tryClazz = Class.forName(input);

				if (!tryClazz.isAssignableFrom(this.core.type)) {
					throw new ClassCastException();
				}

				//noinspection unchecked
				clazz = (Class<T>) tryClazz;
			}

			final Constructor<T> ctor = clazz.getConstructor();
			final T result = ctor.newInstance();

			if (!this.core.type.isInstance(result)) {
				throw new ClassCastException();
			}

			this.core.database.add(result);
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(this.addNewButton, this.addRowErrMsg);
		}

		this.core.fillSelection();
		this.updateSelectionDisplay();
	}

	/**
	 * Sets up the modifier controls for type.
	 *
	 * @param classTypeDescriptor The name describing class type.
	 * @param modifierEnableLabel The label on the toggle for enabling a modifier.
	 * @param exactClassLabel The label on the toggle for having exact class type.
	 * @param modifierInitialText The initial modifier particulars text.
	 */
	private void setupTypeModifier(
			final String classTypeDescriptor,
			final String modifierEnableLabel,
			final String exactClassLabel,
			final String modifierInitialText) {
		final JPanel modifierTypePanel = new JPanel();
		SwingUtils.setTitle(modifierTypePanel, classTypeDescriptor);
		modifierTypePanel.setLayout(new BoxLayout(modifierTypePanel, BoxLayout.Y_AXIS));

		final JCheckBox modifierCheckBox = new JCheckBox(modifierEnableLabel);
		final JCheckBox exactCheckBox = new JCheckBox(exactClassLabel);
		final JTextArea modifierTextArea = new JTextArea(modifierInitialText);
		modifierTextArea.setLineWrap(true);

		SwingUtils.hookCheckboxToggle(modifierCheckBox, (enabled) -> {
					if (enabled) {
						this.typeNameModifier = modifierTextArea.getText();
					} else {
						this.typeNameModifier = null;
					}
				}
		);

		SwingUtils.hookCheckboxToggle(exactCheckBox, (enabled) -> this.exactType = enabled
		);

		SwingUtils.hookDocumentGenericUpdate(modifierTextArea.getDocument(), (event) -> {
			if (modifierCheckBox.isSelected()) {
				this.typeNameModifier = modifierTextArea.getText();
			}
		});

		modifierTypePanel.add(SwingUtils.wrapInGrowingJPanel(modifierCheckBox));
		modifierTypePanel.add(SwingUtils.wrapInGrowingJPanel(exactCheckBox));
		modifierTypePanel.add(SwingUtils.wrapInGrowingJPanel(modifierTextArea));

		this.modifierPanel.add(modifierTypePanel);
	}

	/**
	 * Sets up the modifier controls for an {@link Attribute}, with its {@link HumanDataManipulator}.
	 *
	 * @param pair A {@link Pair} of an {@link Attribute} to manipulate and {@link HumanDataManipulator} to interpret.
	 * @param modifierEnableLabel The label on the toggle for enabling a modifier.
	 * @param modifierInitialText The initial modifier particulars text.
	 */
	private void setupAttributeModifiers(
			final Pair<Attribute<T, Object>, HumanDataManipulator<Object>> pair,
			final String modifierEnableLabel,
			final String modifierInitialText) {
		final Attribute<T, Object> attr = pair.getLeft();

		final JPanel modifierAttributePanel = new JPanel();
		SwingUtils.setTitle(modifierAttributePanel, attr.getName());
		modifierAttributePanel.setLayout(new BoxLayout(modifierAttributePanel, BoxLayout.Y_AXIS));

		final JCheckBox modifierCheckBox = new JCheckBox(modifierEnableLabel);
		final JTextArea modifierTextArea = new JTextArea(modifierInitialText);
		modifierTextArea.setLineWrap(true);

		SwingUtils.hookCheckboxToggle(modifierCheckBox, (enabled) -> {
					if (enabled) {
						this.attributeModifiers.put(attr, modifierTextArea.getText());
					} else {
						this.attributeModifiers.remove(attr);
					}
				}
		);

		SwingUtils.hookDocumentGenericUpdate(modifierTextArea.getDocument(), (event) -> {
			if (this.attributeModifiers.containsKey(attr)) {
				this.attributeModifiers.put(attr, modifierTextArea.getText());
			}
		});

		modifierAttributePanel.add(SwingUtils.wrapInGrowingJPanel(modifierCheckBox));
		modifierAttributePanel.add(SwingUtils.wrapInGrowingJPanel(modifierTextArea));

		this.modifierPanel.add(modifierAttributePanel);
	}

	/**
	 * Tries to apply the user's filters to limit the selection.
	 *
	 * @param exclude {@code true} if the rows that match the filters should be deselected,
	 *                else do so for the rows that don't match.
	 */
	public final void applyFilters(final boolean exclude) {
		this.core.applySimpleFilter(exclude, this.parseTypeFilter(), this.exactType, this.parseAttributeFilters());

		this.updateSelectionDisplay();
	}

	/**
	 * Parses the user's attribute data filters, that are specified and valid, into machine {@link Object} form.
	 *
	 * @return A {@link Map} of the specified {@link Attribute}s, and their {@link Object} filter particulars.
	 */
	private final Map<Attribute<T, Object>, Object> parseAttributeFilters() {
		final Map<Attribute<T, Object>, Object> result = new HashMap<>();

		for (final Map.Entry<Attribute<T, Object>, String> entry : this.attributeModifiers.entrySet()) {
			final Attribute<T, Object> attribute = entry.getKey();

			try {
				result.put(attribute, this.humanDataManipulatorMap.get(attribute)
						.toMachineFormat(entry.getValue()));
			} catch (final Exception e) {
				this.widgets.simplyShowInfoBox(this.attributeFilterParseErrTitle,
						String.format(this.attributeFilterParseErrLayout, attribute.getName()),
						this.modifierPanel);
			}
		}

		return Collections.unmodifiableMap(result);
	}

	/**
	 * Finds the {@link Class} that the user wants to filter to,
	 * if it is existent, specified and valid, else {@code null}.
	 *
	 * @return The {@link Class} of the specified type filter,
	 *         or {@code null} if it was invalid, unspecified, or not found.
	 */
	@SuppressWarnings("unchecked")
	private final Class<? extends T> parseTypeFilter() {
		if (this.typeNameModifier != null) {
			try {
				final Class<?> potentialResult = Class.forName(this.typeNameModifier);

				if (potentialResult != null && this.core.type.isAssignableFrom(potentialResult)) {
					return (Class<? extends T>) potentialResult;
				}
			} catch (final Exception e) {
				JOptionPane.showMessageDialog(this.modifierPanel, this.unresolvedTypeFilterErrMsg);
			}
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doLaunch() {
		final JFrame frame = SwingUtils.generateOnetimeJFrame(this.tmpInitialTitle);
		this.tmpInitialTitle = null;

		this.updateSelectionDisplay();
		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.growSplitViewSectionsContainer
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth, this.initialWindowHeight);

		if (this.prelaunchJFrameCustomSetup != null) {
			this.prelaunchJFrameCustomSetup.accept(frame);
			this.prelaunchJFrameCustomSetup = null; // Free
		}

		SwingUtils.launchJFrame(frame);
	}

	/**
	 * Updates the data table, according to the database.
	 */
	public void updateSelectionDisplay() {
		/*
		 * This method updates the table data by:
		 *
		 * 1. Clearing the table.
		 * 2. Adding the rows back.
		 */
		this.selectionTableData.setRowCount(0);

		for (final MemoryDBRow<T> row : this.core.selection) {
			final T data = row.getData();
			final Vector<String> v = new Vector<>();

			v.add(data.getClass().getName()); // Type is special first column

			for (final Attribute<T, Object> attribute : this.core.attributes) { // Rest of columns are attributes
				try {
					v.add(this.humanDataManipulatorMap.get(attribute).toHumanFormat(attribute.get(data)));
				} catch (final ReflectiveOperationException e) {
					v.add(this.attributeNotApplicableMsg);
				}
			}

			this.selectionTableData.addRow(v);
		}
	}

	/**
	 * Gets the backend.
	 *
	 * @return The backend.
	 */
	public final AttributeDBUICore<T> getCore() {
		return this.core;
	}

	/**
	 * Gets the manipulators for interpreting the database data to and from the user.
	 *
	 * @return The manipulators for interpreting the database data to and from the user.
	 */
	public final Map<Attribute<T, Object>, HumanDataManipulator<Object>> getHumanDataManipulatorMap() {
		return this.humanDataManipulatorMap;
	}

	/**
	 * Gets the user's currently specified modifiers.
	 *
	 * @return The user's currently specified modifiers.
	 */
	public final Map<Attribute<T, Object>, String> getAttributeModifiers() {
		return this.attributeModifiers;
	}

	/**
	 * Gets the {@link String} name modifier for the type {@link Class}. {@code null} means to ignore this modifier
	 *
	 * @return The {@link String} name modifier for the type {@link Class}. {@code null} means to ignore this modifier
	 */
	public final String getTypeNameModifier() {
		return this.typeNameModifier;
	}

	/**
	 * Determines whether the type name modifier does not matches subclasses.
	 *
	 * @return {@code true} when the {@link AttributeDBGraphicalAppContainer#typeNameModifier}
	 *         matches only the specified {@link Class}, else subclasses as well.
	 */
	public final boolean isExactType() {
		return this.exactType;
	}

	/**
	 * Gets the {@link JPanel} wrapping the selection section.
	 *
	 * @return The {@link JPanel} wrapping the selection section.
	 */
	public final JPanel getSelectionSection() {
		return this.selectionSection;
	}

	/**
	 * Gets the {@link JTable} of selected database rows.
	 *
	 * @return The {@link JTable} of selected database rows.
	 */
	public final JTable getSelectionTable() {
		return this.selectionTable;
	}

	/**
	 * Gets the {@link JScrollPane} wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 *
	 * @return The {@link JScrollPane} wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 */
	public final JScrollPane getScrollSelectionTable() {
		return this.scrollSelectionTable;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the {@link JScrollPane}
	 * wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 *
	 * @return The growing {@link JPanel} wrapping the {@link JScrollPane}
	 *         wrapping the {@link JTable} of selected {@link MemoryDBRow}s.
	 */
	public final JPanel getGrowScrollSelectionTable() {
		return this.growScrollSelectionTable;
	}

	/**
	 * Gets the {@link JPanel} toolbar of {@link JButton}s that do not depend on the modifier list.
	 * @return The {@link JPanel} toolbar of {@link JButton}s that do not depend on the modifier list.
	 */
	public final JPanel getSelectionActionBar() {
		return this.selectionActionBar;
	}

	/**
	 * Gets the {@link JButton} for adding a new {@link MemoryDBRow}.
	 *
	 * @return The {@link JButton} for adding a new {@link MemoryDBRow}.
	 */
	public final JButton getAddNewButton() {
		return this.addNewButton;
	}

	/**
	 * Gets the {@link JButton} for replacing the selection with all of the {@link MemoryDBRow}s in the database.
	 *
	 * @return The {@link JButton} for replacing the selection with all of the {@link MemoryDBRow}s in the database.
	 */
	public final JButton getSelectAllButton() {
		return this.selectAllButton;
	}

	/**
	 * Gets the {@link JButton} for displaying info on the all of the selected {@link MemoryDBRow}s.
	 *
	 * @return The {@link JButton} for displaying info on the all of the selected {@link MemoryDBRow}s.
	 */
	public final JButton getInfoButton() {
		return this.infoButton;
	}

	/**
	 * Gets the {@link JButton} for removing all of the selected {@link MemoryDBRow}s.
	 *
	 * @return The {@link JButton} for removing all of the selected {@link MemoryDBRow}s.
	 */
	public final JButton getRemoveButton() {
		return this.removeButton;
	}

	/**
	 * Gets the {@link JPanel} wrapping the modifier section.
	 *
	 * @return The {@link JPanel} wrapping the modifier section.
	 */
	public final JPanel getModifierSection() {
		return this.modifierSection;
	}

	/**
	 * Gets the {@link JPanel} of the list of modifiers.
	 *
	 * @return The {@link JPanel} of the list of modifiers.
	 */
	public final JPanel getModifierPanel() {
		return this.modifierPanel;
	}

	/**
	 * Gets the {@link JScrollPane} wrapping the {@link JPanel} of the list of modifiers.
	 *
	 * @return The {@link JScrollPane} wrapping the {@link JPanel} of the list of modifiers.
	 */
	public final JScrollPane getScrollModifierPanel() {
		return this.scrollModifierPanel;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the {@link JScrollPane}
	 * wrapping the {@link JPanel} of the list of modifiers.
	 *
	 * @return The growing {@link JPanel} wrapping the {@link JScrollPane}
	 *         wrapping the {@link JPanel} of the list of modifiers.
	 */
	public final JPanel getGrowScrollModifierPanel() {
		return this.growScrollModifierPanel;
	}

	/**
	 * Gets the {@link JPanel} toolbar of {@link JButton}s
	 * that depend on the modifier list, plus a credits {@link JLabel}.
	 *
	 * @return Gets the {@link JPanel} toolbar of {@link JButton}s
	 *         that depend on the modifier list, plus a credits {@link JLabel}.
	 */
	public final JPanel getModifierActionBar() {
		return this.modifierActionBar;
	}

	/**
	 * Gets the {@link JLabel} for the credits.
	 *
	 * @return The {@link JLabel} for the credits.
	 */
	public final JLabel getCreditsLabel() {
		return this.creditsLabel;
	}

	/**
	 * Gets the {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 * to only the specified modifier list particulars.
	 *
	 * @return The {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 *         to only the specified modifier list particulars.
	 */
	public final JButton getFilterOnlyButton() {
		return this.filterOnlyButton;
	}

	/**
	 * Gets the {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 * to exclude the specified modifier list particulars.
	 *
	 * @return The {@link JButton} for further limiting the selection of {@link MemoryDBRow}s
	 *         to exclude the specified modifier list particulars.
	 */
	public final JButton getFilterOutButton() {
		return this.filterOutButton;
	}

	/**
	 * Gets he {@link JButton} to try to apply the modifier list particulars so that
	 * the selection of {@link MemoryDBRow}s conforms to it.
	 *
	 * @return The {@link JButton} to try to apply the modifier list particulars so that
	 *         the selection of {@link MemoryDBRow}s conforms to it.
	 */
	public final JButton getApplyButton() {
		return this.applyButton;
	}

	/**
	 * Gets the {@link JSplitPane} splitting the selection and modifier sections.
	 *
	 * @return The {@link JSplitPane} splitting the selection and modifier sections.
	 */
	public final JSplitPane getSplitViewSectionsContainer() {
		return this.splitViewSectionsContainer;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the {@link JSplitPane} splitting the selection and modifier sections.
	 *
	 * @return The growing {@link JPanel} wrapping the {@link JSplitPane} splitting the selection and modifier sections.
	 */
	public final JPanel getGrowSplitViewSectionsContainer() {
		return this.growSplitViewSectionsContainer;
	}
}
