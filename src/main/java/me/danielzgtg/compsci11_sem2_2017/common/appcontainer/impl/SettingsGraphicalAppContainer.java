package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

import java.awt.Component;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

/**
 * A Swing app container for settings.
 *
 * @author Daniel Tang
 * @since 21 May 2017
 */
public final class SettingsGraphicalAppContainer extends AppContainer {

	/**
	 * The tabs, and their contents.
	 */
	private final JTabbedPane wrappedTabs;

	/**
	 * The title of the window.
	 */
	private String tmpTitle;

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	/**
	 * The callback for when the setting configurator has closed.
	 */
	private final Consumer<Map<Object, Object>> callback;

	/**
	 * The checkers for determining whether the settings are valid.
	 */
	private final List<BooleanSupplier> finalCheckers;

	/**
	 * The backing setting storage.
	 */
	private final Map<Object, Object> backend;

	/**
	 * Creates a new Settings configurator.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param initialBackend The backing setting storage, not null.
	 * @param categories A {@link List} of the order of category tabs, null to disable adding tabs in advance.
	 * @param settings A {@link List} of {@link SettingMeta}data, not null.
	 * @param callback The callback to run after the setting configurator has closed, not null,
	 *                 will be passed {@code null} if the backend is invalid, the backend otherwise.
	 */
	@SuppressWarnings("unchecked")
	public SettingsGraphicalAppContainer(final Map<?, ?> constantsMap, final Map<Object, Object> initialBackend,
			final List<String> categories,
			final List<Pair<String, SettingMeta<?>>> settings,
			final Consumer<Map<Object, Object>> callback) {
		Validate.notNull(constantsMap);
		Validate.notNull(initialBackend);
		Validate.notNull(settings);
		Validate.notNull(callback);

		final Widgets widgets = new Widgets(constantsMap, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		// Prepare Categories
		this.wrappedTabs = new JTabbedPane();
		final Map<String, JPanel> categoryPanels = new HashMap<>();

		if (categories != null) {
			for (final String category : categories) {
				if (category != null) {
					if (categoryPanels.get(category) == null) {
						final JPanel categoryPanel = new JPanel();
						SwingUtils.setFlowDirection(categoryPanel, false);

						categoryPanels.put(category, categoryPanel);
						this.wrappedTabs.add(category, SwingUtils.wrapWithScrolling(categoryPanel));
					}
				}
			}
		}

		final List<BooleanSupplier> tmpFinalCheckers = new LinkedList<>();
		this.backend = new HashMap<>(initialBackend);
		for (final Pair<String, SettingMeta<?>> setting : settings) {
			final String category = setting.getLeft();
			Validate.notNull(category);
			final SettingMeta<?> meta = setting.getRight();
			Validate.notNull(meta);

			final JPanel categoryPanel;
			{
				final JPanel tryCategoryPanel = categoryPanels.get(category);

				if (tryCategoryPanel == null) {
					categoryPanel = new JPanel();
					SwingUtils.setFlowDirection(categoryPanel, false);

					categoryPanels.put(category, categoryPanel);
					this.wrappedTabs.add(category, SwingUtils.wrapWithScrolling(categoryPanel));
				} else {
					categoryPanel = tryCategoryPanel;
				}
			}

			final JPanel settingPanel = new JPanel();
			SwingUtils.setFlowDirection(settingPanel, false);
			SwingUtils.setTitle(settingPanel, meta.label);

			final JLabel settingInfoLabel = widgets.createRegularLabel();

			final Class<?> type = meta.getType();
			if (type.equals(String.class)) {
				final String initialValue = Coerce.asString(this.backend.get(meta.id));

				final JTextArea textArea = new JTextArea(initialValue);
				textArea.setLineWrap(true);

				final Consumer<String> updateFunc = (value) -> {
					this.backend.put(meta.id, value);

					final String info = (meta.validator.test(value) ?
							meta.descriptionFunc : meta.invalidDescriptionFunc).apply(value);
					if (info != null) {
						settingInfoLabel.setText(info);
						settingInfoLabel.setVisible(true);
					} else {
						settingInfoLabel.setVisible(false);
					}
				};

				SwingUtils.hookDocumentGenericUpdate(textArea.getDocument(),
						(event) -> updateFunc.accept(textArea.getText()));

				updateFunc.accept(initialValue);
				settingPanel.add(textArea);

				tmpFinalCheckers.add(() -> meta.validator.test(this.backend.get(meta.id)));
			} else if (type.equals(Integer.class)) {
				final int initialValue = Coerce.asInt(this.backend.get(meta.id));

				final Consumer<Integer> updateFunc = (value) -> {
					this.backend.put(meta.id, value);

					final String info = (meta.validator.test(value) ?
							meta.descriptionFunc : meta.invalidDescriptionFunc).apply(value);
					if (info != null) {
						settingInfoLabel.setText(info);
						settingInfoLabel.setVisible(true);
					} else {
						settingInfoLabel.setVisible(false);
					}
				};

				final SpinnerNumberModel model =
						new SpinnerNumberModel(initialValue, null, null, 1);
				final JSpinner spinner = new JSpinner(model);

				spinner.addChangeListener((event) -> updateFunc.accept(model.getNumber().intValue()));

				updateFunc.accept(initialValue);
				settingPanel.add(spinner);

				tmpFinalCheckers.add(() -> meta.validator.test(this.backend.get(meta.id)));
			} else if (type.equals(Boolean.class)) {
				final boolean initialValue = Coerce.asBool(this.backend.get(meta.id));

				final Consumer<Boolean> updateFunc = (value) -> {
					this.backend.put(meta.id, value);

					final String info = (meta.validator.test(value) ?
							meta.descriptionFunc : meta.invalidDescriptionFunc).apply(value);
					if (info != null) {
						settingInfoLabel.setText(info);
						settingInfoLabel.setVisible(true);
					} else {
						settingInfoLabel.setVisible(false);
					}
				};

				final JCheckBox checkbox = new JCheckBox();
				checkbox.setSelected(initialValue);

				SwingUtils.hookCheckboxToggle(checkbox, updateFunc);
				updateFunc.accept(initialValue);

				final JPanel growCheckBox = new JPanel();
				SwingUtils.setFlowDirection(growCheckBox, true);
				growCheckBox.add(checkbox);
				growCheckBox.add(Box.createHorizontalGlue());

				settingPanel.add(growCheckBox);

				tmpFinalCheckers.add(() -> meta.validator.test(this.backend.get(meta.id)));
			} else if (Enum.class.isAssignableFrom(type)) {
				final Class<? extends Enum> enumClass = (Class<? extends Enum>) type;
				final Enum<?> initialValue = Coerce.asEnum(enumClass, this.backend.get(meta.id));

				final Consumer<Enum> updateFunc = (value) -> {
					this.backend.put(meta.id, value);

					final String info = (meta.validator.test(value) ?
							meta.descriptionFunc : meta.invalidDescriptionFunc).apply(value);
					if (info != null) {
						settingInfoLabel.setText(info);
						settingInfoLabel.setVisible(true);
					} else {
						settingInfoLabel.setVisible(false);
					}
				};

				final SpinnerListModel model = new SpinnerListModel(Arrays.asList(enumClass.getEnumConstants()));
				model.setValue(initialValue);
				final JSpinner spinner = new JSpinner(model);

				spinner.addChangeListener((event) -> updateFunc.accept((Enum<?>) model.getValue()));

				updateFunc.accept(initialValue);
				settingPanel.add(spinner);

				tmpFinalCheckers.add(() -> meta.validator.test(this.backend.get(meta.id)));
			} else {
				throw new UnsupportedOperationException("Can't handle setting type: " + type.getName());
			}
			settingPanel.add(settingInfoLabel);

			categoryPanel.add(settingPanel);
		}
		this.finalCheckers = Collections.unmodifiableList(tmpFinalCheckers);

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.settingsgui.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.settingsgui.initialWindowHeight")));

		// Title
		this.tmpTitle = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.settingsgui.title"));

		// Copy Parameters to Fields
		this.callback = callback;
	}

	@Override
	protected void doLaunch() {
		final JFrame frame = SwingUtils.generateOnetimeJFrame(this.tmpTitle);
		this.tmpTitle = null; // Free

		SwingUtils.hookJFrameClosing(frame, () -> {
			for (final BooleanSupplier checker : this.finalCheckers) {
				if (!checker.getAsBoolean()) {
					this.callback.accept(null);
					return true;
				}
			}

			this.callback.accept(this.backend);
			return true;
		});

		// Setup and Launch
		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.wrappedTabs
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth,  this.initialWindowHeight);
		SwingUtils.launchJFrame(frame);
	}
}
