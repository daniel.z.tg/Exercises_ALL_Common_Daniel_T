package me.danielzgtg.compsci11_sem2_2017.common.appcontainer;

import me.danielzgtg.compsci11_sem2_2017.common.Log;

/**
 * An app container pattern for simple apps.
 *
 * @author Daniel Tang
 * @since 16 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AppContainer {

	/**
	 * Called on the instantiation of a new {@link AppContainer}.
	 *
	 * Logs that a new app container has been created.
	 */
	protected AppContainer() {
		Log.log("[AppContainers] Constructed App...: " + this.getClass().getName());
	}

	/**
	 * Flag to prevent app containers being launched more than once.
	 */
	private boolean launched = false;

	/**
	 * Tries to launch the app. Apps may only be launched once. To re-launch an app, instantiate a new one.
	 */
	public final void launch() {
		synchronized (this) {
			if (this.launched) {
				throw new IllegalStateException("App Already Launched!");
			}

			this.launched = true;
		}

		final String ourName = this.getClass().getName();
		Log.log("[AppContainers] Launching App...: " + ourName);

		this.doLaunch();

		Log.log("[AppContainers] App Launched!: " + ourName);
	}

	/**
	 * Actually launches the app.
	 */
	protected abstract void doLaunch();
}
