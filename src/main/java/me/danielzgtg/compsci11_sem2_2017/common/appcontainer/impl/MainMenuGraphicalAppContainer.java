package me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BooleanSupplier;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Component;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

/**
 * A Swing app container for Main Menus.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("unused")
public final class MainMenuGraphicalAppContainer extends AppContainer {

	/**
	 * The {@link JPanel} of the content.
	 */
	private final JPanel contentPanel;

	/**
	 * The {@link JPanel} of the centered content; excluding the about bar.
	 */
	private final JPanel mainContentPanel;

	/**
	 * The title {@link JLabel}.
	 */
	private final JLabel titleLabel;

	/**
	 * The growing {@link JPanel} wrapping the title {@link JLabel}.
	 */
	private final JPanel growTitleLabel;

	/**
	 * The map of controls to their ids.
	 */
	private final Map<String, Pair<JButton, JPanel>> actionControls;

	/**
	 * A {@link List} of functions to link to the controls.
	 */
	private List<Pair<JButton, BooleanSupplier>> tmpActionControlFunctions;

	/**
	 * The title of the window.
	 */
	private String tmpTitle;

	/**
	 * The bottom about bar area.
	 */
	private final JPanel aboutPanel;

	/**
	 * The version info {@link JLabel}.
	 */
	private final JLabel versionLabel;

	/**
	 * The credits {@link JLabel}.
	 */
	private final JLabel creditsLabel;

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	/**
	 * Creates a new Main Menu app container.
	 * An action {@link BooleanSupplier} that returns {@code true} will close this app container.
	 *
	 * @param constantsMap The {@link Map} storing the program's configuration for behavioural constants,
	 *                     correctly formatted.
	 * @param controls The {@link List} of {@link JButton} ids, names, and actions.
	 */
	public MainMenuGraphicalAppContainer(final Map<?, ?> constantsMap,
			final List<Pair<String, Pair<String, BooleanSupplier>>> controls) {
		Validate.notNull(constantsMap);
		Validate.notNull(controls);

		final Widgets widgets =
				new Widgets(ResourceUtils.COMMON_CONSTANT_DATA, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		// Content
		this.mainContentPanel = new JPanel();
		SwingUtils.setFlowDirection(this.mainContentPanel, false);

		// Title
		this.tmpTitle = Coerce.asString(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.title"));
		this.titleLabel = widgets.createTitleLabel(this.tmpTitle);
		this.growTitleLabel = SwingUtils.wrapInGrowingJPanel(this.titleLabel);
		this.mainContentPanel.add(this.growTitleLabel);

		// Control Buttons
		final Map<String, Pair<JButton, JPanel>> tmpActionControls = new HashMap<>();
		this.tmpActionControlFunctions = new LinkedList<>();
		for (final Pair<String, Pair<String, BooleanSupplier>> control : controls) {
			Validate.notNull(control);

			final String name = control.getLeft();
			Validate.notNull(name);
			final Pair<String, BooleanSupplier> controlData = control.getRight();
			Validate.notNull(controlData);

			final String controlLabelUnlocalized = controlData.getLeft();
			Validate.notNull(controlLabelUnlocalized);
			final BooleanSupplier controlAction = controlData.getRight();
			Validate.notNull(controlAction);

			final JButton controlButton =
					widgets.createRegularButton(Coerce.asString(constantsMap.get(controlLabelUnlocalized)));
			final JPanel growControlButton = SwingUtils.wrapInGrowingJPanel(controlButton);

			tmpActionControls.put(name, new Pair<>(controlButton, growControlButton));
			this.tmpActionControlFunctions.add(new Pair<>(controlButton, controlAction));

			this.mainContentPanel.add(widgets.createSimpleSpacer(true));
			this.mainContentPanel.add(growControlButton);
		}
		this.actionControls = Collections.unmodifiableMap(tmpActionControls);

		// About Bar
		this.aboutPanel = new JPanel();
		SwingUtils.setFlowDirection(this.aboutPanel, true);
		{
			// Version
			this.versionLabel = widgets.createRegularLabel(Coerce.asString(constantsMap.get(
					"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.version")));

			// Credits
			this.creditsLabel = widgets.createRegularLabel(Coerce.asString(constantsMap.get(
					"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.credits")));

			// Add About Bar Contents
			this.aboutPanel.add(this.versionLabel);
			this.aboutPanel.add(Box.createHorizontalGlue());
			this.aboutPanel.add(this.creditsLabel);
		}

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE, Coerce.asInt(constantsMap.get(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.initialWindowHeight")));

		// Layout the Panel
		this.contentPanel = new JPanel();
		SwingUtils.setFlowDirection(this.contentPanel, false);

		this.contentPanel.add(SwingUtils.centerInJPanel(this.mainContentPanel));
		this.contentPanel.add(this.aboutPanel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doLaunch() {
		// Show window
		final JFrame frame = SwingUtils.generateOnetimeJFrame(this.tmpTitle);
		this.tmpTitle = null; // Free

		// Link Control Buttons
		for (final Pair<JButton, BooleanSupplier> actionControlFunction : tmpActionControlFunctions) {
			final BooleanSupplier controlAction = actionControlFunction.getRight();

			SwingUtils.hookButtonPress(actionControlFunction.getLeft(), () -> {
				if (controlAction.getAsBoolean()) {
					frame.dispose();
				}
			});
		}
		this.tmpActionControlFunctions = null; // Free

		// Setup and Launch
		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.contentPanel
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth,  this.initialWindowHeight);
		SwingUtils.launchJFrame(frame);
	}

	/**
	 * Gets the {@link JPanel} of the content.
	 *
	 * @return The {@link JPanel} of the content.
	 */
	public JPanel getContentPanel() {
		return this.contentPanel;
	}

	/**
	 * Gets the {@link JPanel} of the centered content; excluding the about bar.
	 *
	 * @return The {@link JPanel} of the centered content; excluding the about bar.
	 */
	public JPanel getMainContentPanel() {
		return this.mainContentPanel;
	}

	/**
	 * Gets the title {@link JLabel}.
	 *
	 * @return The title {@link JLabel}.he title {@link JLabel}.
	 */
	public JLabel getTitleLabel() {
		return this.titleLabel;
	}

	/**
	 * Gets the growing {@link JPanel} wrapping the title {@link JLabel}.
	 *
	 * @return The growing {@link JPanel} wrapping the title {@link JLabel}.
	 */
	public JPanel getGrowTitleLabel() {
		return this.growTitleLabel;
	}

	/**
	 * Gets the map of controls to their ids.
	 *
	 * @return The map of controls to their ids.
	 */
	public Map<String, Pair<JButton, JPanel>> getActionControls() {
		return this.actionControls;
	}

	/**
	 * Gets the bottom about bar area.
	 *
	 * @return The bottom about bar area.
	 */
	public JPanel getAboutPanel() {
		return this.aboutPanel;
	}

	/**
	 * Gets the version info {@link JLabel}.
	 *
	 * @return The version info {@link JLabel}.
	 */
	public JLabel getVersionLabel() {
		return this.versionLabel;
	}

	/**
	 * Gets the credits {@link JLabel}.
	 *
	 * @return The credits {@link JLabel}.
	 */
	public JLabel getCreditsLabel() {
		return this.creditsLabel;
	}
}
