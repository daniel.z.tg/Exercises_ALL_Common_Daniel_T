package me.danielzgtg.compsci11_sem2_2017.common.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Tools to help with stream serialization.
 *
 * @author Daniel Tang
 * @since 16 June 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class IOStreamUtil {

	/**
	 * Writes an {@code int} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to write to.
	 * @param i The {@code int} to write.
	 * @throws IOException if the {@link OutputStream} does.
	 */
	public static final void writeInt(final OutputStream out, final int i) throws IOException {
		out.write((i >>> 24) & 0xFF);
		out.write((i >>> 16) & 0xFF);
		out.write((i >>> 8) & 0xFF);
		out.write(i & 0xFF);
	}

	/**
	 * Writes an {@code char} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to write to.
	 * @param c The {@code char} to write.
	 * @throws IOException if the {@link OutputStream} does.
	 */
	public static final void writeChar(final OutputStream out, final char c) throws IOException {
		out.write((c >>> 8) & 0xFF);
		out.write(c & 0xFF);
	}

	/**
	 * Writes an {@link String} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to write to.
	 * @param str The {@link String} to write.
	 * @throws IOException if the {@link OutputStream} does.
	 */
	public static final void writeString(final OutputStream out, final String str) throws IOException {
		final int length = str.length();
		writeInt(out, length);

		for (int i = 0; i < length; i++) {
			writeChar(out, str.charAt(i));
		}
	}

	/**
	 * Reads an {@code int} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} to read from.
	 * @return The {@code int} that was read.
	 * @throws IOException if the {@link InputStream} does.
	 */
	public static final int readInt(final InputStream in) throws IOException {
		int result = in.read() << 24;
		result += in.read() << 16;
		result += in.read() << 8;
		result += in.read();

		return result;
	}

	/**
	 * Reads an {@code char} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} to read from.
	 * @return The {@code char} that was read.
	 * @throws IOException if the {@link InputStream} does.
	 */
	public static final char readChar(final InputStream in) throws IOException {
		int result = in.read() << 8;
		result += in.read();

		return (char) result;
	}

	/**
	 * Reads an {@link String} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} to read from.
	 * @return The {@link String} that was read.
	 * @throws IOException if the {@link InputStream} does.
	 */
	public static final String readString(final InputStream in) throws IOException {
		final int length = readInt(in);

		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < length; i++) {
			result.append(readChar(in));
		}

		return result.toString();
	}

	@Deprecated
	private IOStreamUtil() { throw new UnsupportedOperationException(); }
}
