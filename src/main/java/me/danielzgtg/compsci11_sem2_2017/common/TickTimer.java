package me.danielzgtg.compsci11_sem2_2017.common;

/**
 * A stopwatch-like timer.
 *
 * @author Daniel Tang
 * @since 27 March 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class TickTimer {

	/**
	 * The conversion constant between nanoseconds and milliseconds.
	 */
	public static final long NANOSECONDS_MILLISECONDS_CONV = 1_000_000L;

	/**
	 * The time of the previous expected tick.
	 */
	private long lastTickTime;

	/**
	 * The length of period between ticks. Must be greater than 0.
	 */
	private long period;

	/**
	 * Creates a new {@link TickTimer} with a minimum period.
	 */
	public TickTimer() {
		this.period = 1L;
	}

	/**
	 * Creates a new {@link TickTimer} with the specified {@code period}.
	 *
	 * @param period The length of period between ticks. Must be greater than 0.
	 * @throws IllegalArgumentException If the period is 0 or less.
	 */
	public TickTimer(final long period) {
		this.setPeriod(period);
	}

	/**
	 * Creates a new {@link TickTimer} with the specified {@code period}, and {@code lastTickTime}.
	 * This method is meant to be used internally only for cloning.
	 *
	 * @param period The length of period between ticks.
	 * @param lastTickTime The time of the previous expected tick.
	 */
	private TickTimer(final long period, final long lastTickTime) {
		this.lastTickTime = lastTickTime;
		this.period = period;
	}

	/**
	 * Gets the length of period between ticks.
	 *
	 * @return The length of period between ticks.
	 */
	public final long getPeriod() {
		return this.period;
	}

	/**
	 * Sets the length of period between ticks.
	 *
	 * @param period The length of period between ticks. Must be greater than 0.
	 * @throws IllegalArgumentException If the period is 0 or less.
	 */
	public final void setPeriod(final long period) throws IllegalArgumentException {
		if (period < 1) {
			throw new IllegalArgumentException("A TickTimer is not meaningful without a period!");
		}

		this.period = period;
	}

	/**
	 * Gets the time of the previous expected tick.
	 *
	 * @return The time of the previous expected tick.
	 */
	public final long getLastTickTime() {
		return this.lastTickTime;
	}

	/**
	 * Resets the {@link TickTimer}'s previous expected tick time to now.
	 */
	public final void reset() {
		this.lastTickTime = System.nanoTime();
	}

	/**
	 * Gets the elapsed time since the previous expected tick, and resets the tick time to now.
	 *
	 * @return The elapsed time since the previous expected tick.
	 */
	public final long getElapsed() {
		return -this.lastTickTime + (this.lastTickTime = System.nanoTime());
	}

	/**
	 * Gets the elapsed time since the previous expected tick, but does NOT reset the tick time.
	 *
	 * @return The elapsed time since the previous expected tick.
	 */
	public final long peekElapsed() {
		return System.nanoTime() - this.lastTickTime;
	}

	/**
	 * Determines whether it is time for the next tick yet, and if so, ticks forward.
	 *
	 * @return {@code true} if it is time for the next tick.
	 */
	public final boolean nextTickReady() {
		if ((System.nanoTime() - this.lastTickTime) >= this.period) {
			this.lastTickTime += this.period;
			return true;
		}

		return false;
	}

	/**
	 * Determines whether it is time for the next tick yet, but does NOT tick.
	 *
	 * @return {@code true} if it is time for the next tick.
	 */
	public final boolean peekTickReady() {
		return (System.nanoTime() - this.lastTickTime) >= this.period;
	}

	/**
	 * Puts the current {@link Thread} to sleep until it is time for the next tick.
	 */
	public final void waitUntilNextTick() {
		final long waitTime = this.period - (System.nanoTime() - this.lastTickTime);

		this.lastTickTime += this.period;

		if (waitTime > 0) {
			try {
				Thread.sleep(waitTime / NANOSECONDS_MILLISECONDS_CONV);
			} catch (final InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	/**
	 * Computes a hash code for this {@link TickTimer}.
	 * The hash code is simply the {@code long} hashes of the lastTickTime and period XOR'ed together.
	 *
	 * @return The hash code.
	 */
	@Override
	public final int hashCode() {
		return Long.hashCode(this.lastTickTime) ^ Long.hashCode(this.period);
	}

	/**
	 * Determines if this {@link TickTimer} is equal to another {@link Object}.
	 *
	 * @param o The other {@link Object}.
	 * @return {@link true} if the other object is a {@link TickTimer},
	 *         and it contains the same {@code period} and {@code lastTickTime}.
	 */
	@Override
	public final boolean equals(final Object o) {
		if (!(o instanceof TickTimer)) {
			return false;
		}

		final TickTimer other = (TickTimer) o;
		return this.lastTickTime == other.lastTickTime && this.period == other.period;
	}

	/**
	 * Produces a human-readable {@link String} representation of this {@link TickTimer},
	 * including its {@code lastTickTime} and {@code period}.
	 *
	 * @return The human-readable {@link String} representation of this {@link TickTimer}.
	 */
	@Override
	public final String toString() {
		return "TickTimer: { period=" + this.period + ", lastTickTime=" + this.lastTickTime + " }";
	}

	/**
	 * Creates a new {@link TickTimer} with the same contents as this one,
	 * including its {@code lastTickTime} and {@code period}, which can be manipulated independently.
	 *
	 * @return A new identical {@link TickTimer}.
	 */
	@Override
	public final TickTimer clone() {
		return new TickTimer(this.period, this.lastTickTime);
	}
}
