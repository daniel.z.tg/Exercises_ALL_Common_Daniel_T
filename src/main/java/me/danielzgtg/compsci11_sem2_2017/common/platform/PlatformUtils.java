package me.danielzgtg.compsci11_sem2_2017.common.platform;

import me.danielzgtg.compsci11_sem2_2017.common.Log;

/**
 * Tools for detecting the OS.
 *
 * @author Daniel Tang
 * @since 20 April 2017
 */
@SuppressWarnings({"SameParameterValue", "unused"})
public final class PlatformUtils {

	/**
	 * The detected OS type.
	 */
	private static OperatingSystem OS;

	/**
	 * Whether the current platform is high-performance.
	 */
	private static boolean highPerformance = true;

	/**
	 * Does the actual determination of the OS.
	 */
	private static final void calculateOS() {
		OperatingSystem os = null;

		try {
			final String osString = String.valueOf(System.getProperty("os.name")).toLowerCase();

			if (osString.startsWith("linux")) {
				os = OperatingSystem.LINUX;
			} else if (osString.startsWith("windows")) {
				os = OperatingSystem.WINDOWS;
			} else if (osString.startsWith("mac")) {
				os = OperatingSystem.MAC;
			} else {
				Log.log("[PlatformUtils] Running on unknown OS!");
				//os = null;
			}
		} catch (final Exception e) {
			Log.log("[PlatformUtils] Error when detecting OS!");
		}

		OS = os;
	}

	/**
	 * Determines the operating system this program is running on.
	 *
	 * @return The detected operating system.
	 */
	public static final OperatingSystem getOS() {
		return OS;
	}

	/**
	 * Sets whether the current platform should be considered high-performance.
	 *
	 * @param highPerformanceNew {@code true} if the current platform should be considered high-performance.
	 */
	public static final void setHighPerformance(final boolean highPerformanceNew) {
		highPerformance = highPerformanceNew;
	}

	/**
	 * Gets whether the current platform should be considered high-performance.
	 *
	 * @return {@code true} if the current platform should be considered high-performance.
	 */
	public static final boolean getHighPerformance() {
		return highPerformance;
	}

	/**
	 * An enum of supported OSes. {@code null} is used to denote an unknown operating system.
	 */
	public static enum OperatingSystem {
		/**
		 * The Microsoft Windows operating system.
		 */
		WINDOWS,

		/**
		 * The GNU/Linux operating system.
		 */
		LINUX,

		/**
		 * The macOS operating system.
		 */
		MAC
	}

	static {
		calculateOS();
	}

	@Deprecated
	private PlatformUtils() { throw new UnsupportedOperationException(); }
}
