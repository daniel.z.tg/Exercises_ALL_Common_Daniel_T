package me.danielzgtg.compsci11_sem2_2017.common.platform;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.BlockingPipe;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Container;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Flag;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;

/**
 * A workaround for libraries that need work to be done on the main thread.
 *
 * @author Daniel Tang
 * @since 18 June 2017
 */
public final class MainThreadUtils {

	/**
	 * The number of users using the main {@link Thread}
	 */
	private static volatile int users = 1;

	/**
	 * The underlying {@link BlockingPipe}.
	 */
	private static final BlockingPipe<Runnable> pipe = new BlockingPipe<>();

	/**
	 * Runs a {@link Runnable} job as the main thread.
	 *
	 * @param task The {@link Runnable} job to run.
	 * @param block Whether to wait for results.
	 */
	@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
	public static final void run(final Runnable task, final boolean block) {
		pipe.send(task, block);
	}

	/**
	 * Tells the main {@link Thread} there is one more user requiring it.
	 */
	public static synchronized final void require() {
		if (users <= 0) {
			throw new IllegalStateException("Main Thread already dying!");
		}

		users++;
	}

	/**
	 * Tells the main {@link Thread} there is one less user requiring it.
	 */
	public static synchronized final void release() {
		if (users <= 0) {
			throw new IllegalStateException("Tried to release Main Thread, but is is already dying!");
		}

		users--;

		if (users == 0) {
			pipe.release();
		}
	}

	/**
	 * Traps the main {@link Thread}, and makes it do jobs.
	 *
	 * @param otherTask The other task to start.
	 */
	@SuppressWarnings({"SynchronizationOnLocalVariableOrMethodParameter", "InfiniteLoopStatement"})
	public static final void trap(final Runnable otherTask) {
		Validate.notNull(otherTask);

		final long threadID = Thread.currentThread().getId();

		if (threadID != 1) {
			throw new IllegalThreadStateException("Not main thread: " + threadID);
		}

		new Thread(() -> {
			otherTask.run();
			MainThreadUtils.release();
		}).start();

		pipe.trap(Runnable::run);
	}
}
