package me.danielzgtg.compsci11_sem2_2017.common.platform;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import javax.imageio.ImageIO;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.serialization.SerializationUtils;

import static me.danielzgtg.compsci11_sem2_2017.common.Log.log;

/**
 * Tools for loading resources.
 *
 * @author Daniel Tang
 * @since 20 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue"})
public final class ResourceUtils {

	/**
	 * A resource loader for the commonlib {@link ClassLoader}.
	 */
	public static final Function<String, InputStream> COMMONLIB_RESOURCE_LOADER =
			getResourceLoaderForClass(ResourceUtils.class);

	/**
	 * Config for commonlib.
	 */
	public static final Map<?, ?> COMMON_CONSTANT_DATA =
			loadConstantsMap("commonlib/commondata.constants", COMMONLIB_RESOURCE_LOADER);

	/**
	 * A resource loader for external {@link File}s.
	 */
	public static final Function<String, InputStream> EXTERNAL_RESOURCE_LOADER = (path) -> {
		try {
			return new FileInputStream(new File(path));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	};

	/**
	 * Gets the resource loader for a {@link Class}.
	 *
	 * @param root The {@link Class} to get a resource loader from, not null.
	 * @return A resource loader from the {@link Class}.
	 * @throws IllegalArgumentException If {@code root} is {@code null}.
	 */
	public static final Function<String, InputStream> getResourceLoaderForClass(final Class<?> root) {
		Validate.notNull(root);

		return root.getClassLoader()::getResourceAsStream;
	}

	/**
	 * Loads the constants {@link Map} from the {@link InputStream}.
	 *
	 * @param path The bundled path of the {@link SerializationUtils}-style constants {@link Map}.
	 * @param loader The resource loader the {@code path} refers to, not null.
	 * @return A {@link SerializationUtils}-style {@link Map} with
	 *         the parsed {@link SerializationUtils}-style data contents.
	 */
	public static final Map<?, ?> loadConstantsMap(final String path, final Function<String, InputStream> loader) {
		Validate.notNull(loader);

		try {
			return Collections.unmodifiableMap(
					SerializationUtils.decode(loader.apply(path)));
		} catch (final Exception e) {
			throw new RuntimeException("ResourceUtils could not load the constants map!", e);
		}
	}

	/**
	 * Loads the constants {@link Map} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} with the {@link SerializationUtils}-style constants data, not null.
	 * @return A {@link SerializationUtils}-style {@link Map} with
	 *         the parsed {@link SerializationUtils}-style data contents.
	 */
	public static final Map<?, ?> loadConstantsMap(final InputStream in) {
		Validate.notNull(in);

		try {
			return Collections.unmodifiableMap(SerializationUtils.decode(in));
		} catch (final Exception e) {
			throw new RuntimeException("ResourceUtils could not load the constants map!", e);
		}
	}

	/**
	 * Loads the {@link BufferedImage} from the path.
	 *
	 * @param path The path of the {@link BufferedImage}.
	 * @param loader The resource loader the {@code path} refers to, not null.
	 * @return The {@link BufferedImage} if loading was successful, else {@code null} otherwise.
	 */
	public static final BufferedImage loadImgSafe(final String path, final Function<String, InputStream> loader) {
		Validate.notNull(loader);

		log("[ResourceUtils] Loading Image from: " + path);
		try {
			return ImageIO.read(loader.apply(path));
		} catch (IOException e) {
			log("[ResourceUtils] Failed to load Image from: " + path);
			log(e);
			return null;
		} finally {
			log("[ResourceUtils] Loaded Image from: " + path);
		}
	}

	/**
	 * Loads the {@link BufferedImage} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} with the {@link BufferedImage}, not null.
	 * @return The {@link BufferedImage} if loading was successful, else {@code null} otherwise.
	 */
	public static final BufferedImage loadImgSafe(final InputStream in) {
		Validate.notNull(in);

		log("[ResourceUtils] Loading Image from: " + in);

		try {
			return ImageIO.read(in);
		} catch (IOException e) {
			log("[ResourceUtils] Failed to load Image from: " + in);
			log(e);
			return null;
		} finally {
			log("[ResourceUtils] Loaded Image from: " + in);
		}
	}

	/**
	 * Loads the {@link Font} from the path.
	 *
	 * @param path The path of the {@link Font}.
	 * @param loader The resource loader the {@code path} refers to, not null.
	 * @return The {@link Font}.
	 * @throws IOException If an IO error occurred during loading.
	 * @throws FontFormatException If the {@link Font} data is invalid.
	 * @throws IllegalArgumentException If {@code loader} is {@code null}, or the resource could not be found.
	 */
	public static final Font loadTTF(final String path, final Function<String, InputStream> loader)
			throws IOException, FontFormatException {
		Validate.notNull(loader);

		return loadTTF(loader.apply(path));
	}

	/**
	 * Loads the {@link Font} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} with the {@link Font}, not null.
	 * @return The {@link Font}.
	 * @throws IOException If an IO error occurred during loading.
	 * @throws FontFormatException If the {@link Font} data is invalid.
	 * @throws IllegalArgumentException If {@code in} is {@code null}.
	 */
	public static final Font loadTTF(final InputStream in) throws IOException, FontFormatException {
		Validate.notNull(in);

		return Font.createFont(Font.TRUETYPE_FONT, in);
	}

	/**
	 * Loads the {@link Font} from the path.
	 *
	 * @param path The path of the {@link Font}.
	 * @param loader The resource loader the {@code path} refers to, not null.
	 * @return The {@link Font} if loading was successful, else {@code null} otherwise.
	 */
	public static final Font loadTTFSafe(final String path, final Function<String, InputStream> loader) {
		Validate.notNull(loader);

		log("[ResourceUtils] Loading Font from: " + path);

		try {
			final Font result = loadTTF(loader.apply(path));

			log("[ResourceUtils] Loaded Font from: " + path);
			return result;
		} catch (final Exception e) {
			log("[ResourceUtils] Failed to load Font from " + path);
			log(e);

			return null;
		}
	}

	/**
	 * Loads the {@link Font} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} with the {@link Font}, not null.
	 * @return The {@link Font} if loading was successful, else {@code null} otherwise.
	 */
	public static final Font loadTTFSafe(final InputStream in) {
		Validate.notNull(in);

		log("[ResourceUtils] Loading Font from: " + in);

		try {
			final Font result = loadTTF(in);

			log("[ResourceUtils] Loaded Font from: " + in);
			return result;
		} catch (final Exception e) {
			log("[ResourceUtils] Failed to load Font from: " + in);
			log(e);

			return null;
		}
	}

	/**
	 * Reads all the {@code byte}s in an {@link InputStream} into a {@code byte[]}.
	 *
	 * @param in The {@link InputStream} to read.
	 * @return A {@code byte[]} of the read {@code byte}s.
	 * @throws IOException If there was a problem reading.
	 */
	public static final byte[] readFully(final InputStream in) throws IOException {
		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			while (true) {
				int read = in.read();

				if (read >= 0) {
					baos.write(read);
				} else {
					return baos.toByteArray();
				}
			}
		}
	}

	/**
	 * Loads the multiline {@link String} from the {@link InputStream}.
	 *
	 * @param in The {@link InputStream} to read.
	 * @return A {@link String} of the rest of the {@link InputStream}.
	 * @throws IOException if {@link BufferedReader#readLine()} does.
	 */
	public static final String readAsString(final InputStream in) throws IOException {
		Validate.notNull(in);

		final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		final StringBuilder result = new StringBuilder();

		String read;
		while ((read = reader.readLine()) != null) {
			result.append(read);
			result.append('\n');
		}

		return result.toString();
	}

	/**
	 * Loads the multiline {@link String} from the path.
	 *
	 * @param path The path of the multiline {@link String}.
	 * @param loader The resource loader the {@code path} refers to, not null.
	 * @return A multiline {@link String} of the contents of the path.
	 * @throws IOException if {@link BufferedReader#readLine()} does.
	 */
	public static final String readAsString(final String path, final Function<String, InputStream> loader)
			throws IOException {
		Validate.notNull(loader);

		return readAsString(loader.apply(path));
	}

	@Deprecated
	private ResourceUtils() { throw new UnsupportedOperationException(); }
}
