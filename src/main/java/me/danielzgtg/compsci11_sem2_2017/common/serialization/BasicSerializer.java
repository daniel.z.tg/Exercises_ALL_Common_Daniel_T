package me.danielzgtg.compsci11_sem2_2017.common.serialization;

import java.io.OutputStream;

/**
 * An implementation detail used by {@link SerializationUtils}.
 */
@FunctionalInterface
/*packaged*/ abstract interface BasicSerializer {

	/**
	 * Serializes the data to the output.
	 *
	 * @param out The output for the data.
	 * @param data The data to serialize.
	 * @throws Exception If the serialization failed.
	 * @see SerializationUtils#writeObject(OutputStream, Object)
	 */
	abstract void serialize(final OutputStream out, final Object data) throws Exception;
}
