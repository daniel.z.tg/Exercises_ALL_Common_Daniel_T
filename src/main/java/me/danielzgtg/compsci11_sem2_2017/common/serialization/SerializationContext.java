package me.danielzgtg.compsci11_sem2_2017.common.serialization;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;

/**
 * Converts between machine-usable {@link T}s and more portable {@link Map}s.
 *
 * @param <T> The type of machine-readable {@link Object} that this operates on.
 *
 * @author Daniel Tang
 * @since 1 May 2017
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class SerializationContext<T> {

	/**
	 * The {@link Function}s that convert between machine-usable {@link T}s and more portable {@link Map}s.
	 */
	private final Map<Class<? extends T>, Pair<Function<Map, ? extends T>,
			Function<? extends T, Map>>> serializers = new HashMap<>();

	/**
	 * Sets the {@link Function}s that convert between machine-usable {@link S}s and more portable {@link Map}s.
	 *
	 * @param type The {@link Class} of {@link S}.
	 * @param serializer The {@link Function} that converts
	 *                   between machine-usable {@link S}s and more portable {@link Map}s.
	 * @param <S> {@link T} or a subclass that the {@link Map} can be converted between.
	 */
	@SuppressWarnings("unchecked")
	public final <S extends T> void setSerializer(final Class<S> type,
			final Pair<Function<Map, S>, Function<S, Map>> serializer) {
		Validate.notNull(type);

		if (serializer == null) {
			this.serializers.remove(type);
		} else {
			Validate.notNull(serializer.getLeft());
			Validate.notNull(serializer.getRight());
			this.serializers.put(
					type, (Pair<Function<Map, ? extends T>, Function<? extends T, Map>>) (Object) serializer);
		}
	}

	/**
	 * Resets the {@link SerializationContext} by removing all stored serializers.
	 */
	public final void reset() {
		this.serializers.clear();
	}

	/**
	 * Converts a portable {@link Map} into a machine-usable {@link S}.
	 *
	 * @param type The {@link Class} of {@link S}, not null.
	 * @param target A portable {@link Map} representing a {@link S},
	 *               nullable if the converting {@link Function} accepts that.
	 * @param <S> {@link T} or a subclass to convert the {@link Map} into.
	 * @throws IllegalArgumentException If {@code type} is {@code null}.
	 * @return An instance of {@link S}, nullable if the converting {@link Function} returns that.
	 */
	@SuppressWarnings("unchecked")
	public final <S extends T> S decode(final Class<S> type, final Map target) {
		Validate.notNull(type);

		final Pair<Function<Map, S>, Function<S, Map>> tools =
				(Pair<Function<Map, S>, Function<S, Map>>) (Object) this.serializers.get(type);

		if (tools == null) {
			throw new UnsupportedOperationException();
		}

		final Function<Map, S> deserializer = tools.getLeft();
		assert deserializer != null;

		return deserializer.apply(target);
	}

	/**
	 * Converts a machine-usable {@link S} into a more portable {@link Map},
	 * the type determined by {@link Object#getClass()}.
	 *
	 * @param target An instance of {@link S}, not null.
	 * @param <S> {@link T} or a subclass to convert to a {@link Map} from.
	 * @return A more portable {@link Map} representing the machine-usable {@link S},
	 *         nullable if the converting {@link Function} returns that.
	 * @throws IllegalArgumentException If {@code target} is {@code null}.
	 */
	@SuppressWarnings("unchecked")
	public final <S extends T> Map encode(final S target) {
		Validate.notNull(target);

		return this.encode((Class<S>) target.getClass(), target);
	}

	/**
	 * Converts a machine-usable {@link S} into a more portable {@link Map}.
	 *
	 * @param type The {@link Class} of {@link S}, not null.
	 * @param target An instance of {@link S}, nullable if the converting {@link Function} accepts that.
	 * @param <S> {@link T} or a subclass to convert to a {@link Map} from.
	 * @return A more portable {@link Map} representing the machine-usable {@link S},
	 *         nullable if the converting {@link Function} returns that.
	 * @throws IllegalArgumentException If {@code type} is {@code null}.
	 */
	@SuppressWarnings("unchecked")
	public final <S extends T> Map encode(final Class<S> type, final S target) {
		final Pair<Function<Map, ? extends T>, Function<? extends T, Map>> tools = this.serializers.get(type);

		if (tools == null) {
			throw new UnsupportedOperationException();
		}

		final Function<S, Map> serializer = (Function<S, Map>) tools.getRight();
		assert serializer != null;

		return serializer.apply(target);
	}

}
