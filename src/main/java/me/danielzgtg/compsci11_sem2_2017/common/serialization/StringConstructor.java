package me.danielzgtg.compsci11_sem2_2017.common.serialization;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

/**
 * A tool that can be used to build {@link String}s out of {@code byte}s.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("WeakerAccess")
public class StringConstructor implements CharSequence {

	/**
	 * Creates a new {@link StringConstructor} with the default {@link Charset}.
	 */
	public StringConstructor() {
		this(null);
	}

	/**
	 * Creates a new {@link StringConstructor} with a custom {@link Charset}.
	 *
	 * @param charset The {@link Charset} to build the {@link String} with {@code byte}s with.
	 */
	public StringConstructor(final Charset charset) {
		this.charset = charset;
	}

	/**
	 * The output to build the result on.
	 */
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();

	/**
	 * The cached result.
	 */
	private String stringCache = null;

	/**
	 * The {@link Charset} to build the {@link String} with {@code byte}s with.
	 */
	private final Charset charset;

	/**
	 * Adds another {@code byte} to this {@link StringConstructor}.
	 *
	 * @param b The {@code byte} to add.
	 */
	public synchronized void add(final byte b) {
		this.stringCache = null;

		this.output.write(b & 0xFF);
	}

	/**
	 * Gets the length of the result of {@link StringConstructor}.
	 * This delegates to {@link StringConstructor#toString()} and {@link String#length()}
	 *
	 * @return The length of the result of {@link StringConstructor}.
	 */
	@Override
	public int length() {
		return this.toString().length();
	}

	/**
	 * Gets the {@code char} at the specified index.
	 * This delegates to {@link StringConstructor#toString()} and {@link String#charAt(int)}
	 *
	 * @param index The index of the requested {@code char}.
	 * @return The {@code char} at the specified index.
	 * @see StringConstructor#toString()
	 * @see StringConstructor#charAt(int)
	 */
	@Override
	public char charAt(final int index) {
		return this.toString().charAt(index);
	}

	/**
	 * Gets a specific subsequence of this.
	 * This delegates to {@link StringConstructor#toString()} and {@link String#subSequence(int, int)}.
	 *
	 * @param start The inclusive start index.
	 * @param end The exclusive end index.
	 * @return The specified subsequence.
	 * @see StringConstructor#toString()
	 * @see CharSequence#subSequence(int, int)
	 * @see String#subSequence(int, int)
	 */
	@Override
	public CharSequence subSequence(final int start, final int end) {
		return this.toString().subSequence(start, end);
	}

	/**
	 * Gets a hash code for this.
	 * This delegates to {@link StringConstructor#toString()} and {@link String#hashCode()}.
	 *
	 * @return A hashcode for this.
	 * @see StringConstructor#toString()
	 * @see String#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	/**
	 * Determines if this is equal to another {@link Object}.
	 * This delegates to {@link StringConstructor#toString()} and {@link String#equals(Object)}.
	 *
	 * @param obj The {@link Object} to check equality.
	 * @return {@code true} if this is equal to the other {@link Object}.
	 * @see StringConstructor#toString()
	 * @see String#equals(Object)
	 */
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	@Override
	public boolean equals(final Object obj) {
		return this.toString().equals(obj);
	}

	/**
	 * Gets a {@link String} representation of this {@link StringConstructor}.
	 * This is typically used when the {@link String} is finished being built.
	 *
	 * @return A {@link String} representation of this {@link StringConstructor}.
	 */
	@Override
	public synchronized String toString() {
		return this.stringCache != null ? this.stringCache :
				(this.stringCache = this.charset == null ? new String(this.output.toByteArray()) :
						new String(this.output.toByteArray(), this.charset));
	}
}
