package me.danielzgtg.compsci11_sem2_2017.common.serialization;

import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;

/**
 * Tools for serializing data to and from a human-readable format and a machine representation.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
@SuppressWarnings("unused")
public final class SerializationUtils {

	/**
	 * Encodes the {@link SerializationUtils}-style {@link Map} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to put the {@link SerializationUtils}-style data.
	 * @param data A {@link SerializationUtils}-style {@link Map}
	 *             with the {@link SerializationUtils}-style data to write.
	 */
	public static final void encode(final OutputStream out, final Map data) {
		Validate.notNull(out);
		Validate.notNull(data);

		try {
			writeMap(out, data);
		} catch (final Exception e) {
			throw new IllegalStateException("Encoding Problem!", e);
		}
	}

	/**
	 * Actually writes the {@link Map} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to put the {@link SerializationUtils}-style data.
	 * @param data A {@link SerializationUtils}-style {@link Map}.
	 * @throws Exception If there was a problem serializing.
	 */
	@SuppressWarnings("unchecked")
	private static final void writeMap(final OutputStream out, final Map data) throws Exception {
		final Iterator<Map.Entry> iter = data.entrySet().iterator();

		if (!iter.hasNext()) {
			// Empty map: "{}"
			return;
		}

		// Map: "{ key1 = value1, key2 = value2 }"
		out.write(' ');
		while (true) {
			final Map.Entry entry = iter.next();

			writeObject(out, entry.getKey());

			out.write(' ');
			out.write('=');
			out.write(' ');

			writeObject(out, entry.getValue());

			if (iter.hasNext()) {
				out.write(',');
				out.write(' ');
			} else {
				break;
			}
		}

		out.write(' ');
	}

	/**
	 * Actually writes the {@link List} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to put the {@link SerializationUtils}-style data.
	 * @param data A {@link SerializationUtils}-style {@link List}.
	 * @throws Exception If there was a problem serializing.
	 */
	private static final void writeList(final OutputStream out, final List data) throws Exception {
		final Iterator iter = data.iterator();

		if (!iter.hasNext()) {
			// Empty list: "[]"
			return;
		}

		// List: "[ item1, item2, item3 ]"
		out.write(' ');
		while (true) {
			final Object item = iter.next();

			writeObject(out, item);

			if (iter.hasNext()) {
				out.write(',');
				out.write(' ');
			} else {
				break;
			}
		}

		out.write(' ');
	}

	/**
	 * Actually writes the {@link Object} to the {@link OutputStream}.
	 *
	 * @param out The {@link OutputStream} to put the {@link SerializationUtils}-style data.
	 * @param data A {@link SerializationUtils}-style {@link Object}.
	 * @throws Exception If there was a problem serializing.
	 */
	private static final void writeObject(final OutputStream out, final Object data) throws Exception {
		if (data == null) {
			// N is for null
			out.write('N');
			return;
		}

		BASIC_SERIALIZERS.get(out.getClass()).serialize(out, data);
	}

	/**
	 * The list of basic serializers.
	 */
	private static final Map<Class, BasicSerializer> BASIC_SERIALIZERS;

	static {
		final HashMap<Class, BasicSerializer> basicSerializers = new HashMap<>();

		basicSerializers.put(Boolean.class, (out, data) -> {
			if ((Boolean) data) {
				// T is for true.
				out.write('T');
			} else {
				// F is for false.
				out.write('F');
			}
		});

		final BasicSerializer wholeNumberSerializer = (out, data) -> {
			final String result = data.toString();

			//if (data instanceof Integer) {
			//result = String.valueOf((Integer) data);
			//} else {
			//result = String.valueOf((Long) data);
			//}

			/*
			 * Whole numbers start and end with #.
			 * The digits go between the #'s.
			 */
			out.write('#');

			final int length = result.length();
			for (int i = 0; i < length; i++) {
				out.write(0xFF & result.charAt(i));
			}

			out.write('#');
		};
		basicSerializers.put(Integer.class, wholeNumberSerializer);
		basicSerializers.put(Long.class, wholeNumberSerializer);

		final BasicSerializer floatingPointSerializer = (out, data) -> {
			final String result = data.toString();

			//if (data instanceof Float) {
			//result = String.valueOf((Float) data);
			//} else {
			//result = String.valueOf((Double) data);
			//}

			if (StringUtils.isDecimal(result)) {
				/*
				 * Decimal numbers start and end with %.
				 * The digits go between the %'s.
				 */
				out.write('%');

				final int length = result.length();
				for (int i = 0; i < length; i++) {
					out.write(0xFF & result.charAt(i));
				}

				out.write('%');
			} else {
				// Complain about Infinities, and NaN's.
				throw new IllegalArgumentException("Not a decimal!");
			}
		};
		basicSerializers.put(Float.class, floatingPointSerializer);
		basicSerializers.put(Double.class, floatingPointSerializer);

		basicSerializers.put(Character.class, (out, data) -> {
			// C is for character.
			out.write('C');
			out.write(((Character) data) & 0xFF);
		});

		basicSerializers.put(List.class, (out, data) -> {
			/*
			 * Lists are wrapped with square brackets.
			 */
			out.write('[');

			writeList(out, (List) data);

			out.write(']');
		});

		basicSerializers.put(Map.class, (out, data) -> {
			/*
			 * Map are wrapped with curly braces.
			 */
			out.write('{');

			writeMap(out, (Map) data);

			out.write('}');
		});

		basicSerializers.put(String.class, (out, data) -> {
			/*
			 * Strings start with a quote (") and end with another.
			 *
			 * The contents go between the quotes,
			 * but embedded quotes are escaped with a backslash in front.
			 */
			final String result = (String) data;

			out.write('\"');

			final int length = result.length();
			for (int i = 0; i < length; i++) {
				final int toWrite = 0xFF & result.charAt(i);

				if (toWrite == '\"') {
					out.write('\\');
					out.write('\"');
				} else {
					out.write(toWrite);
				}
			}

			out.write('\"');
		});

		BASIC_SERIALIZERS = Collections.unmodifiableMap(basicSerializers);
	}

	/**
	 * Parses the {@link SerializationUtils}-style {@link InputStream}
	 * into a {@link SerializationUtils}-style {@link Map}.
	 *
	 * @param in The {@link InputStream} with the {@link SerializationUtils}-style data.
	 * @return A {@link SerializationUtils}-style {@link Map} with
	 *         the parsed {@link SerializationUtils}-style data contents.
	 * @throws IllegalStateException If there was a problem parsing.
	 */
	public static final Map decode(final InputStream in) throws IllegalStateException {
		Validate.notNull(in);

		try {
			return parse0(in);
		} catch (final Exception e) {
			throw new IllegalStateException("Decoding Problem!", e);
		}
	}

	/**
	 * Actually parses the {@link InputStream} into a {@link SerializationUtils}-style {@link Map}.
	 *
	 * @param in The {@link InputStream} with the {@link SerializationUtils}-style data.
	 * @return A {@link SerializationUtils}-style {@link Map} with
	 *         the parsed {@link SerializationUtils}-style data contents.
	 * @throws Exception If there was a problem parsing.
	 */
	private static final Map parse0(final InputStream in) throws Exception {
		final Map result = new HashMap();
		Object current = result; // Must be List or Map
		Object[] keyRef = new Object[] { null };
		Object[] parsingRef = new Object[] { null };
		final Deque<Object> parseStack = new LinkedList<>();
		boolean inComment = false;
		boolean escape = false;
		boolean negative = false;
		int decimalPoint = 0;
		boolean[] hasParsedKeyRef = new boolean[] { false };
		while (true) {
			final int read = in.read();

			if (read < 0) {
				// End
				break;
			}

			// Comments Handling
			// All Comments are full line
			if (inComment) {
				if (read == '\n') { // Read until next newline
					inComment = false;
				}

				continue;
			}

			// String Handling
			if (parsingRef[0] instanceof StringConstructor) {
				if (!escape) {
					switch (read) {
						case '\\': // Backslash escape
							escape = true;
							continue;
						case '"': // End of String
							post(keyRef, parsingRef, current, hasParsedKeyRef, Object::toString);
							continue;
					}
				} else {
					// Escape: Read next char, and stop escaping
					escape = false;
				}

				((StringConstructor) parsingRef[0]).add((byte) read); // Read char
				continue;
			}

			// Comment, whitespace, formatting, and structural characters
			switch (read) {
				case '$': // '$' is full line comment
					inComment = true;
					//continue;
				case ' ':
				case '_':
				case '=':
				case ':':
				case '\n':
				case ',':
				case '\t':
				case '\r':
					continue;
			}

			// Integer handling
			if (parsingRef[0] instanceof long[]) {
				final long[] numRef = (long[]) parsingRef[0];

				switch (read) {
					case '#':
						// End with '#'
						numRef[0] *= negative ? -1 : 1;
						post(keyRef, parsingRef, current, hasParsedKeyRef, (x) -> ((long[]) x)[0]);
						negative = false;
						continue;
					case '-':
						// Negatives
						if (numRef[0] != 0 && !negative) {
							throw new IllegalStateException("Too many negative signs!");
						}
						negative = true;
						continue;
				}

				// Append digit
				numRef[0] = (numRef[0] * 10) + Long.valueOf(String.valueOf((char) read));
				continue;
			}

			// Floating-point handling
			if (parsingRef[0] instanceof double[]) {
				final double[] numRef = (double[]) parsingRef[0];

				switch (read) {
					case '%':
						// End with '#'
						numRef[0] *= negative ? -1 : 1;
						post(keyRef, parsingRef, current, hasParsedKeyRef, (x) -> ((double[]) x)[0]);
						negative = false;
						decimalPoint = 0;
						continue;
					case '-':
						// Negatives
						if (numRef[0] != 0 && !negative) {
							throw new IllegalStateException("Too many negative signs!");
						}
						negative = true;
						continue;
					case '.':
						// Decimals
						if (decimalPoint > 0) {
							throw new IllegalStateException("Too many decimal points!");
						}
						decimalPoint = 1;
						continue;
				}

				// Append digit
				if (decimalPoint == 0) {
					numRef[0] = (numRef[0] * 10) + Long.valueOf(String.valueOf((char) read));
				} else {
					numRef[0] = numRef[0] +
							(Long.valueOf(String.valueOf((char) read)).doubleValue()) * Math.pow(10, -decimalPoint);
					decimalPoint++;
				}

				continue;
			}

			// Character literals
			if (parsingRef[0] == Character.class) {
				parsingRef[0] = (char) read;
				post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
			}

			// Shouldn't have multi-character objects now that can't contain others.
			if (parsingRef[0] != null) {
				throw new IllegalStateException(
						"Unexpected abandoning of Object!: " + parsingRef[0].getClass().getName());
			}

			switch (read) {
				case 'T':
					// T is for True
					parsingRef[0] = Boolean.TRUE;
					post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
					continue;
				case 'F':
					// F is for False
					parsingRef[0] = Boolean.FALSE;
					post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
					continue;
				case 'N':
					// N is for null
					//parsingRef[0] = null;
					post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
					continue;
				case 'C':
					// C is for char
					parsingRef[0] = Character.class;
					continue;
				case '"':
					// '"' starts a String
					parsingRef[0] = new StringConstructor();
					continue;
				case '#':
					// '#' starts a whole number
					parsingRef[0] = new long[1];
					continue;
				case '%':
					// '%' starts a floating-point number
					parsingRef[0] = new double[1];
					continue;
					/*
					 * Maps and lists use stacks for keeping track of the hiearchy.
					 * The state is pushed and popped.
					 */
				case '[':
					// Square brackets are for lists. Opening starts them.
					parseStack.push(new Object[] { current, keyRef, hasParsedKeyRef });
					hasParsedKeyRef = new boolean[1];
					keyRef = new Object[1];
					current = new LinkedList();
					continue;
				case ']': {
					// Square brackets are for lists. Closing ends them.
					if (!(current instanceof List)) {
						throw new IllegalStateException("Closing list when not current!");
					}
					final Object[] restore = (Object[]) parseStack.pop();
					parsingRef[0] = current;
					current = restore[0];
					keyRef = (Object[]) restore[1];
					hasParsedKeyRef = (boolean[]) restore[2];
					post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
					continue;
				}
				case '{':
					// Curly braces are for maps. Opening starts them.
					parseStack.push(new Object[] { current, keyRef, hasParsedKeyRef });
					hasParsedKeyRef = new boolean[1];
					keyRef = new Object[1];
					current = new HashMap();
					continue;
				case '}': {
					// Curly braces are for maps. Closing ends them.
					if (!(current instanceof Map)) {
						throw new IllegalStateException("Closing map when not current!");
					}
					if (hasParsedKeyRef[0]) {
						throw new IllegalStateException("Closing map when value not parsed!");
					}
					final Object[] restore = (Object[]) parseStack.pop();
					parsingRef[0] = current;
					current = restore[0];
					keyRef = (Object[]) restore[1];
					hasParsedKeyRef = (boolean[]) restore[2];
					post(keyRef, parsingRef, current, hasParsedKeyRef, CommonLambdas.PASSTHROUGH_FUNCTION);
					continue;
				}
				default:
					throw new IllegalStateException("Unexpected char!: " + (char) read);
			}
		}

		// Check if parsed correctly
		if (hasParsedKeyRef[0] || escape || keyRef[0] != null || parsingRef[0] != null ||
				decimalPoint != 0 || negative || parseStack.size() != 0) {
			throw new IllegalStateException("Stream ended with unfinished result!");
		}

		return result;
	}

	/**
	 * Helper function for {@link SerializationUtils#parse0(InputStream)}.
	 *
	 * Called to help when an Object is finished reading.
	 */
	@SuppressWarnings({"unchecked", "JavaDoc"})
	private static final void post(final Object[] keyRef, final Object[] parsingRef,
			final Object current, final boolean[] hasParsedKeyRef, final Function finalizeFunction) {
		final Object parse = finalizeFunction.apply(parsingRef[0]);
		parsingRef[0] = null;

		if (current instanceof List) {
			((List) current).add(parse);
			return;
		}

		// current instanceof Map
		if (!hasParsedKeyRef[0]) {
			keyRef[0] = parse;
			hasParsedKeyRef[0] = true;
			return;
		}

		((Map) current).put(keyRef[0], parse);
		keyRef[0] = null;
		hasParsedKeyRef[0] = false;
	}

	@Deprecated
	private SerializationUtils() { throw new UnsupportedOperationException(); }
}
