package me.danielzgtg.compsci11_sem2_2017.common;

import java.util.Collection;
import java.util.Map;

/**
 * A class for checking conditions.
 *
 * @author Daniel Tang
 * @since 14 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue"})
public final class Validate {

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@code boolean} is {@code false},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param condition The boolean condition to require to be {@code true}.
	 * @throws IllegalArgumentException If {@code condition} is false.
	 */
	public static final void require(final boolean condition) throws IllegalArgumentException {
		if (!condition) {
			throw new IllegalArgumentException("Condition not met!");
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@code boolean} is {@code false},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param condition The {@code boolean} condition to require to be {@code true}.
	 * @param message The message to include in the exception.
	 * @throws IllegalArgumentException If {@code condition} is false.
	 */
	public static final void require(final boolean condition, final String message)
			throws IllegalArgumentException{
		if (!condition) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link String} is {@code null},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param s The {@link String} to null-check.
	 * @throws IllegalArgumentException If {@code s} is {@code null}.
	 */
	public static final void notNull(final String s) throws IllegalArgumentException {
		if (s == null) {
			throw new IllegalArgumentException("Null Strings don't make sense!");
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Object} is {@code null},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param o The {@link Object} to null-check.
	 * @throws IllegalArgumentException If {@code o} is {@code null}.
	 */
	public static final void notNull(final Object o) throws IllegalArgumentException {
		if (o == null) {
			throw new IllegalArgumentException("Argument cannot be Null!");
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Object} is {@code null},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param o The {@link Object} to null-check.
	 * @param message The message to include in the exception.
	 * @throws IllegalArgumentException If {@code o} is {@code null}.
	 */
	public static final void notNull(final Object o, final String message)
			throws IllegalArgumentException {
		if (o == null) {
			throw new  IllegalArgumentException(message);
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Collection} is {@code null},
	 * or contains any {@code null} elements,
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param c The {@link Collection} to check.
	 * @throws IllegalArgumentException If {@code c}, or any element of it, is {@code null}.
	 */
	public static final void withoutNull(final Collection<?> c) throws IllegalArgumentException {
		if (c == null) {
			throw new IllegalArgumentException("Collection itself cannot be null!");
		}

		for (final Object o : c) {
			if (o == null) {
				throw new IllegalArgumentException("Collection element cannot be null!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Collection} is {@code null},
	 * or contains any {@code null} elements,
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param c The {@link Collection} to check.
	 * @param message The message to include in the exception.
	 * @throws IllegalArgumentException If {@code c}, or any element of it, is {@code null}.
	 */
	public static final void withoutNull(final Collection<?> c, final String message)
			throws IllegalArgumentException {
		if (c == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : c) {
			if (o == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Map} is {@code null},
	 * or contains any {@code null} values,
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param m The {@link Map} to check.
	 * @throws IllegalArgumentException If {@code m}, or any value of it, is {@code null}.
	 */
	public static final void withoutNull(final Map<?, ?> m) throws IllegalArgumentException {
		if (m == null) {
			throw new IllegalArgumentException("Map itself cannot be null!");
		}

		for (final Object o : m.values()) {
			if (o == null) {
				throw new IllegalArgumentException("Map value cannot be null!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Map} is {@code null},
	 * or contains any {@code null} values,
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param m The {@link Map} to check.
	 * @param message The message to include in the exception.
	 * @throws IllegalArgumentException If {@code m}, or any value of it, is {@code null}.
	 */
	public static final void withoutNull(final Map<?, ?> m, final String message)
			throws IllegalArgumentException {
		if (m == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : m.values()) {
			if (o == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified array is {@code null},
	 * or contains any {@code null} items,
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param a The array to check.
	 * @throws IllegalArgumentException If {@code a}, or any item of it, is {@code null}.
	 */
	public static final void withoutNull(final Object[] a) throws IllegalArgumentException {
		if (a == null) {
			throw new IllegalArgumentException("Array itself cannot be null");
		}

		for (final Object o : a) {
			if (o == null) {
				throw new IllegalArgumentException("Array item cannot be null!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified array is {@code null},
	 * or contains any {@code null} items,
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param a The array to check.
	 * @param message The message to include in the exception.
	 * @throws IllegalArgumentException If {@code a}, or any item of it, is {@code null}.
	 */
	public static final void withoutNull(final Object[] a, final String message) {
		if (a == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : a) {
			if (o == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Collection} is {@code null},
	 * or contains any elements that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param c The {@link Collection} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code c}, or any element of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Collection<?> c, final Class<T> type) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (c == null) {
			throw new IllegalArgumentException("Collection itself cannot be null!");
		}

		for (final Object o : c) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException("Collection element is of wrong type!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Collection} is {@code null},
	 * or contains any elements that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param c The {@link Collection} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param message The message to include in the exception.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code c}, or any element of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Collection<?> c, final Class<T> type, final String message) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (c == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : c) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Map} is {@code null},
	 * or contains any values that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param m The {@link Map} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code m}, or any value of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Map<?, ?> m, final Class<T> type) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (m == null) {
			throw new IllegalArgumentException("Map itself cannot be null!");
		}

		for (final Object o : m.values()) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException("Map value is of wrong type!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified {@link Map} is {@code null},
	 * or contains any values that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param m The {@link Map} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param message The message to include in the exception.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code m}, or any value of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Map<?, ?> m, final Class<T> type, final String message) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (m == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : m.values()) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified array is {@code null},
	 * or contains any items that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param a The array to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code a}, or any item of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Object[] a, final Class<T> type) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (a == null) {
			throw new IllegalArgumentException("Array itself cannot be null!");
		}

		for (final Object o : a) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException("Array item is of wrong type!");
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified array is {@code null},
	 * or contains any items that are not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param a The array to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param message The message to include in the exception.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code a}, or any item of it, is not {@code instanceof} {@link T}.
	 */
	public static final <T> void allAreType(final Object[] a, final Class<T> type, final String message) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (a == null) {
			throw new IllegalArgumentException(message);
		}

		for (final Object o : a) {
			if (!type.isInstance(o)) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified arr {@link Object} not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException}.
	 *
	 * @param o The {@link Object} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code a} is not {@code instanceof} {@link T}.
	 */
	public static final <T> void isType(final Object o, final Class<T> type) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (!type.isInstance(o)) {
			throw new IllegalArgumentException("Wrong Type!");
		}
	}

	/**
	 * Makes sure logic does not flow past the invocation of this method
	 * when the specified arr {@link Object} not {@code instanceof} {@link T},
	 * by throwing an {@link IllegalArgumentException} with a predetermined message.
	 *
	 * @param o The {@link Object} to check.
	 * @param type The {@link Class} of {@link T}.
	 * @param message The message to include in the exception.
	 * @param <T> The type to validate for.
	 * @throws IllegalArgumentException If {@code a} is not {@code instanceof} {@link T}.
	 */
	public static final <T> void isType(final Object o, final Class<T> type, final String message) {
		if (type == null) {
			throw new IllegalArgumentException("Must specify a Type!");
		}

		if (!type.isInstance(o)) {
			throw new IllegalArgumentException(message);
		}
	}

	@Deprecated
	private Validate() { throw new UnsupportedOperationException();}
}
