package me.danielzgtg.compsci11_sem2_2017.common.reflect;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A class representing an attribute.
 *
 * The underlying backing might be a field, a getter-setter pair, or something else.
 * None of the contained methods are guaranteed to work.
 *
 * @param <C> The type of the attribute container object.
 * @param <T> The type of the attribute value.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class Attribute<C, T> {

	/**
	 * The class of the type of the attribute container object.
	 */
	public final Class<C> targetClass;

	/**
	 * The class of the type of the attribute value.
	 */
	public final Class<T> attributeClass;

	/**
	 * The name of this attribute.
	 */
	public final String name;

	/**
	 * Creates a wrapper around an attribute.
	 *
	 * @param targetClass The class of the target object type, may NOT be null.
	 * @param attributeClass The class of the attribute value type, may NOT be null.
	 * @param name The name of this attribute.
	 */
	protected Attribute(final Class<C> targetClass, final Class<T> attributeClass,
			final String name) {
		Validate.notNull(targetClass);
		Validate.notNull(attributeClass);
		Validate.notNull(name);

		this.targetClass = targetClass;
		this.attributeClass = attributeClass;
		this.name = name;
	}

	/**
	 * Gets the class of the type of the attribute container object.
	 *
	 * @return The class of the type of the attribute container object.
	 */
	public final Class<C> getTargetClass() {
		return this.targetClass;
	}

	/**
	 * Get the class of the type of the attribute value.
	 *
	 * @return The class of the type of the attribute value.
	 */
	public final Class<T> getAttributeClass() {
		return this.attributeClass;
	}

	/**
	 * Gets the name of this attribute.
	 *
	 * @return The name of this attribute.
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * Sets the value of the attribute.
	 *
	 * @param target The object containing the attribute.
	 * @param data The new value of the attribute.
	 * @throws ReflectiveOperationException If the setting operation failed.
	 */
	public abstract void set(final C target, final T data) throws ReflectiveOperationException;

	/**
	 * Determines whether the attribute can be written.
	 *
	 * @return {@code false} if {@link Attribute#set(Object, Object)} is not expected to work.
	 */
	public abstract boolean isMutable();

	/**
	 * Gets the value of the attribute.
	 *
	 * @param target The object containing the attribute.
	 * @return The value of the attribute.
	 * @throws ReflectiveOperationException If the getting operation failed.
	 */
	public abstract T get(final C target) throws ReflectiveOperationException;

	/**
	 * Determines whether the attribute can be read.
	 *
	 * @return {@code false} if {@link Attribute#get(Object)} is not expected to work.
	 */
	public abstract boolean isVisible();
}
