package me.danielzgtg.compsci11_sem2_2017.common.reflect;

/**
 * A builder-pattern class.
 *
 * @author Daniel Tang
 * @since 16 June 2017
 *
 * @param <T> The type of {@link Object} that this will build into.
 */
public abstract interface IBuilder<T> {

	/**
	 * Builds into a {@link T}.
	 *
	 * @return The build {@link T}.
	 */
	public abstract T build();
}
