package me.danielzgtg.compsci11_sem2_2017.common.reflect;

import java.lang.reflect.Method;

import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;

/**
 * An {@link Attribute} based on a getter-setter encapsulation.
 *
 * @param <C> The type of the attribute container object.
 * @param <T> The type of the attribute value.
 */
@SuppressWarnings("unused")
public final class EncapsulatedAttribute<C, T> extends Attribute<C, T> {

	/**
	 * The getter method.
	 */
	private final Method getter;

	/**
	 * The setter method.
	 */
	private final Method setter;

	/**
	 * Creates a wrapper around a getter-setter pair using the field name and target.
	 *
	 * @param targetClass The class of the target object type, may NOT be null.
	 * @param fieldName The name of the field, to derive standard getter and setter names, may NOT be null.
	 * @param attributeClass The class of the attribute value type, may NOT be null.
	 */
	public EncapsulatedAttribute(final Class<C> targetClass, final String fieldName, final Class<T> attributeClass) {
		super(targetClass, attributeClass, fieldName);

		Method newGetter = null;
		try {
			final Method foundGetter = targetClass.getDeclaredMethod(
					StringUtils.fieldToGetterName(fieldName));

			if (foundGetter.getReturnType().isAssignableFrom(attributeClass)) {
				newGetter = foundGetter;
			}
		} catch (final NoSuchMethodException ignore) {}

		this.getter = newGetter;

		Method newSetter = null;
		try {
			final Method foundSetter = targetClass.getDeclaredMethod(
					StringUtils.fieldToSetterName(fieldName), attributeClass);

			if (foundSetter.getReturnType().equals(void.class)) {
				newSetter = foundSetter;
			}
		} catch (final NoSuchMethodException ignore) {}

		this.setter = newSetter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void set(final C target, final T data) throws ReflectiveOperationException {
		if (this.setter == null) {
			throw new IllegalAccessException();
		}

		if (!this.targetClass.isInstance(target)) {
			throw new NoSuchMethodException();
		}

		this.setter.invoke(target, data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMutable() {
		return this.setter != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T get(final C target) throws ReflectiveOperationException {
		if (this.getter == null) {
			throw new IllegalAccessException();
		}

		if (!this.targetClass.isInstance(target)) {
			throw new NoSuchMethodException();
		}

		return (T) this.getter.invoke(target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisible() {
		return this.getter != null;
	}
}
