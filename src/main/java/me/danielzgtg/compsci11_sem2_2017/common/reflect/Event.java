package me.danielzgtg.compsci11_sem2_2017.common.reflect;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * An event type that can be fire.
 * These events cannot be cancelled.
 *
 * @author Daniel Tang
 * @since 18 June 2017
 *
 * @param <D> The type of payload of the event.
 */
@SuppressWarnings("unused")
public final class Event<D> {

	/**
	 * The {@link List} of {@link Event} listeners.
	 */
	private final List<Consumer<D>> listeners = new LinkedList<>();

	/**
	 * Fires the {@link Event} with the particulars.
	 *
	 * @param data The particulars of this event occurrence.
	 */
	public final void fire(final D data) {
		for (final Consumer<D> listener : this.listeners) {
			listener.accept(data);
		}
	}

	/**
	 * Adds an {@link Event} listener.
	 *
	 * @param listener The {@link Event} listener.
	 */
	public final void addListener(final Consumer<D> listener) {
		Validate.notNull(listener);

		this.listeners.add(listener);
	}

	/**
	 * Removes an {@link Event} listener.
	 *
	 * @param listener The {@link Event} listener.
	 */
	public final void removeListener(final Consumer<D> listener) {
		if (listener != null) {
			this.listeners.remove(listener);
		}
	}

	/**
	 * Removes all {@link Event} listeners.
	 */
	public final void reset() {
		this.listeners.clear();
	}
}
