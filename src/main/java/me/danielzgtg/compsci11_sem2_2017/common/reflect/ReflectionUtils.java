package me.danielzgtg.compsci11_sem2_2017.common.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Tools to help with reflection.
 *
 * @author Daniel Tang
 * @since 30 April 2017
 */
@SuppressWarnings("unused")
public final class ReflectionUtils {

	/**
	 * Determines is the specified {@link Method} is an instance method.
	 *
	 * @param method The {@link Method} to check.
	 * @return {@code true} if the specified {@link Method} is indeed an instance method.
	 */
	public static final boolean isInstanceMethod(final Method method) {
		Validate.notNull(method);

		return !Modifier.isStatic(method.getModifiers());
	}

	/**
	 * Produces the result of the {@link Object#toString()} method as if it had not been overridden.
	 *
	 * @param o The {@link Object} to get the default {@link Object#toString()} of.
	 * @return {@code "null"} if {@code o} is {@code null}, else an imitation of {@link Object#toString()}.
	 */
	public static final String defaultToString(final Object o) {
		return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(o.hashCode());
	}

	/**
	 * Produces the result of the {@link Object#toString()} method
	 * as if it and {@link Object#hashCode()} have not been overridden.
	 *
	 * @param o The {@link Object} to get the very default {@link Object#toString()} of.
	 * @return {@code "null"} if {@code o} is {@code null}, else an imitation of {@link Object#toString()}.
	 */
	public static final String veryDefaultToString(final Object o) {
		return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
	}

	@Deprecated
	private ReflectionUtils() { throw new UnsupportedOperationException(); }
}
